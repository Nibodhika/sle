// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "graphics/text.hpp"

Text::Text(Font* font, std::string text, Color color,DrawMode mode, Color bgColor,SDL_Renderer* renderer){
  create(font, text, color, mode, bgColor,renderer);
}

Text::~Text(){
    if(m_texture)
            delete(m_texture);
}

bool Text::changeFont(Font* font){
  m_font = font;
  return _recreate();
}
bool Text::changeText(std::string text){
  m_text = text;
  return _recreate();
}
bool Text::changeColor(Color color){
  m_color = color;
  return _recreate();
}
bool Text::changeRenderer(SDL_Renderer* renderer){
  m_renderer = renderer ? renderer : Window::main_window->renderer();
  return _recreate();
}

bool Text::create(Font* font, std::string text, Color color,DrawMode mode, Color bgColor, SDL_Renderer* renderer) {
  if(!font || text.empty())
    return false;
  m_font = font;
  m_text = text;
  m_color = color;
  m_mode = mode;
  m_bg = bgColor;
  m_renderer = renderer ? renderer : Window::main_window->renderer();
  return _recreate();
}
bool Text::_recreate() {
  SDL_Surface* textSurface = NULL;
  switch(m_mode){
    case SOLID:
       textSurface = TTF_RenderText_Solid( m_font->font(), m_text.c_str(), *m_color.color());
      break;
    case SHADED:
       textSurface = TTF_RenderText_Shaded( m_font->font(), m_text.c_str(), *m_color.color(), *m_bg.color());
      break;
    case BLENDED:
       textSurface = TTF_RenderText_Blended( m_font->font(), m_text.c_str(), *m_color.color());
      break;
  };
 
    if( textSurface == NULL ) {
        printf( "Unable to render text surface! SDL_ttf Error: %s\n", TTF_GetError() );
        return false;
    }

    if(m_texture)
        delete(m_texture);
    m_texture = new Texture(textSurface,m_renderer);
    if(m_pos.isNull()){
        m_pos.setSize(m_texture->size());
    }
    //Get rid of old surface
    SDL_FreeSurface( textSurface );
    
    if( m_texture == NULL )  {
      printf( "Unable to create texture from rendered text! SDL Error: %s\n", SDL_GetError() );
      return false;
    }

    return true;
}

void Text::draw(Window* window) {
    //TODO nullptr here should not be working... this is weird
    doDraw(window,m_texture, nullptr);
}

