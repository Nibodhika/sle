#include "graphics/drawable.hpp"
#include "graphics/window.hpp"

Drawable::Drawable(){
    
}
void Drawable::setPos(Point pos){
  m_pos.setPos(pos);
}
void Drawable::setPos(float x, float y){
  m_pos.setPos(x,y);
}
void Drawable::setSize(float w, float h){
  m_pos.setSize(w,h);
  if(m_defaultCenter)
      setCenter();
}
void Drawable::setSize(Point size){
  m_pos.setSize(size);
  if(m_defaultCenter)
      setCenter();
}
void Drawable::setPosSize(Rect pos) {
    m_pos = pos;
    if(m_defaultCenter)
      setCenter();
}
void Drawable::setRotation(double value) {
    m_angle = value;
}

void Drawable::scale(float factor){
  m_pos.scale(factor);
  if(m_defaultCenter)
      setCenter();
}
void Drawable::scaleToWidth(float width){
  m_pos.scaleToWidth(width);
  if(m_defaultCenter)
      setCenter();
}
void Drawable::scaleToHeight(float height){
  m_pos.scaleToHeight(height);
  if(m_defaultCenter)
      setCenter();
}
void Drawable::scaleToSize(Point size){
  m_pos.scaleToSize(size);
  if(m_defaultCenter)
      setCenter();
}
void Drawable::scaleToSize(float width, float height){
  m_pos.scaleToSize(width,height);
  if(m_defaultCenter)
      setCenter();
}

Rect* Drawable::pos(){
  return &m_pos;
}
void Drawable::move(Point delta) {
    m_pos.moveBy(delta);
//   m_pos.x() += delta.x();
//   m_pos.y() += delta.y();
}
void Drawable::move(float x, float y) {
    m_pos.moveBy(x,y);
}

void Drawable::rotate(double angle) {
    m_angle += angle;
}

void Drawable::setCenter(Point center) {
    setCenter(center.x(), center.y());
}

bool Drawable::isDefaultCenter() {
    return m_defaultCenter;
}


void Drawable::setCenter(int x, int y) {
    m_center.x(x);
    m_center.y(y);
    std::cout << "\n\n\nSetCenter\n\n" << std::endl;
    m_defaultCenter = false;
}

void Drawable::setCenter() {
    m_center.x(m_pos.w() / 2);
    m_center.y(m_pos.h() / 2);
    m_defaultCenter = true;
}


void Drawable::setFlipH(bool v) {
    if(v)
        m_flipped = (SDL_RendererFlip) (m_flipped | SDL_RendererFlip::SDL_FLIP_HORIZONTAL);
    else
        m_flipped = (SDL_RendererFlip) (m_flipped & (!SDL_RendererFlip::SDL_FLIP_HORIZONTAL));
    
}

void Drawable::setFlipV(bool v) {
    if(v)
        m_flipped = (SDL_RendererFlip) (m_flipped | SDL_RendererFlip::SDL_FLIP_VERTICAL);
    else
        m_flipped = (SDL_RendererFlip) (m_flipped & (! SDL_RendererFlip::SDL_FLIP_VERTICAL));
}

void Drawable::setFlipped(SDL_RendererFlip v) {
    m_flipped = v;
}


void Drawable::flipH(){
    setFlipH( ! isFlippedH() );
}
void Drawable::flipV() {
    setFlipV( ! isFlippedV() );
}


bool Drawable::isFlippedH() {
    return (m_flipped & SDL_RendererFlip::SDL_FLIP_HORIZONTAL) != 0;
}
bool Drawable::isFlippedV() {
    return (m_flipped & SDL_RendererFlip::SDL_FLIP_VERTICAL) != 0;
}
SDL_RendererFlip Drawable::flipped() {
    return m_flipped;
}

void Drawable::doDraw(Window* window, Texture* tex, Rect* srcRect) {
//     std::cout << "Drawing: " << tex << " src " << *srcRect << " on " << m_pos << std::endl;
    window->draw(tex, srcRect, &m_pos, m_angle, &m_center, m_flipped);
}


