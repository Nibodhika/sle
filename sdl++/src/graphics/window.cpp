#include "graphics/window.hpp"

#include "audio/sound.hpp"
#include "graphics/drawable.hpp"

bool Window::sdlInited = false;
Window* Window::main_window = nullptr;

Window::Window(std::string title, int width, int height,SDL_WindowFlags flags, Interpolation interpolation,Uint32 init_flags) {
  m_width = width;
  m_height = height;
//   m_scale = scale;
  
  //if SDL was not initialized, do the initialization
  if(!sdlInited) {
    initializeSDL(init_flags);
    //If I was left in charge of initializing things initialize other subsystems also
    Sound::simpleInit();
  }
  
  setInterpolation(interpolation);
  
  //Create window
  m_window = SDL_CreateWindow( title.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, m_width, m_height, flags );
  if( m_window == NULL ) {
    printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
  }
  else  {
    //Create vsynced renderer for window
    m_renderer = SDL_CreateRenderer( m_window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC );
    if( m_renderer == NULL ) {
      printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
    }
    else {
      //Initialize renderer color
      SDL_SetRenderDrawColor( m_renderer, 0xFF, 0xFF, 0xFF, 0xFF );

      //Initialize PNG loading
      int imgFlags = IMG_INIT_PNG;
      if( !( IMG_Init( imgFlags ) & imgFlags ) ) {
	printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
      }
    }
  }

  //If this is the first window to be created, make it the main window
  if(!main_window){
    main_window = this;
  }
  
}

Window::~Window() {
    if(m_renderer)
        SDL_DestroyRenderer(m_renderer);
    if(m_window)
        SDL_DestroyWindow(m_window);
    if(this == main_window){
        SDL_Quit();
        main_window = nullptr;
    }
}

bool Window::initializeSDL(Uint32 flags) {
  //Initialization flag
  bool success = true;

  //Initialize SDL
  //TODO Perhaps just init video here, and then each thing is initialized when it's first needed using SDL_InitSubSystem
  if( SDL_Init( flags ) < 0 ) {
    printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
    success = false;
  }
  else {
    //Set texture filtering to linear
    if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) ) {
      printf( "Warning: Linear texture filtering not enabled!" );
    }
  }

  sdlInited = success;
  
  return success;
}

SDL_Renderer* Window::renderer() {
return m_renderer;
}
SDL_Window* Window::window() {
return m_window;
}

void Window::clear(Color color) {
  SDL_SetRenderDrawColor(m_renderer,color.r(), color.g(), color.b(), color.a());
  SDL_RenderClear(m_renderer);
}
void Window::display()
{
  SDL_RenderPresent(m_renderer);
   Uint32 current = SDL_GetTicks();
  m_delta = current - m_prev_time;
  m_prev_time = current;
}

void Window::setZoom(float zoomX, float zoomY) {
    SDL_RenderSetScale(m_renderer, zoomX, zoomY);
}

void Window::setViewport(SDL_Rect* viewport) {
  SDL_RenderSetViewport(m_renderer, viewport);
}

void Window::draw(Drawable* drawable) {
    drawable->draw(this);
}


void Window::draw(SDL_Texture* texture, const SDL_Rect* srcrect, const SDL_Rect* dstrect) {
 SDL_RenderCopy(m_renderer, texture, srcrect, dstrect);
}

void Window::draw(Texture* texture, Rect* src, Rect* dst) {
    draw(texture->texture(), src->rect(), dst->rect());
}
void Window::draw(Texture& texture, Rect& src, Rect& dst) {
    draw(&texture,&src,&dst);
}



void Window::draw(SDL_Texture* texture, const SDL_Rect* srcrect, const SDL_Rect* dstrect, const double angle, const SDL_Point* center, const SDL_RendererFlip flip) {
    SDL_RenderCopyEx(m_renderer,texture,srcrect,dstrect,angle,center,flip);
}

void Window::draw(Texture* texture, Rect* src, Rect* dst, double angle, Point* point, SDL_RendererFlip flip) {
    draw(texture->texture(), src->rect(),dst->rect(),angle,point->point(),flip);
}

void Window::draw(Texture& texture, Rect& src, Rect& dst, double angle, Point& point, SDL_RendererFlip flip) {
    draw(&texture,&src,&dst,angle,&point,flip);
}


// void Window::draw(Sprite* sprite) {
//   SDL_RenderCopy(m_renderer, sprite->sdlTexture(), sprite->sdlSrcRect(), sprite->sdlDstRect());
// }

Uint32 Window::delta() {
    return m_delta;
}

float Window::deltaSeconds() {
    return m_delta / 1000.0;
}

void Window::setInterpolation(Window::Interpolation interpolation) {
    m_interpolation = interpolation;
    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, std::to_string(interpolation).c_str());
}

