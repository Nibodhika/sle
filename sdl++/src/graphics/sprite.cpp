// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "graphics/sprite.hpp"
#include <iostream>

Sprite::Sprite(Image* i) {
   setImage(i);
}

void Sprite::setImage(Image* i){
    m_image = i;
    if(i == nullptr){
        std::cerr << "WARNING: Created a sprite without image" << std::endl;
        return;
    }
    //making the default position the same size as the image we're using
    if(m_pos.isNull()){
        setSize(m_image->srcRect()->size());
    }
}

Image * Sprite::getImage() {
    return m_image;
}



void Sprite::draw(Window* window) {
    doDraw(window, m_image->texture(), m_image->srcRect());
     //SDL_RenderCopy(window->renderer(),m_texture, sdlSrcRect(), sdlDstRect());
}
