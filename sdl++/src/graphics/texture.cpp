// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "graphics/texture.hpp"

#include "graphics/window.hpp"

#include <iostream>

Texture::Texture(std::string filepath) {
  if(!filepath.empty())
    open(filepath);
}

Texture::Texture(SDL_Surface* surface, SDL_Renderer* renderer) {
  _create(surface,renderer);
}

Texture::~Texture() {
    if(m_texture)
        SDL_DestroyTexture(m_texture);
}

bool Texture::open(std::string filepath, SDL_Renderer* renderer) {
  //Load image at specified path
  SDL_Surface* loadedSurface = IMG_Load( filepath.c_str() );
  if( loadedSurface == NULL ) {
    printf( "Unable to load image %s! SDL_image Error: %s\n", filepath.c_str(), IMG_GetError() );
    return false;
  }
  //Color key image
  SDL_SetColorKey( loadedSurface, SDL_TRUE, SDL_MapRGB( loadedSurface->format, 0, 0xFF, 0xFF ) );
  bool out = _create(loadedSurface,renderer);
  //Get rid of old loaded surface
  SDL_FreeSurface( loadedSurface );
  return out;
}

bool Texture::_create(SDL_Surface* surface, SDL_Renderer* renderer){
  if(!surface)
    return false;
  //Create texture from surface pixels
  //If no renderer was passed use the main renderer from the main_window
  if(!renderer)
    renderer = Window::main_window->renderer();

  m_texture = SDL_CreateTextureFromSurface( renderer, surface );
  //Get image dimensions
  m_width = surface->w;
  m_height = surface->h;
  if( m_texture == NULL ) {
    printf( "Unable to create texture! SDL Error: %s\n", SDL_GetError() );
    return false;
  }
  return true;
}
int Texture::width()
{
return m_width;
}
int Texture::height()
{
return m_height;
}
Point Texture::size()
{
return Point(m_width, m_height);
}
SDL_Texture* Texture::texture(){
  return m_texture;
}
