#include "graphics/image.hpp"

#include <iostream>

Image::Image(Texture* texture, Rect subRect) {
     if(texture) {
        setTexture(texture);
        setSubrect(subRect);
    }
}

void Image::setTexture(Texture* texture){
  m_texture = texture;
}
void Image::setSubrect(Rect subRect){
  if(subRect.isNull()) {
    m_subimg.x(subRect.x());
    m_subimg.y(subRect.y());
    if(m_texture){
        //Using full texture size
      auto size = m_texture->size();
      m_subimg.setSize(size);
    }
    else
      std::cerr << "Texture is null on setSubRect constructor" << std::endl;
  }
  else{
    m_subimg = subRect;
  }
}

Texture* Image::texture(){
  return m_texture;
}
Rect* Image::srcRect(){
  return &m_subimg;
}

