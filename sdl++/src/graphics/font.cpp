// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "graphics/font.hpp"

bool Font::TTF_lib_inited = false;

Font::Font(std::string filepath,int size){
  if(!filepath.empty())
    open(filepath, size);
}
Font::~Font(){
  if(m_font)
    TTF_CloseFont( m_font );
}

bool Font::open(std::string filepath,int size){
  if(!TTF_lib_inited){
    if( TTF_Init() == -1 )
    {
      printf( "SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError() );
      return false;
    }
    TTF_lib_inited = true;
  }
  
  m_font = TTF_OpenFont(filepath.c_str(),size );
  if(!m_font) {
    printf("TTF_OpenFont: %s\n", TTF_GetError());
    return false;
  }
  return true;
}

TTF_Font* Font::font(){
  return m_font;
}
