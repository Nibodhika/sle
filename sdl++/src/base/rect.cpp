#include "base/rect.hpp"


Rect::Rect(int x, int y, int w, int h){
  setPos(x,y);
  setSize(w,h);
}
Rect::Rect(Point pos, Point size) {
  setPos(pos);
  setSize(size);
}
Rect::Rect(std::vector<int> vector) {
    m_rect.x = m_rect.y = m_rect.w = m_rect.h = 0;
    fromVector(vector);
}


void Rect::fromVector(std::vector<int> vector) {
    if(vector.size() >= 1)
        m_rect.x = vector[0];
    if(vector.size() >= 2)
        m_rect.y = vector[1];
    if(vector.size() >= 3)
        m_rect.w = vector[2];
    if(vector.size() >= 4)
        m_rect.h = vector[3];
}

SDL_Rect* Rect::rect(){
  return &m_rect;
}
int Rect::x() const {
  return m_rect.x;
}
int Rect::y() const {
  return m_rect.y;
}
int Rect::w() const {
  return m_rect.w;
}
int Rect::h() const {
  return m_rect.h;
}
void Rect::x(int x) {
    m_rect.x = x;
}
void Rect::y(int y) {
    m_rect.y = y;
}
void Rect::w(int w) {
    m_rect.w = w;
}
void Rect::h(int h) {
    m_rect.h = h;
}

Point Rect::pos(){
  return Point(m_rect.x,m_rect.y);
}
void Rect::setPos(int x, int y){
  m_rect.x = x;
  m_rect.y = y;
}
void Rect::setPos(Point pos){
  setPos(pos.x(),pos.y());
}
Point Rect::size(){
  return Point(m_rect.w,m_rect.h);
}
void Rect::setSize(int w, int h){
  m_rect.w = w;
  m_rect.h = h;
}
void Rect::setSize(Point size){
  setSize(size.x(),size.y());
}
void Rect::scale(float factor){
  m_rect.w *= factor;
  m_rect.h *= factor;
}
void Rect::scaleToWidth(float width){
  float factor = width/ ((float)m_rect.w);
  scale(factor);
}
void Rect::scaleToHeight(float height){
  float factor = height / ((float)m_rect.h);
  scale(factor);
}
void Rect::scaleToSize(Point size){
  scaleToSize(size.x(),size.y());
}
void Rect::scaleToSize(float width, float height){
  float factorW = width/ ((float)m_rect.w);
 float factorH = height / ((float)m_rect.h);
 float factor = factorW < factorH ? factorW : factorH;
 scale(factor);
}
bool Rect::isNull(){
  return (m_rect.w == 0 && m_rect.h ==0);
}
void Rect::moveBy(const int x, const int y) {
    m_rect.x = x;
    m_rect.y = y;
}

void Rect::moveBy(const Point& delta) {
    m_rect.x += delta.x();
    m_rect.y += delta.y();
}

void Rect::moveTo(const Point& delta) {
    m_rect.x = delta.x();
    m_rect.y = delta.y();
}

#include <iostream>
#include <sstream>
void Rect::fromString(std::string values) {
    std::stringstream stream(values);
    stream >> m_rect.x >> m_rect.y >> m_rect.w >> m_rect.h;
}

std::string Rect::toString() {
    return std::to_string(m_rect.x)+" "+std::to_string(m_rect.y)+" "+std::to_string(m_rect.w)+" "+std::to_string(m_rect.h);
}

std::ostream & operator<<(std::ostream &os, Rect const &m) { 
    return os << "["<< m.x() <<","<<m.y() << " " << m.w() << "x" << m.h() << "]";
}

