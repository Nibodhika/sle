// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "base/transform.hpp"

Transform::Transform(){
    m_pos = PointF(0,0);
    m_rotation = 0;
    m_scale = PointF(1,1);
}
PointF Transform::position() const {
    return m_pos;
}
void Transform::setPosition(PointF p) {
    m_pos = p;
}

float Transform::rotation() const {
    return m_rotation;
}
void Transform::setRotation(float v) {
    m_rotation = v;
}

PointF Transform::scale() const {
    return m_scale;
}
void Transform::setScale(PointF p) {
    m_scale = p;
}
void Transform::move(float x, float y) {
    move(PointF(x,y));
}
void Transform::move(PointF delta) {
    m_pos += delta;
}
void Transform::rotate(float angle) {
    m_rotation += angle;
}
void Transform::scale(float delta) {
    scale(delta,delta);
}
void Transform::scale(float x, float y) {
    scale(PointF(x,y));
}
void Transform::scale(PointF delta) {
    m_scale *= delta;
}






Transform Transform::operator*(const Transform& other) {
    Transform out;
    out.setPosition(other.position() +  m_pos);
    out.setRotation ( other.rotation() + m_rotation);
    out.setScale( other.scale() * m_scale);
    return out;
}
void Transform::operator*=(const Transform& other) {
    m_pos += other.position();
    m_rotation += other.rotation();
    m_scale *= other.scale();
}
