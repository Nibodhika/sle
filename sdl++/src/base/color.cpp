#include "base/color.hpp"

Color Color::White = Color(255,255,255);
Color Color::Black = Color(0,0,0);
Color Color::Red   = Color(255,0,0);
Color Color::Green = Color(0,255,0);
Color Color::Blue  = Color(0,0,255);

Color::Color(Uint8 r, Uint8 g, Uint8 b, Uint8 a){
    m_color.r = r;
    m_color.g = g;
    m_color.b = b;
    m_color.a = a;
}

SDL_Color * Color::color() {
    return &m_color;
}
Uint8 Color::r() const {
    return m_color.r;
}
Uint8 Color::g() const {
    return m_color.g;
}
Uint8 Color::b() const {
    return m_color.b;
}
Uint8 Color::a() const {
    return m_color.a;
}

void Color::r(Uint8 r) {
    m_color.r = r;
}
void Color::g(Uint8 g) {
    m_color.g = g;
}
void Color::b(Uint8 b) {
    m_color.b = b;
}
void Color::a(Uint8 a) {
    m_color.a = a;
}






