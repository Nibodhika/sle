#include "base/point.hpp"

Point::Point(int x, int y){
    m_point.x = x;
    m_point.y = y;
}
SDL_Point * Point::point() {
    return &m_point;
}
int Point::x() const {
    return m_point.x;
}
int Point::y() const {
    return m_point.y;
}
void Point::x(int x) {
    m_point.x = x;
}
void Point::y(int y) {
    m_point.y = y;
}

Point Point::operator-(const Point& other){
  return Point(this->x() - other.x(),
             this->y() - other.y());
}

Point Point::operator+(const Point& other){
  return Point(this->x() + other.x(),
             this->y() + other.y());
}
Point Point::operator*(const Point& other) {
     return Point(this->x() * other.x(),
             this->y() * other.y());
}

Point Point::operator/(const Point& other) {
     return Point(this->x() / other.x(),
             this->y() / other.y());
}


void Point::operator=(const Point& other){
  this->x(other.x());
  this->y(other.y());
}

void Point::operator+=(const Point& other){
  this->x( this->x() + other.x());
  this->y( this->y() + other.y());
}

void Point::operator-=(const Point& other){
  this->x( this->x() - other.x());
  this->y( this->x() - other.y());
}

void Point::operator*=(const Point& other) {
     this->x( this->x() * other.x());
  this->y( this->x() * other.y());
}
void Point::operator/=(const Point& other) {
     this->x( this->x() / other.x());
  this->y( this->x() / other.y());
}



float Point::module() {
    return sqrt(m_point.x*m_point.x + m_point.y*m_point.y);
}

Point Point::normalized() {
    float m = module();
    return Point(m_point.x / m, m_point.y / m);
}

Point Point::operator+(const int& other) {
    return Point(m_point.x + other, m_point.y + other);
}
Point Point::operator-(const int& other) {
    return Point(m_point.x - other, m_point.y - other);
}
Point Point::operator*(const float& other) {
    return Point(m_point.x * other, m_point.y * other);
}
Point Point::operator/(const float& other) {
    return Point(m_point.x / other, m_point.y / other);
}

void Point::operator*=(const float& other) {
    this->x( this->x() * other);
    this->y( this->x() * other);
}

void Point::operator+=(const float& other) {
    this->x( this->x() + other);
    this->y( this->x() + other);
}

void Point::operator-=(const float& other) {
    this->x( this->x() - other);
    this->y( this->x() - other);
}

void Point::operator/=(const float& other) {
    this->x( this->x() / other);
    this->y( this->x() / other);
}

std::ostream & operator<<(std::ostream &os, Point const &m) { 
    return os << "["<< m.x() <<","<<m.y() << "]";
}

// void normalize(Point2d& v){
//   float modulo = sqrt(v.x*v.x+v.y*v.y);
//   v.x /= modulo;
//   v.y /= modulo;
// }
// 
// // Point2d Point2d::operator*(const Point2d& other){
// //   return Point2d(this->y*other.z - this->z*other.y,
// //              this->z*other.x - this->x*other.z,
// //              this->x*other.y - this->y*other.x);
// // }
// 
// Point2d Point2d::operator-(const Point2d& other){
//   return Point2d(this->x - other.x,
//              this->y - other.y);
// }
// 
// Point2d Point2d::operator+(const Point2d& other){
//   return Point2d(this->x + other.x,
//              this->y + other.y);
// }
// 
// void Point2d::operator=(const Point2d& other){
//   this->x = other.x;
//   this->y = other.y;
// }
// 
// void Point2d::operator+=(const Point2d& other){
//   this->x = this->x + other.x;
//   this->y = this->x + other.y;
// }
// 
// void Point2d::operator-=(const Point2d& other){
//   this->x = this->x - other.x;
//   this->y = this->x - other.y;
// }
// 
// 
// void normalize(Point3d& v){
//   float modulo = sqrt(v.x*v.x+v.y*v.y+v.z*v.z);
//   v.x /= modulo;
//   v.y /= modulo;
//   v.z /= modulo;
// }
// 
// Point3d Point3d::operator*(const Point3d& other){
//   return Point3d(this->y*other.z - this->z*other.y,
//              this->z*other.x - this->x*other.z,
//              this->x*other.y - this->y*other.x);
// }
// 
// Point3d Point3d::operator-(const Point3d& other){
//   return Point3d(this->x - other.x,
//              this->y - other.y,
//              this->z - other.z);
// }
// 
// Point3d Point3d::operator+(const Point3d& other){
//   return Point3d(this->x + other.x,
//              this->y + other.y,
//              this->z + other.z);
// }
// 
// void Point3d::operator=(const Point3d& other){
//   this->x = other.x;
//   this->y = other.y;
//   this->z = other.z;
// }
// 
// void Point3d::operator+=(const Point3d& other){
//   this->x = this->x + other.x;
//   this->y = this->x + other.y;
//   this->z = this->x + other.z;
// }
// 
// void Point3d::operator-=(const Point3d& other){
//   this->x = this->x - other.x;
//   this->y = this->x - other.y;
//   this->z = this->x - other.z;
// }
// 
// //Vec4u
// vec4u vec4u::operator-(const vec4u& other){
//   return vec4u(this->x - other.x,
//              this->y - other.y,
//                this->z - other.z,
//                this->w - other.w);
// }
// 
// vec4u vec4u::operator+(const vec4u& other){
//   return vec4u(this->x + other.x,
//              this->y + other.y,
//                this->z + other.z,
//                this->w + other.w);
// }
// 
// void vec4u::operator=(const vec4u& other){
//   this->x = other.x;
//   this->y = other.y;
//   this->z = other.z;
//   this->w = other.w;
// }
// 
// void vec4u::operator+=(const vec4u& other){
//   this->x = this->x + other.x;
//   this->y = this->x + other.y;
//   this->z = this->x + other.z;
//   this->w = this->w + other.w;
// }
// 
// void vec4u::operator-=(const vec4u& other){
//   this->x = this->x - other.x;
//   this->y = this->x - other.y;
//   this->z = this->x - other.z;
//   this->w = this->w - other.w;
// }
