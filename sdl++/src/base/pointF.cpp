#include "base/pointF.hpp"
#include <math.h>

PointF::PointF(float x, float y){
    m_x = x;
    m_y = y;
}
PointF::PointF(Point p) {
    m_x = p.x();
    m_y = p.y();
}

float PointF::x() const {
    return m_x;
}
float PointF::y() const {
    return m_y;
}
void PointF::x(float x) {
    m_x = x;
}
void PointF::y(float y) {
    m_y = y;
}

Point PointF::rounded() {
    return Point( round(m_x), round(m_y));
}

Point PointF::truncated() {
    return Point(m_x,m_y);
}

float PointF::module() {
    return sqrt(m_x*m_x + m_y*m_y);
}

PointF PointF::normalized() {
    float m = module();
    return PointF(m_x / m, m_y / m);
}

// PointF operators
PointF PointF::operator-(const PointF& other){
  return PointF(this->x() - other.x(),
             this->y() - other.y());
}

PointF PointF::operator+(const PointF& other){
  return PointF(this->x() + other.x(),
             this->y() + other.y());
}
PointF PointF::operator*(const PointF& other) {
     return PointF(this->x() * other.x(),
             this->y() * other.y());
}

PointF PointF::operator/(const PointF& other) {
     return PointF(this->x() / other.x(),
             this->y() / other.y());
}


void PointF::operator=(const PointF& other){
  this->x(other.x());
  this->y(other.y());
}

void PointF::operator+=(const PointF& other){
  this->x( this->x() + other.x());
  this->y( this->y() + other.y());
}

void PointF::operator-=(const PointF& other){
  this->x( this->x() - other.x());
  this->y( this->x() - other.y());
}

void PointF::operator*=(const PointF& other) {
     this->x( this->x() * other.x());
  this->y( this->x() * other.y());
}
void PointF::operator/=(const PointF& other) {
     this->x( this->x() / other.x());
  this->y( this->x() / other.y());
}

//Point operators
PointF PointF::operator-(const Point& other){
  return PointF(this->x() - other.x(),
             this->y() - other.y());
}

PointF PointF::operator+(const Point& other){
  return PointF(this->x() + other.x(),
             this->y() + other.y());
}
PointF PointF::operator*(const Point& other) {
     return PointF(this->x() * other.x(),
             this->y() * other.y());
}

PointF PointF::operator/(const Point& other) {
     return PointF(this->x() / other.x(),
             this->y() / other.y());
}
void PointF::operator=(const Point& other){
  this->x(other.x());
  this->y(other.y());
}
void PointF::operator+=(const Point& other){
  this->x( this->x() + other.x());
  this->y( this->y() + other.y());
}

void PointF::operator-=(const Point& other){
  this->x( this->x() - other.x());
  this->y( this->x() - other.y());
}

void PointF::operator*=(const Point& other) {
     this->x( this->x() * other.x());
  this->y( this->x() * other.y());
}
void PointF::operator/=(const Point& other) {
     this->x( this->x() / other.x());
  this->y( this->x() / other.y());
}


// Float operators
PointF PointF::operator+(const float& other) {
    return PointF(m_x + other, m_y + other);
}
PointF PointF::operator-(const float& other) {
    return PointF(m_x - other, m_y - other);
}
PointF PointF::operator*(const float& other) {
    return PointF(m_x * other, m_y * other);
}
PointF PointF::operator/(const float& other) {
    return PointF(m_x / other, m_y / other);
}

void PointF::operator*=(const float& other) {
    this->x( this->x() * other);
    this->y( this->x() * other);
}

void PointF::operator+=(const float& other) {
    this->x( this->x() + other);
    this->y( this->x() + other);
}

void PointF::operator-=(const float& other) {
    this->x( this->x() - other);
    this->y( this->x() - other);
}

void PointF::operator/=(const float& other) {
    this->x( this->x() / other);
    this->y( this->x() / other);
}

std::ostream & operator<<(std::ostream &os, PointF const &m) { 
    return os << "["<< m.x() <<","<<m.y() << "]";
}
