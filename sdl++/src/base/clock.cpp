#include "base/clock.hpp"

Clock::Clock(){
    start();
}

Uint32 Clock::miliseconds() {
    return (SDL_GetTicks() - m_startTime);
}

double Clock::seconds() {
        return miliseconds() / 1000.0;
}

double Clock::restart() {
    double out = seconds();
    start();
    return out;
}

Uint32 Clock::restartMili() {
    Uint32 out = miliseconds();
    start();
    return out;
}


void Clock::start() {
    m_startTime = SDL_GetTicks();
}

Uint32 Clock::ticks() {
    return SDL_GetTicks();
}


void Clock::wait(double seconds) {
    SDL_Delay(seconds * 1000);
}

