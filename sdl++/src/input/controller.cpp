// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "input/controller.hpp"

const std::map<std::string,SDL_GameControllerButton> Controller::m_buttons = {
  {"INVALID", SDL_CONTROLLER_BUTTON_INVALID},
  {"A", SDL_CONTROLLER_BUTTON_A},
  {"B", SDL_CONTROLLER_BUTTON_B},
  {"X", SDL_CONTROLLER_BUTTON_X},
  {"Y", SDL_CONTROLLER_BUTTON_Y},
  {"BACK", SDL_CONTROLLER_BUTTON_BACK},
  {"GUIDE", SDL_CONTROLLER_BUTTON_GUIDE},
  {"START", SDL_CONTROLLER_BUTTON_START},
  {"LEFTSTICK", SDL_CONTROLLER_BUTTON_LEFTSTICK},
  {"RIGHTSTICK", SDL_CONTROLLER_BUTTON_RIGHTSTICK},
  {"LEFTSHOULDER", SDL_CONTROLLER_BUTTON_LEFTSHOULDER},
  {"RIGHTSHOULDER", SDL_CONTROLLER_BUTTON_RIGHTSHOULDER},
  {"DPAD_UP", SDL_CONTROLLER_BUTTON_DPAD_UP},
  {"DPAD_DOWN", SDL_CONTROLLER_BUTTON_DPAD_DOWN},
  {"DPAD_LEFT", SDL_CONTROLLER_BUTTON_DPAD_LEFT},
  {"DPAD_RIGHT", SDL_CONTROLLER_BUTTON_DPAD_RIGHT},
  {"MAX", SDL_CONTROLLER_BUTTON_MAX}
};

const std::map<std::string,SDL_GameControllerAxis> Controller::m_axis = {
  {"INVALID", SDL_CONTROLLER_AXIS_INVALID},
  {"LEFTX", SDL_CONTROLLER_AXIS_LEFTX},
  {"LEFTY", SDL_CONTROLLER_AXIS_LEFTY},
  {"RIGHTX", SDL_CONTROLLER_AXIS_RIGHTX},
  {"RIGHTY", SDL_CONTROLLER_AXIS_RIGHTY},
  {"LT", SDL_CONTROLLER_AXIS_TRIGGERLEFT},
  {"RT", SDL_CONTROLLER_AXIS_TRIGGERRIGHT},
  {"MAX", SDL_CONTROLLER_AXIS_MAX}
};

bool Controller::nonDefaultMappingsAdded = false;

Controller::Controller(int index, Uint16 deadZone){
  if(!nonDefaultMappingsAdded)
    addNonDefaultMappings();
  
  setDeadZone(deadZone);

  m_controller = SDL_GameControllerOpen(index);
  if( m_controller == NULL ) {
    printf( "Warning: Unable to open game controller! SDL Error: %s\n", SDL_GetError() );
  }
  else{
    m_index = index;
    m_joystick = SDL_GameControllerGetJoystick(m_controller);
    //Get controller haptic device
    m_haptic = SDL_HapticOpenFromJoystick( m_joystick );
    if( m_haptic == NULL ) {
      printf( "Warning: Controller does not support haptics! SDL Error: %s\n", SDL_GetError() );
    }
    else {
      //Get initialize rumble
      if( SDL_HapticRumbleInit( m_haptic ) < 0 ) {
        printf( "Warning: Unable to initialize rumble! SDL Error: %s\n", SDL_GetError() );
      }
    }
    
    m_id = SDL_JoystickInstanceID(m_joystick);
  }
  
  
}
Controller::~Controller() {
//     std::cout << "Destructor of controller " << m_id << std::endl;
    if(m_haptic)
        SDL_HapticClose( m_haptic );
    if(m_controller)
        SDL_GameControllerClose(m_controller);
  // No need to close the joystick, the close game controller should handle that
  
  //Test
//   SDL_JoystickClose(m_joystick);
}

int Controller::amount(){
  return SDL_NumJoysticks();
}

int Controller::index() {
    return m_index;
}


std::string Controller::name(){
  const char* joystickname = SDL_JoystickName(m_joystick);
  return joystickname;
}

void Controller::rumble(float strenght, float time){
  SDL_HapticRumblePlay( m_haptic, strenght, time ); // This will return != 0 if it fails, but I have no reason to check
}

void Controller::setDeadZone(Uint16 deadZone) {
    m_deadZone = deadZone;
}

Uint16 Controller::getDeadZone() {
    return m_deadZone;
}

bool Controller::up(){
  return SDL_GameControllerGetButton(m_controller,SDL_CONTROLLER_BUTTON_DPAD_UP);
}
bool Controller::down(){
  return SDL_GameControllerGetButton(m_controller,SDL_CONTROLLER_BUTTON_DPAD_DOWN);
}
bool Controller::left(){
  return SDL_GameControllerGetButton(m_controller,SDL_CONTROLLER_BUTTON_DPAD_LEFT);
}
bool Controller::right(){
  return SDL_GameControllerGetButton(m_controller,SDL_CONTROLLER_BUTTON_DPAD_RIGHT);
}
bool Controller::A(){
  return SDL_GameControllerGetButton(m_controller,SDL_CONTROLLER_BUTTON_A);
}
bool Controller::B(){
  return SDL_GameControllerGetButton(m_controller,SDL_CONTROLLER_BUTTON_B);
}
bool Controller::X(){
  return SDL_GameControllerGetButton(m_controller,SDL_CONTROLLER_BUTTON_X);
}
bool Controller::Y(){
  return SDL_GameControllerGetButton(m_controller,SDL_CONTROLLER_BUTTON_Y);
}
bool Controller::Back(){
  return SDL_GameControllerGetButton(m_controller,SDL_CONTROLLER_BUTTON_BACK);
}
bool Controller::Guide(){
  return SDL_GameControllerGetButton(m_controller,SDL_CONTROLLER_BUTTON_GUIDE);
}
bool Controller::Start(){
  return SDL_GameControllerGetButton(m_controller,SDL_CONTROLLER_BUTTON_START);
}
bool Controller::LStick(){
  return SDL_GameControllerGetButton(m_controller,SDL_CONTROLLER_BUTTON_LEFTSTICK);
}
bool Controller::RStick(){
  return SDL_GameControllerGetButton(m_controller,SDL_CONTROLLER_BUTTON_RIGHTSTICK);
}
bool Controller::LShoulder(){
  return SDL_GameControllerGetButton(m_controller,SDL_CONTROLLER_BUTTON_LEFTSHOULDER);
}
bool Controller::RShoulder(){
  return SDL_GameControllerGetButton(m_controller,SDL_CONTROLLER_BUTTON_RIGHTSHOULDER);
}

bool Controller::isButtonDown(SDL_GameControllerButton button){
  return SDL_GameControllerGetButton(m_controller,button);
}

double Controller::getAxis(SDL_GameControllerAxis axis){
     Sint16 v = SDL_GameControllerGetAxis(m_controller, axis);
     if( abs(v) > m_deadZone)
        return v / 32768.0;
     return 0;
}

double Controller::LX(){
  return getAxis(SDL_CONTROLLER_AXIS_LEFTX);
}
double Controller::LY(){
  return getAxis(SDL_CONTROLLER_AXIS_LEFTY);
}
double Controller::RX(){
  return getAxis(SDL_CONTROLLER_AXIS_RIGHTX);
}
double Controller::RY(){
  return getAxis(SDL_CONTROLLER_AXIS_RIGHTY);
}
double Controller::LT(){
  return getAxis(SDL_CONTROLLER_AXIS_TRIGGERLEFT);
}
double Controller::RT(){
  return getAxis(SDL_CONTROLLER_AXIS_TRIGGERRIGHT);
}

SDL_JoystickID Controller::id() {
    return m_id;
}


void Controller::addNonDefaultMappings(){
  //There are some mappings that are non default for standard SDL

  //PS3 Wireless Controller
  std::string ps3_wireless =  "050000004c0500006802000000010000,Sony Computer Entertainment Wireless Controller,platform:Linux,a:b14,b:b13,x:b15,y:b12,back:b0,start:b3,guide:b16,leftshoulder:b10,rightshoulder:b11,leftstick:b1,rightstick:b2,leftx:a0,lefty:a1,rightx:a2,righty:a3,lefttrigger:b8,righttrigger:b9,dpup:b4,dpleft:b7,dpdown:b6,dpright:b5,";
    addMapping(ps3_wireless);
}

void Controller::addMapping(std::string mapping) {
    SDL_GameControllerAddMapping(mapping.c_str());
}
void Controller::addMappingFromFile(std::string filepath) {
    SDL_GameControllerAddMappingsFromFile(filepath.c_str());
}


SDL_GameControllerButton Controller::buttonFromName(std::string name) {
    auto it = m_buttons.find(name);
    if(it == m_buttons.end()){
        std::cerr << "Button not found " << name << std::endl;
        return SDL_CONTROLLER_BUTTON_INVALID;
    }
    return it->second;
}


SDL_GameControllerAxis Controller::axisFromName(std::string name) {
    auto it = m_axis.find(name);
    if(it == m_axis.end()){
        std::cerr << "Axis not found " << name << std::endl;
        return SDL_CONTROLLER_AXIS_INVALID;
    }
    return it->second;
}

std::string Controller::nameFromAxis(SDL_GameControllerAxis axis) {
    for(const auto& kv : m_axis){
        if(kv.second == axis)
            return kv.first;
    }
    return "UNKNOWN";
}

std::string Controller::nameFromButton(SDL_GameControllerButton btn) {
     for(const auto& kv : m_buttons){
        if(kv.second == btn)
            return kv.first;
    }
    return "UNKNOWN";
}

