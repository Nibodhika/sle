// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include <iostream>
#include "audio/chunk.hpp"

Chunk::Chunk(std::string path){
  if(path != "")
    open(path);
}
Chunk::~Chunk(){
  Mix_FreeChunk(m_chunk);
}
bool Chunk::open(std::string path){
    
    if(m_chunk)
        Mix_FreeChunk(m_chunk);
    
    m_chunk = Mix_LoadWAV(path.c_str());
    if(!m_chunk){
       std::cout << "Unable to load audio! SDL Error: " <<  SDL_GetError()  << std::endl;
       return false;
   }
    return true;
}

Mix_Chunk* Chunk::chunk(){
  return m_chunk;
}
