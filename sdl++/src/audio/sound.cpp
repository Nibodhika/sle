// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include <iostream>
#include "audio/sound.hpp"

Sound::Sound(Chunk* chunk, int channel){
  m_chunk = chunk;
  m_channel = channel;
}
Sound::~Sound(){

}
void Sound::play(int loops){
  m_channel = Mix_PlayChannel(m_channel, m_chunk->chunk(), loops);
}
void Sound::pause(){
  if(m_channel != -1)
    Mix_Pause(m_channel);
}
void Sound::stop(){
  if(m_channel != -1)
    Mix_HaltChannel(m_channel);
}
void Sound::resume(){
  if(m_channel != -1)
    Mix_Resume(m_channel);
}
int Sound::channel(){
  return m_channel;
}
void Sound::setChannel(int channel){
  if(isPlaying())
    stop();
  m_channel = channel;
}
void Sound::fadeOut(int ms){
  Mix_FadeOutChannel(m_channel, ms);
}
void Sound::fadeIn(int ms, int loops){
  Mix_FadeInChannel(m_channel, m_chunk->chunk(), loops, ms);
}
bool Sound::isPlaying(){
  if(m_channel != -1){
    if(Mix_Playing(m_channel) == 0)
      return false;
    Mix_Chunk* other = Mix_GetChunk(m_channel);
    if(m_chunk->chunk() == other)
      return true;
  }
  return false;
}
bool Sound::isPaused(){
  if(m_channel != -1){
    if(Mix_Paused(m_channel) == 0)
      return false;
    Mix_Chunk* other = Mix_GetChunk(m_channel);
    if(m_chunk->chunk() == other)
      return true;
  }
  return false;
}

int Sound::getVolume() {
    //Passing negative values to Mix_Volume returns the current volume
    return Mix_Volume(m_channel, -1);
}

int Sound::setVolume(unsigned int volume) {
    return Mix_Volume(m_channel, volume);
}

Chunk* Sound::getChunk(){
  return m_chunk;
}

void Sound::setChunk(Chunk* chunk) {
    if(isPlaying())
        stop();
    m_chunk = chunk;
}


//Static functions
void Sound::pauseAll(){
  Mix_Pause(-1);
}
void Sound::resumeAll(){
  Mix_Resume(-1);
}
void Sound::fadeOutAll(int ms){
  Mix_FadeOutChannel(-1, ms);
}
void Sound::stopAll(){
  Mix_HaltChannel(-1);
}

bool Sound::simpleInit() {
    int result = 0;
     int flags = MIX_INIT_MP3;
      if (flags != (result = Mix_Init(flags))) {
        std::cout << "Could not initialize mixer (result: " <<  result << ")" << std::endl;
        std::cout << "Mix_Init: " <<  Mix_GetError() << std::endl;
        return false;
    }
    
    Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2 /* stereo */, 1024 /* Bytes used per output sample. */);
    return true;
}
