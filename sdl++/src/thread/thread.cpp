// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
#include "thread/thread.hpp"

Thread::Thread(SDL_ThreadFunction fn, std::string name, void* data){
 m_fn = fn;
 m_name = name;
 m_data = data;
 m_thread = nullptr;
}

void Thread::start() {
       m_thread = SDL_CreateThread(m_fn,m_name.c_str(),m_data);
}

int Thread::wait() {
    int out;
    SDL_WaitThread(m_thread, &out);
    return out;
}

std::string Thread::getName() {
    return m_name;
}


SDL_Thread* Thread::thread(){
    return m_thread;
}
