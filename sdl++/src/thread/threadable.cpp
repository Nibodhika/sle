#include "thread/threadable.hpp"

#include "thread/thread.hpp"

int Threadable::doRun(void* threadable) {
    Threadable* t = (Threadable*) threadable;
    t->run();
    return 0;
}


Threadable::Threadable(std::string name){
    m_thread = new Thread(Threadable::doRun, name.c_str(), this);
}

Threadable::~Threadable() {

}

void Threadable::start() {
    m_thread->start();
}
