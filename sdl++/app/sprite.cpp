#include <iostream>

#include "graphics/window.hpp"
#include "graphics/texture.hpp"
#include "input/keyboard.hpp"
#include "graphics/sprite.hpp"


int main() {

  //Creating the window. This will start most of th sdl stuff, and create a Window::main_window and Window::main_renderer
  Window main_window("title", 800,600);
  
  //Opening an image as a texture
  std::string imagePath = "../../../resources/SDL_logo.png";
  Texture t(imagePath);
  Image i(&t);
  Sprite s(&i);
  s.setSize(Point(200,200));
  s.setPos(Point(100,100));

  float speed = 5;

  SDL_Event e;
  bool alive = true;  
  //Main loop
  while(alive){
    Point move(0,0);
    //Event handler, using native SDL, no wrapping here
    while( SDL_PollEvent( &e ) != 0 ) {
      //User requests quit
      if( e.type == SDL_QUIT ) {
        printf("closing\n");
        alive = false;
      }
      //User presses a key
      else if( e.type == SDL_KEYDOWN ){
        //Select surfaces based on key press
        switch( e.key.keysym.sym ) {
        case SDLK_ESCAPE:
          printf("escaping\n");
          alive = false;
          break;
        }
      }

    }


    if (Keyboard::isKeyDown(SDL_SCANCODE_DOWN) || Keyboard::isKeyDown(SDL_SCANCODE_S)) {
      move.y(move.y() + speed);
    }
    else if (Keyboard::isKeyDown(SDL_SCANCODE_UP) || Keyboard::isKeyDown(SDL_SCANCODE_W)) {
      move.y(move.y() - speed);
    }
 
    if (Keyboard::isKeyDown(SDL_SCANCODE_RIGHT) || Keyboard::isKeyDown(SDL_SCANCODE_D)){
      move.x(move.x() + speed);
    }
    else if(Keyboard::isKeyDown(SDL_SCANCODE_LEFT) || Keyboard::isKeyDown(SDL_SCANCODE_A)) {
      move.x(move.x() - speed);
    }
    s.move(move);
    
    if ( Keyboard::isKeyDown(SDL_SCANCODE_E)){
      s.rotate(speed);
    }
    else if(Keyboard::isKeyDown(SDL_SCANCODE_Q)) {
      s.rotate(speed * -1);
    }

    
    
    //First clear the window
    main_window.clear();
    //Draw the texture occupying the entire screen
    main_window.draw(&s);
    //Display the screen
    main_window.display();
  }
  
  
  //No need to worry about closing SDL, it will automatically close whenever the main_window is destroyed

  return 0;
}
