#include <iostream>

#include <SDL.h>

#include "thread/thread.hpp"
#include "thread/threadable.hpp"


int func(void* data){
    while(true) {
        std::cout << "This is the C-style thread" << std::endl;
        SDL_Delay(500);
    }
}

class A : public Threadable{
protected:
    void run(){
        while(true) {
            std::cout << "This is the class oriented thread" << std::endl;
            SDL_Delay(500);
        }
    }
};

int main(){
    //There's no need for a window in this program
  SDL_Init(SDL_INIT_EVERYTHING);
  
  //C-style thread
  Thread thread(func,"thread_name",nullptr);
  thread.start(); 
  
  //Class oriented thread
  A a;
  a.start();
  
  //Run for 5 seconds
  SDL_Delay(5000);
  
}
