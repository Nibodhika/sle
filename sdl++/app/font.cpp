#include <iostream>

#include "graphics/window.hpp"
#include "graphics/font.hpp"
#include "graphics/text.hpp"

int main(){
  std::string font_path = "/home/nibodhika/Downloads/Kenney Game Assets (version 40)/Fonts/KenPixel Blocks.ttf";
  int font_size = 64;

  Font f;
  bool could = f.open(font_path,font_size);
  std::cout << "could open font? " << could << std::endl;

  Window main_window("title", 800,600);

  std::string textStr = "Isso eh uma c cedilha: ?";
  Color textColor = { 255, 128, 128 };

  Text text(&f,textStr,textColor);
  text.setPos(100,100);
  // text.scale(0.75);
//   text.scaleToSize(300,100);
  // text.setSize(500,100);

//   std::cout << "as text" << std::endl;
//   text.sdlSrcRect();
//   std::cout << "as sprite" << std::endl;
//   Sprite* sprite = &text;
//   sprite->sdlSrcRect();
  
  SDL_Event e;
  bool alive = true;  
  //Main loop
  while(alive) {
    Point move(0,0);
    //Event handler, using native SDL, no wrapping here
    while( SDL_PollEvent( &e ) != 0 ) {
      //User requests quit
      if( e.type == SDL_QUIT ) {
        printf("closing\n");
        alive = false;
      }
      //User presses a key
      else if( e.type == SDL_KEYDOWN ){
        //Select surfaces based on key press
        switch( e.key.keysym.sym ) {
        case SDLK_ESCAPE:
          printf("escaping\n");
          alive = false;
          break;
        case SDLK_SPACE:
             text.changeText("I can change");
            break;
        case SDLK_UP:
                text.scaleToSize(600,200);
            break;
        }
      }

    }
    
    //First clear the window
    main_window.clear();
    //Display the text
    main_window.draw(&text);
    // SDL_RenderCopy(main_window.renderer(),text.sdlTexture(),NULL, text.sdlDstRect());
    //Display the screen
    main_window.display();
  }

  return 0;
}
