#include <iostream>
#include <SDL.h>

int main(){
    SDL_Init(SDL_INIT_EVERYTHING);

    bool alive = true;
    SDL_Event e;
    while(alive){
        while( SDL_PollEvent( &e ) != 0 ){
            if( e.type == SDL_QUIT ) {
                std::cout << "closing" << std::endl;
                alive = false;
            }
            else if(e.type == SDL_CONTROLLERDEVICEADDED){
                std::cout << "Controller " << e.cdevice.which << " added" << std::endl;
            }
            else if(e.type == SDL_CONTROLLERDEVICEREMOVED){
                std::cout << "Controller " << e.cdevice.which << " removed" << std::endl;
            }
        
            else if(e.type == SDL_JOYDEVICEADDED){
                std::cout << "Joy " << e.jdevice.which << " added" << std::endl;
            }
            else if(e.type == SDL_JOYDEVICEREMOVED){
                std::cout << "Joy " << e.jdevice.which << " removed" << std::endl;
            }
        }
    }
      
    return 0;
};



// #include <iostream>
// #include <vector>
// #include "graphics/window.hpp"
// #include "input/controller.hpp"
// #include "graphics/sprite.hpp"
// 
// void getControllers(std::vector<Controller*>& controllers){
//     controllers.clear();
//     int controller_amount = Controller::amount();
//     std::cout << "I have: " << controller_amount << " controllers" << std::endl;
// 
// 
//     for(int i = 0; i < controller_amount; i++){
//         Controller* c = new Controller(i);
//         //Identify the controller by name and make it rumble if it can
//         std::cout << "This is: " <<  c->id() << " ( " << c->name() << " )"<< std::endl;
//         c->rumble(0.75,500);
//         controllers.push_back(c);
//         //Wait a bit so not all controllers rumble at the same time
//         SDL_Delay(1000);
//     }
// }
// 
// int main(){
//   Window main_window("title", 800,600);
// 
// 
// //   std::cout << " joystic subsystem is: " << SDL_INIT_EVERYTHING << std::endl;
// // 
// // 
// //   for(int i = 0; i < 10; i++){
// //   SDL_Haptic* haptic =  SDL_HapticOpen(i);
// //   std::cout << "haptic " << i << " is: " << haptic << std::endl;
// //     // Initialize simple rumble
// //     if (SDL_HapticRumbleInit( haptic ) != 0)
// //         std::cout << "Unable to init" << std::endl;
// //     //return -1;
// // 
// //     // Play effect at 50% strength for 2 seconds
// //     if (SDL_HapticRumblePlay( haptic, 0.5, 2000 ) != 0)
// //         std::cout << "Unable to rumble" << std::endl;
// // //     return -1;
// //     else
// //         SDL_Delay( 2000 );
// //   }
// 
// 
//   std::vector<Controller*> controllers;
//   getControllers(controllers);
// 
//   std::string imagePath = "../../../resources/SDL_logo.png";
//   Texture t(imagePath);
//   Sprite s(&t);
//   // s.setSize(Point2d(200,200));
// 
//   float speed = 5;
//   SDL_Event e;
//   bool alive = true;
//   while(alive){
//     Point move(0,0);
//     while( SDL_PollEvent( &e ) != 0 ){
//       if( e.type == SDL_QUIT ) {
//         std::cout << "closing" << std::endl;
//         alive = false;
//       }
//       else if( e.type == SDL_KEYDOWN ){
//         //Select surfaces based on key press
//         switch( e.key.keysym.sym ) {
//         case SDLK_ESCAPE:
//           std::cout << "escaping" << std::endl;
//           alive = false;
//           break;
//         }
//       }
//       else if(e.type == SDL_CONTROLLERDEVICEADDED){
//           std::cout << "Controller " << e.cdevice.which << " added" << std::endl;
//       }
//       else if(e.type == SDL_CONTROLLERDEVICEREMOVED){
//           std::cout << "Controller " << e.cdevice.which << " removed" << std::endl;
//       }
//       
//       else if(e.type == SDL_JOYDEVICEADDED){
//           std::cout << "Joy " << e.jdevice.which << " added" << std::endl;
//       }
//       else if(e.type == SDL_JOYDEVICEREMOVED){
//           std::cout << "Joy " << e.jdevice.which << " removed" << std::endl;
//       }
//       
//       else if(e.type == SDL_CONTROLLERBUTTONDOWN){
//             std::cout << "C Button " << Controller::nameFromButton( (SDL_GameControllerButton) e.cbutton.button) << " pressed on " << e.cbutton.which << std::endl;
//       }
//       else if(e.type == SDL_CONTROLLERBUTTONUP){
//             std::cout << "C Button " << Controller::nameFromButton((SDL_GameControllerButton) e.cbutton.button) << " released on " << e.cbutton.which << std::endl;
//       }
//       
//       else if(e.type == SDL_CONTROLLERAXISMOTION){
//             std::cout << "axis " << Controller::nameFromAxis( (SDL_GameControllerAxis) e.caxis.axis) << " " << e.caxis.value << " on " << e.caxis.which << std::endl;
//       }
//       
//       
// //       else if(e.type == SDL_JOYBUTTONDOWN){
// //             std::cout << "J Button " << e.jbutton.button << " pressed on " << e.jbutton.which << std::endl;
// //       }
// //       else if(e.type == SDL_JOYBUTTONUP){
// //             std::cout << "J Button " << e.jbutton.button << " released on " << e.jbutton.which << std::endl;
// //       }
//       
//     }
// 
//     if(controllers.size() > 0){
//       move.x( controllers[0]->getAxis(SDL_CONTROLLER_AXIS_LEFTX)*speed );
//       move.y(controllers[0]->getAxis(SDL_CONTROLLER_AXIS_LEFTY)*speed);
//     }
//     else{
// //       std::cout << "0 controllers detected" << std::endl;
//     }
// 
//     s.move(move);
// 
//     for(int i = 0; i < controllers.size(); i++){
//       if(controllers[i]->Start()){
//         std::cout << "start" << std::endl;
//         alive = false;
//       }
//       if(controllers[i]->Back()){
//         std::cout << "Select" << std::endl;
// 
//         getControllers(controllers);
// 
//       }
//       if(controllers[i]->Guide()){
//         std::cout << "Guide" << std::endl;
//       }
// //       if(controllers[i]->A()){
// //         std::cout << "A" << std::endl;
// //       }
// //       if(controllers[i]->B()){
// //         std::cout << "B" << std::endl;
// //       }
// //       if(controllers[i]->X()){
// //         std::cout << "X" << std::endl;
// //       }
// //       if(controllers[i]->Y()){
// //         std::cout << "Y" << std::endl;
// //       }
// 
//       // std::cout << controllers[i]->getAxis(SDL_CONTROLLER_AXIS_LEFTX) << "x" << controllers[i]->getAxis(SDL_CONTROLLER_AXIS_LEFTY) << std::endl ;
//     }
// 
//   //Clear the window
//   main_window.clear();
//   main_window.draw(&s);
//   //Display the screen
//   main_window.display();
//   }
// 
// }
