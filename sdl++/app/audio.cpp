#include <iostream>
#include "audio/sound.hpp"

int main(int argc, char **argv) {
//     int result = 0;
//     int flags = MIX_INIT_MP3;

    std::string file = "../../../resources/music.ogg";
    std::string file2 = "../../../resources/music2.ogg";

    if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
        std::cout << "Failed to init SDL" << std::endl;
        exit(1);
    }

//     if (flags != (result = Mix_Init(flags))) {
//         std::cout << "Could not initialize mixer (result: " <<  result << ")" << std::endl;
//         std::cout << "Mix_Init: " <<  Mix_GetError() << std::endl;
//         exit(1);
//     }
//     
//     Mix_OpenAudio(22050, AUDIO_S16SYS, 2, 640);

    Sound::simpleInit();

    // Mix_PlayMusic(music, 1);
    std::cout << "Will load audio" << std::endl;    
    Chunk chunk(file);
    Sound s(&chunk);
    Chunk chunk2(file2);
    Sound s2(&chunk2);
    std::cout << "Audio loaded" << std::endl;    
    
    std::cout << "Will play audio 1" << std::endl;    
    s.play(-1);
    int delay = 5000;
    // while (!SDL_QuitRequested()) {
      SDL_Delay(delay);
      std::cout << "Will play audio 2" << std::endl;
      s.pause();
      s2.play(-1);
      SDL_Delay(delay);
      std::cout << "Will play both" << std::endl;
      s.resume();
      SDL_Delay(delay);
      std::cout << "Will pause both" << std::endl;
      Sound::pauseAll(); //Pauses all channels
      SDL_Delay(delay);
      std::cout << "Will resume both" << std::endl;
      Sound::resumeAll();
      SDL_Delay(delay);
      std::cout << "Will fade" << std::endl;
      // fade out all channels to finish 3 seconds from now
      Sound::fadeOutAll(3000); 
      SDL_Delay(delay);
    // }

    return 0;
}

