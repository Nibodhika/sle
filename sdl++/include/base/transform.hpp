#ifndef TRANSFORM_HPP
#define TRANSFORM_HPP

#include <base/pointF.hpp>

/**
 *  @ingroup Base
 */

//! A Transformation

/*!
  A Transform contains a position, a scale and a rotation.
  All of them are returned as a reference in order to be directly editied in future classes.
  
  In the future this will also provide a transform matrix to be used with OpenGL, and perhaps the values will be stored in that matrix to speed things up.
 */
class Transform{
public:
    //! Default constructor
    /*!
        Initializes position to 0,0, scale to 1,1 and rotation to 0
    */
    Transform();
    //! Returns the position
    /*!
        \returns The position.
    */
    PointF position() const;
    void setPosition(PointF p);
    //! Returns the rotation
    /*!
        \returns The rotation (angle is in degrees).
    */
    float rotation() const;
    void setRotation(float v);
    //! Returns the scale
    /*!
        \returns The scale.
    */
    PointF scale() const;
    void setScale(PointF p);
    
    void move(float x, float y);
    void move(PointF delta);
    void rotate(float angle);
    //!Scales the drawable
    /*!
      \param x Scale factor in X axis
      \param y Scale factor in Y axis
     */
    void scale(float x, float y);
    //!Scales uniformingly
    /*!
      Same as calling scale(delta,delta)
      \param delta Scale factor
      \sa scale
     */
    void scale(float delta);
    void scale(PointF delta);
    
    //! 
    /*!
        
    */
    Transform operator*(const Transform& other);
    //! 
    /*!
         
    */
    void operator*=(const Transform& other);
    
    
protected:
    //! Position
    PointF m_pos;
    //! Rotation
    float m_rotation;
    //! Scale
    PointF m_scale;
};

#endif

