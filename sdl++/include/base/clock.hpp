#ifndef CLOCK_HPP
#define CLOCK_HPP

#include <SDL/SDL.h>

//! A simple clock class to manage time
/*!
    
 */
class Clock{
public:
    //! Default constructor
    Clock();
    //! Starts the clock, this gets called on the constructor
    void start();
    //! Returns time since start in seconds
    double seconds();
    //! Returns time since start in miliseconds
    Uint32 miliseconds();
    //! Restarts the clock and returns the current seconds
    double restart();
    //! Restarts the clock and returns the current miliseconds
    Uint32 restartMili();
    //! A Wrapper arround SDL_GetTicks
    static Uint32 ticks();
    //! A Wrapper arround SDL_Delay converting from seconds
    static void wait(double seconds);
    //! A Wrapper arround SDL_Delay
    static void waitMili(Uint32 seconds);
    
protected:
    //! Time when constructor/start/restart was called
    Uint32 m_startTime;
};

#endif
