#include "base/color.hpp"
#include "base/point.hpp"
#include "base/rect.hpp"
#include "base/transform.hpp"
#include "base/clock.hpp"

/** @defgroup Base Base
 *  @brief This group contains all classes that are generic and will be used in several places
 */
