#ifndef COLOR_HPP
#define COLOR_HPP

#include <SDL.h>

/**
 *  @ingroup Base
 */

//! An RGBA color

/*!
  The Color class is a simple class to represent a color capable of transparency.
  It uses the RGBA color scale and the SDL_Color as a backend.
 */

class Color{
public:
    //! Default constructor
    /*!
        \param r Red component
        \param g Green component
        \param b Blue component
        \param a Alpha component
    */
    Color(Uint8 r = 0, Uint8 g = 0, Uint8 b = 0, Uint8 a = 255);
    
    //! Returns a pointer to the native SDL_Color
    /*!
        \returns The native SDL_Color
    */
    SDL_Color* color();
    
    //! Red component
    /*!
        \returns Red component of the color
    */
    Uint8 r() const;
    
    //! Green component
    /*!
        \returns Green component of the color
    */
    Uint8 g() const;
    
    //! Blue component
    /*!
        \returns Blue component of the color
    */
    Uint8 b() const;
    
    //! Alpha component
    /*!
        \returns Alpha component of the color
    */
    Uint8 a() const;
    
    //! Sets the Red component
    /*!
        \param value new value for the Red component
    */
    void r(Uint8 value);
     //! Sets the Green component
    /*!
        \param value new value for the Green component
    */
    void g(Uint8 g);
    //! Sets the Blue component
    /*!
        \param value new value for the Blue component
    */
    void b(Uint8 value);
     //! Sets the Alpha component
    /*!
        \param value new value for the Alpha component
    */
    void a(Uint8 value);
    
    
    static Color White;
    static Color Black;
    static Color Red;
    static Color Green;
    static Color Blue;
    
protected:
    //! Native SDL_Color
    SDL_Color m_color;
};

#endif
