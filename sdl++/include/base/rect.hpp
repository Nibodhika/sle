#ifndef RECT_HPP
#define RECT_HPP
#include <vector>

#include <string>
#include "base/point.hpp"
#include <SDL.h>


/**
 *  @ingroup Base
 */

//! An integer rectangle

/*!
  The Rect class represents a rectangle expressed in integers, it's a wrapper around SDL_Rect.
 */
class Rect{
public:
    //! Integer constructor
    /*!
      \param x X position
      \param y Y position
      \param w Width
      \param h Height
    */
    Rect(int x = 0, int y = 0, int w = 0, int h = 0);
    //! Point constructor
    /*!
      \param pos Position
      \param size Size, x is Width, y is Height
    */
    Rect(Point pos, Point size);
    
    //! Vector constructor
    /*!
      If vector is smaller than 4 the remaining values are set to 0, if larger than 4 the remaining are ignored
      \param vector A vector containing x,y,width,height
    */
    Rect(std::vector<int> vector);

    //! Sets 4 parameters from a vector
    /*!
      If vector is smaller than 4 the remaining values are not changed, if larger than 4 the remaining are ignored
      \param vector A vector containing x,y,width,height
    */
    void fromVector(std::vector<int> vector);
    //! If size is 0
    /*!
      \returns Returns true if width and height are 0.
    */
    bool isNull();
    //! Returns a pointer to the native SDL_Rect
    /*!
        \returns The native SDL_Rect
    */
    SDL_Rect* rect();
    //! X component
    /*!
        \returns X component of the rect
    */
    int x() const;
    //! Y component
    /*!
        \returns Y component of the rect
    */
    int y() const;
    //! Width
    /*!
        \returns Width of the rect
    */
    int w() const;
    //! Height
    /*!
        \returns Height of the rect
    */
    int h() const;
    //! Sets the X component
    /*!
        \param value X component of the rect
    */
    void x(int value);
    //! Sets the Y component
    /*!
        \param value Y component of the rect
    */
    void y(int value);
    //! Sets the Width
    /*!
        \param value Width of the rect
    */
    void w(int value);
    //! Sets the Height
    /*!
        \param value Height of the rect
    */
    void h(int value);
    //! Position as a Point
    /*!
        \returns Position as a Point
    */
    Point pos();
    //! Sets the position
    /*!
        \param x X position
        \param y Y position
    */
    void setPos(int x, int y);
    //! Sets the position from a Point
    /*!
        \param pos Position as a Point
    */
    void setPos(Point pos);
    //! Sizeas a Point
    /*!
        \returns Size as a Point
    */
    Point size();
    //! Sets the size
    /*!
        \param w Width
        \param h Height
    */
    void setSize(int w, int h);
    //! Sets the size from a Point
    /*!
        \param size Size as a Point, x component is Width, h component is Height
    */
    void setSize(Point size);
    //! Sets the scale
    /*!
        \param factor The size will be multiplied by factor
    */
    void scale(float factor);
    //! Scales to Width
    /*!
        \param width Sets width to width and scales height without distorting
    */
    void scaleToWidth(float width);
    //! Scales to Height
    /*!
        \param height Sets height to height and scales width without distorting
    */
    void scaleToHeight(float height);
    //! Scales to the size
    /*!
        Attempts to scale to width or height, whichever produces a smaller factor and scales uniformly the other
        \param width Suggested width
        \param height Suggested height
    */
    void scaleToSize(float width, float height);
    //! Scales to the size
    /*!
        Same as scaleToSize but receiving a Point as a parameter
        \param size Suggested size
        \sa scaleToSize
    */
    void scaleToSize(Point size); //Scales uniformly to the wanted size
    
    //! Moves the rect by a x and y
    /*!
        \param x Displacement in X
        \param y Displacement in Y
    */
    void moveBy(const int x, const int y);
    //! Moves the rect by a delta
    /*!
        \param delta Moves the rect by delta
    */
    void moveBy(const Point& delta);
    //! Changes the rect position
    /*!
        \param pos Changes the position to pos
    */
    void moveTo(const Point& pos);
    
    //! Gets the rect as a string
    /*!
        \returns The rectangle as a string separated by spaces, for example "1 2 3 4" for a rect at 1,2 with size 3,4
        \sa fromString
    */
    std::string toString();
    //! Sets the values from a string
    /*!
        \param values A string in the format "x y w h"
        \sa toString
    */
    void fromString(std::string values);
  
     friend std::ostream& operator<<(std::ostream &os, Rect const &m);
    
protected:
    //! Native SDL_Rect
    SDL_Rect m_rect;
};

#endif
