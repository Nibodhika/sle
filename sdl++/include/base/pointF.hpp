#ifndef POINT_F_HPP
#define POINT_F_HPP

#include <iostream>
#include "base/point.hpp" //In the future there should be a way to use both together

//! A 2D Floating point position

/*!
  The PointF class represents a 2 dimensional point expressed in floating point precition.
 */
class PointF {
public:
    //! Default constructor
    /*!
        \param x X component of the point
        \param y Y component of the point
    */
    PointF(float x=0, float y=0);
    //! Point constructor
    /*!
        \param p Point to copy
    */
    PointF(Point p);
    
    //! X component
    /*!
        \returns X component of the point
    */
    float x() const;
    //! Y component
    /*!
        \returns Y component of the point
    */
    float y() const;
    
    //! Sets the X component
    /*!
        \param value X component of the point
    */
    void x(float value);
    //! Sets the Y component
    /*!
        \param value Y component of the point
    */
    void y(float value);
    
    //! Module of the point
    /*!
        \returns The module of the point \f$\sqrt{x^2+y^2}\f$.
    */
    float module();
    
    //! Normalized point
    /*!
        \returns A new point with each component divided by the module
    */
    PointF normalized();
    
    Point rounded();
    Point truncated();
   
    /** @name PointF Operators
    *  Operators for + - *  / and = for other point 
    */
    ///@{
    PointF operator-(const PointF& other);
    PointF operator+(const PointF& other);
    PointF operator*(const PointF& other);
    PointF operator/(const PointF& other);
    void operator=(const PointF& other);
    void operator+=(const PointF& other);
    void operator-=(const PointF& other);
    void operator*=(const PointF& other);
    void operator/=(const PointF& other);
    ///@}
    
    /** @name Point Operators
    *  Operators for + - *  / and = for other point 
    */
    ///@{
    PointF operator-(const Point& other);
    PointF operator+(const Point& other);
    PointF operator*(const Point& other);
    PointF operator/(const Point& other);
    void operator=(const Point& other);
    void operator+=(const Point& other);
    void operator-=(const Point& other);
    void operator*=(const Point& other);
    void operator/=(const Point& other);
    ///@}
    
     /** @name Number Operators
    *  Operators for + - * / for other numbers, this get applied to both components of the point
    */
    ///@{
    PointF operator+(const float& other);
    PointF operator-(const float& other);
    PointF operator/(const float& other);
    PointF operator*(const float& other);
    void operator+=(const float& other);
    void operator-=(const float& other);
    void operator*=(const float& other);
    void operator/=(const float& other);
     ///@}
    
    friend std::ostream& operator<<(std::ostream &os, PointF const &m);
    
protected: 
    float m_x,m_y;
};

#endif
