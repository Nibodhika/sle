#ifndef POINT_HPP
#define POINT_HPP

#include <iostream>
#include <SDL.h>

/**
 *  @ingroup Base
 */

//! A 2D Integer point

/*!
  The Point class represents a 2 dimensional point expressed in integers, it's a wrapper around SDL_Point.
 */
class Point {
public:
    //! Default constructor
    /*!
        \param x X component of the point
        \param y Y component of the point
    */
    Point(int x=0, int y=0);
    
    //! Returns a pointer to the native SDL_Point
    /*!
        \returns The native SDL_Point
    */
    SDL_Point* point();
    
    //! X component
    /*!
        \returns X component of the point
    */
    int x() const;
    //! Y component
    /*!
        \returns Y component of the point
    */
    int y() const;
    
    //! Sets the X component
    /*!
        \param value X component of the point
    */
    void x(int value);
    //! Sets the Y component
    /*!
        \param value Y component of the point
    */
    void y(int value);
    
    //! Module of the point
    /*!
        \returns The module of the point \f$\sqrt{x^2+y^2}\f$.
    */
    float module();
    
    //! Normalized point
    /*!
        \returns A new point with each component divided by the module
    */
    Point normalized();
    
   
    /** @name Point Operators
    *  Operators for + - *  / and = for other point 
    */
    ///@{
    Point operator-(const Point& other);
    Point operator+(const Point& other);
    Point operator*(const Point& other);
    Point operator/(const Point& other);
    void operator=(const Point& other);
    void operator+=(const Point& other);
    void operator-=(const Point& other);
    void operator*=(const Point& other);
    void operator/=(const Point& other);
    ///@}
    
     /** @name Number Operators
    *  Operators for + - * / for other numbers, this get applied to both components of the point
    */
    ///@{
    Point operator+(const int& other);
    Point operator-(const int& other);
    Point operator/(const float& other);
    Point operator*(const float& other);
    void operator+=(const float& other);
    void operator-=(const float& other);
    void operator*=(const float& other);
    void operator/=(const float& other);
     ///@}
    
    friend std::ostream& operator<<(std::ostream &os, Point const &m);
    
protected:
    //! Native SDL_Point
    SDL_Point m_point;
};



#endif
