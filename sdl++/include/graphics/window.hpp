#ifndef WINDOW_HPP
#define WINDOW_HPP

#include <string>
#include <SDL.h>
#include <SDL_image.h>

#include <sys/types.h>

#include "base/color.hpp"
#include "base/point.hpp"
#include "base/rect.hpp"

#include "graphics/texture.hpp"

class Drawable;

/**
 *  @ingroup Graphics
 */

//! A Window 
/*!
    Window is a wrapper around SDL_Window, but it also manages SDL Initialization
    
    Since most applications use only one window, that needs to be accessed in many places,
    the first window to be created initializes sdl, and when it's destroyed it closes everything.
    
    This window can always be accessed from anywere using Window::main_window
 */
class Window{
public:
    
    //! Interpolation method
    /*!
        Interpolation method to be used by the window Renderer
     */
    enum Interpolation { 
        NEAREST, ///< Nearest Neighbour interpolation
        LINEAR, ///< Linear interpolation
        ANISOTROPIC ///< Anisotropic interpolation (it is only supported by DirectX)
    }; 
    
    //! Default constructor
    /*!
        \param title Title of the window
        \param width Width of the window
        \param height Height of the window
        \param flags SDL_WindowFlags passed to SDL_CreateWindow
        \param interpolation Interpolation passed to SDL_SetHint SDL_HINT_RENDER_SCALE_QUALITY
        \param init_flags Flags to initialize SDL if this is the first window
    */
    Window(std::string title, int width = 800, int height=600, SDL_WindowFlags flags = SDL_WINDOW_SHOWN, Interpolation interpolation = NEAREST, Uint32 init_flags = SDL_INIT_EVERYTHING);
    //! Destructor
    /*!
        Destroys the window and the Renderer, if this is the main_window also close SDL
     */
    ~Window();
    //! Wrapper around SDL_Init
    /*!
        Calls SDL_Init and checks for errors
        \returns Wether SDL was correctly initialized
     */
    static bool initializeSDL(Uint32 flags = SDL_INIT_EVERYTHING);
    //! Pointer to the native SDL_Window
    /*!
        \returns A pointer to the native SDL_Window
     */
    SDL_Window* window();
    //! Pointer to the native SDL_Renderer
    /*!
        \returns A pointer to the native SDL_Renderer
     */
    SDL_Renderer* renderer();
    //! First window to be created
    /*!
        Since most applications and games only use one Window it's easier to initialize everything when the first window is created and store it's pointer to close everything at the end.
        
        Since this is public it should be able to be changed on the fly if needed (For example to have a config screen and later open the main game)
     */
    static Window* main_window;
    //! Clears the screen with a given color
    /*!
        Clears the screen with a given color, the default color {75,75,75} was chosen as to not be mistaken with a coincidentally black screen
        \param color Color to use to clear the window
     */
    void clear(Color color = Color(75,75,75,255));
    //! Low level draw texture
    /*!
        Direct wrapping around SDL_RenderCopy
        \param texture SDL_Texture to be drawn
        \param srcrect SDL_Rect inside the texture to be drawn
        \param dstrect SDL_Rect position in the window to draw the texture
     */
    void draw(SDL_Texture* texture, const SDL_Rect* srcrect = nullptr, const SDL_Rect* dstrect= nullptr);
    //! High level draw texture
    /*!
        Draws a texture using the SDL++ classes
        \param texture Texture to be drawn
        \param src Rect inside the texture to be drawn
        \param dst Rect position in the window to draw the texture
     */
    void draw(Texture* texture, Rect* src = nullptr, Rect* dst = nullptr);
    //! High level draw texture
    /*!
        Same as above but with refs instead of pointers
        \param texture Texture to be drawn
        \param src Rect inside the texture to be drawn
        \param dst Rect position in the window to draw the texture
     */
    void draw(Texture& texture, Rect& src, Rect& dst);
    
    //! Low level complex drawing
    /*!
        Direct wrapping around SDL_RenderCopyEx
        \param texture SDL_Texture to be drawn
        \param srcrect SDL_Rect inside the texture to be drawn
        \param dstrect SDL_Rect position in the window to draw the texture
        \param angle Angle in degrees to flip the texture
        \param center SDL_Point to use as center for the rotation
        \param flip Flip state for the texture
     */
    void draw(SDL_Texture* texture,
                const SDL_Rect*        srcrect,
                const SDL_Rect*        dstrect,
                const double           angle,
                const SDL_Point*       center,
                const SDL_RendererFlip flip = SDL_RendererFlip::SDL_FLIP_NONE ); //More complete way to call the draw
    
    //! High level complex drawing
    /*!
        Draws a texture using the SDL++ classes
        \param texture Texture to be drawn
        \param src Rect inside the texture to be drawn
        \param dst Rect position in the window to draw the texture
        \param angle Angle in degrees to flip the texture
        \param center Point to use as center for the rotation
        \param flip Flip state for the texture
     */
    void draw(Texture* texture, Rect* src, Rect* dst, double angle, Point* center, SDL_RendererFlip flip = SDL_RendererFlip::SDL_FLIP_NONE); 
    //! High level complex drawing
    /*!
        Same as above but uses refs instead of pointers
        \param texture Texture to be drawn
        \param src Rect inside the texture to be drawn
        \param dst Rect position in the window to draw the texture
        \param angle Angle in degrees to flip the texture
        \param center Point to use as center for the rotation
        \param flip Flip state for the texture
     */
    void draw(Texture& texture, Rect& src, Rect& dst, double angle, Point& center, SDL_RendererFlip flip = SDL_RendererFlip::SDL_FLIP_NONE);
    //! Draw a drawable
    /*!
        Calls the draw function on the drawable passing this as argument
        \param drawable Drawable to be drawn
     */
    void draw(Drawable* drawable);
    //! Displays the drawn
    /*!
        Wrap around SDL_RenderPresent, basically switches the buffers
     */
    void display(); //Displays the window
    //! Time elapsed since last time display was called in native Uint32 miliseconds
    Uint32 delta();
    //! Time elapsed converted to seconds
    float deltaSeconds();
    //! Wrapper around SDL_RenderSetScale
    void setZoom(float zoomX, float zoomY);
    //! Wrapper around SDL_RenderSetViewport
    void setViewport(SDL_Rect* viewport);
    //! Sets Interpolation method
    void setInterpolation(Interpolation interpolation);
    
protected:
    //! Window Width
    int m_width  = 800;
    //! Window Height
    int m_height = 600;
    //! Wether SDL was already initialized
    static bool sdlInited;
    
    //! The actual SDL_Window
    SDL_Window* m_window = NULL;
    //! The window renderer
    SDL_Renderer* m_renderer = NULL;
    
    //! Interpolation method
    Interpolation m_interpolation;
    
    /** @name FPS variables
        Variables to use in the calculation of delta and deltaSeconds
    */
    ///@{
    Uint32 m_delta = 0;
    Uint32 m_prev_time = 0;
    ///@}
    
};

#endif
