#ifndef FONT_HPP
#define FONT_HPP

#include <SDL.h>
#include <SDL_ttf.h>

#include <string>

/**
 *  @ingroup Graphics
 */

//! A Font to draw Text
/*!
  A font to be used to render text. A wrapper around TTF_Font.
 */
class Font {
public:
    //! Default constructor
    /*!
        \param filepath Path to the file containing a font
        \param size Size to load the font
    */
    Font(std::string filepath = "", int size = 16);
    //! Destructor
    /*!
        If the font was opened calls TTF_CloseFont on it
    */
    ~Font();
    //! Opens a font with a defined size
    /*!
        Initializes TTF_Init if needed, and calls TTF_OpenFont with the passed parameters.
        \param filepath Path to the file containing a font
        \param size Size to load the font
    */
    bool open(std::string filepath, int size = 16);
    //! Pointer to the native TTF_Font
    TTF_Font* font();
protected:
    //! Pointer to the native TTF_Font
    TTF_Font* m_font = nullptr;
    //! Wether the TTF lib has already been initialized
    static bool TTF_lib_inited;
};

#endif
