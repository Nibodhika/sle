#ifndef TEXTURE_HPP
#define TEXTURE_HPP

#include <string>
#include <SDL.h>

#include "base/point.hpp"

/**
 *  @ingroup Graphics
 */

//! An image loaded in memory
/*!
    A Texture is an image loaded from a file into memory.
 */
class Texture{
public:
    //! Default constructor
    /*!
        \param filepath Path to the image that should be loaded
    */
    Texture(std::string filepath = "");
    //! Constructor from surface
    /*!
        A wrapper around SDL_CreateTextureFromSurface
        \param surface Surface to be copied
        \param renderer Renderer to be used, if null the default one at main_window will be used
    */
    Texture(SDL_Surface* surface, SDL_Renderer* renderer = nullptr);
    //! Destructor
    /*!
        A wrapper around SDL_DestroyTexture
     */
    ~Texture();
    //! Opens the file
    /*!
        \param filepath File to open
        \param renderer Renderer to be used, if null the default one at main_window will be used
     */
    bool open(std::string filepath, SDL_Renderer* renderer = nullptr);
    //! Returns a pointer to the native SDL_Texture
    /*!
        \returns The native SDL_Texture
    */
    SDL_Texture* texture();
    //! Size of the Texture
    /*!
        \returns Point representing the size of the texture, x is Width, y is Height.
    */
    Point size();
    //! Width of the Texture
    int width();
    //! Height of the Texture
    int height();
    
protected:
    //! Native SDL_Texture
    SDL_Texture* m_texture = nullptr;
    //! Width of the Texture
    int m_width = 0;
    //! Height of the Texture
    int m_height = 0;

private:
    //! Creates a texture based on a surface and a renderer
    bool _create(SDL_Surface* surface, SDL_Renderer* renderer = nullptr);
};

#endif
