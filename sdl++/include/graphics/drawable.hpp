#ifndef DRAWABLE_HPP
#define DRAWABLE_HPP

#include "graphics/window.hpp"

//TODO Drawable should use a Transform in a future release, when Transform is more mature

/**
 *  @ingroup Graphics
 */

//! Generic Drawable
/*!
  A Drawable is any class that knows how to draw itself.
 */
class Drawable{
public:
    //! Default constructor
    Drawable();
     //! Sets the position and the size from a Rect
    /*!
        \param pos Rect to use as position and size.
    */
    void setPosSize(Rect pos);
    //! Sets the position from a Point
    /*!
        \param pos Point to use as position.
    */
    void setPos(Point pos);
    //! Sets the position from two values
    /*!
        \param x X component of the position.
        \param y Y component of the position.
    */
    void setPos(float x, float y);
    //! Sets the size from a Point
    /*!
        \param size Point to use as size, x as Width, y as Height.
    */
    void setSize(Point size);
    //! Sets the size from two values
    /*!
        \param w Width.
        \param h Height.
    */
    void setSize(float w, float h);
    //! Scales the rectangle rotation
    /*!
        \param value Value to set as rotation in Degrees.
    */
    void setRotation(double value);
    //! Scales the rectangle position by factor
    /*!
        \param factor Position rect size will be scaled by factor.
    */
    void scale(float factor);
    //! Scales the rectangle position to fit Width
    /*!
        \param width desired Width for the Drawable, Height will be scaled accordingly to preserve ratio.
    */
    void scaleToWidth(float width);
    //! Scales the rectangle position to fit Height
    /*!
        \param height desired Height for the Drawable, Width will be scaled accordingly to preserve ratio.
    */
    void scaleToHeight(float height);
    //! Scales the rectangle to size
    /*!
        Scales to the fit the larger dimension without distorging the drawable.
        \param size Point that represents the desired size.
        \sa setSize
    */
    void scaleToSize(Point size);
    //! Scales the rectangle to width or height
    /*!
        Scales to the fit the larger dimension without distorging the drawable.
        \param width Desired width.
        \param height Desired Height.
        \sa setSize
    */
    void scaleToSize(float width, float height);
    //! Moves by x and y
    /*!
        \param x Displacement in x.
        \param y Displacement in y.
        \sa setPos
    */
    void move(float x, float y);
    //! Moves by Point
    /*!
        \param delta Drawable will be moved by the components of point.
        \sa setPos
    */
    void move(Point delta);
    //! Rotates by angle
    /*!
        \param angle Angle in degrees to add to current rotation.
        \sa setRotation
    */
    void rotate(double angle);
    //! Position and Size of the Drawable
    /*!
        <b>WARNING</b> Changing values returned by this might compromise the center for rotation purposes.
        \returns Rectangle containging the position and size of the drawable
    */
    Rect* pos();
    //! Sets the center from a Point
    /*!
     * Center is relative to the top left corner and it's only used for rotation purposes.
        \param center Center point.
    */
    void setCenter(Point center); //Sets the point to be used as the center
    //! Sets the center from x and y
    /*!
        \param x X component of the center.
        \param y Y component of the center.
    */
    void setCenter(int x, int y);
    //! Sets the center to default
    /*!
        Sets the center to the default value, which is in the center of the dest rect.
        If center is already at default this gets called whenever a resize happens
    */
    void setCenter();

    //! Sets wether the texture should be flipped Horizontally
    /*!
        \param v True for flipped, false for not flipped.
    */
    void setFlipH(bool v);
    //! Sets wether the texture should be flipped Vertically
    /*!
        \param v True for flipped, false for not flipped.
    */
    void setFlipV(bool v);
    //! Sets wether the texture Flipped state from a SDL_RendererFlip
    /*!
        \param v The SDL_RendererFlip to set the Flipped state.
    */
    void setFlipped(SDL_RendererFlip v);
    //! Changes the Horizontal Flip value
    /*!
        Flippes the image Horizontally.
    */
    void flipH();
    //! Changes the Vertical Flip value
    /*!
        Flippes the image Vertically.
    */
    void flipV();
    
    //! Returns horizontal flipped state
    /*!
        \returns Wether the image is flipped Horizontally.
    */
    bool isFlippedH();
    //! Returns vertical flipped state
    /*!
        \returns Wether the image is flipped Vertically.
    */
    bool isFlippedV();
    //! Returns flipped state
    /*!
        \returns Current SDL_RendererFlip 
    */
    SDL_RendererFlip flipped();
        
    //! Draw function
    /*!
        Function to be called when attempting to draw the Drawable.
        It should be implemented in any Drawable class.
        
        If your drawable class has a texture and a srcRect you can use the doDraw inside of it.
        \param window Window to draw the Drawable at.
        \sa doDraw
    */
    virtual void draw(Window* window) = 0;
    
    bool isDefaultCenter();
    
protected:
    //! Internal draw function
    /*!
        Calls window->draw passing the tex and srcRect as well as all the internal parameters for position, rotation, center point and flipped state
        
        \param window Window to draw the Drawable at.
        \param tex Texture to draw.
        \param srcRect Rectangle inside of the image to draw.
        \sa draw
    */
    void doDraw(Window* window, Texture* tex, Rect* srcRect);
    //! The rectangle of the drawable position in the world
    Rect m_pos; 
    //! Angle that the drawable is rotated
    double m_angle = 0;
    //! Center point for rotation purposes
    Point m_center;
    //! Flipped state
    SDL_RendererFlip m_flipped = SDL_RendererFlip::SDL_FLIP_NONE;
    //! Wether should recalculate center when resized
    bool m_defaultCenter = true;
};

#endif
