#include "graphics/drawable.hpp"
#include "graphics/font.hpp"
#include "graphics/image.hpp"
#include "graphics/sprite.hpp"
#include "graphics/text.hpp"
#include "graphics/texture.hpp"
#include "graphics/window.hpp"

/** @defgroup Graphics Graphics
 *  @brief This group contains all classes that are related to Graphics
 */
