#ifndef IMAGE_HPP
#define IMAGE_HPP

#include "graphics/texture.hpp"
#include "base/rect.hpp"

/**
 *  @ingroup Graphics
 */

//! An image is a piece of a texture
/*!
    An image is a piece of a Texture, represented by a pointer to the Texture and a Rect inside that texture.
 */
class Image {
public:
    //! Default constructor
    /*!
        \param texture Pointer to the texture.
        \param subRect Rectangle inside the teture. If zero sized, it will default to the full Texture.
    */
    Image(Texture* texture = nullptr, Rect subRect = Rect(0,0,0,0));
    //! Sets the Texture
    /*!
        \param texture Pointer to the texture.
    */
    void setTexture(Texture* texture);
    //! Sets rectangle inside of the texture
    /*!
        \param subRect Rectangle inside the teture. If zero sized, it will default to the full Texture.
    */
    void setSubrect(Rect subRect = Rect(0,0,0,0)); 
    //! Returns the Texture
    Texture* texture();
    //! Returns the Rect inside the Texture
    Rect* srcRect();
    
protected:
    Texture* m_texture;
    Rect m_subimg;
  
};

#endif
