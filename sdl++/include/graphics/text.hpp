#ifndef TEXT_HPP
#define TEXT_HPP

#include "graphics/window.hpp"
#include "graphics/sprite.hpp"
#include "graphics/font.hpp"
#include "base/color.hpp"

/**
 *  @ingroup Graphics
 */

//! A Text is a drawable string
/*!
    A Text draws a string with a given font and a given DrawMode
 */
class Text : public Drawable {
public:
    //! Draw Mode
    /*!
        The mode in which the text should be drawn, Solid, Shaded or Blended.
     */
    enum DrawMode { 
        SOLID, ///< Solid text draw (use TTF_RenderText_Solid)
        SHADED, ///< Shaded text draw (use TTF_RenderText_Shaded)
        BLENDED ///< Blended text draw, (use TTF_RenderText_Blended)
    };
    
    //! Default constructor
    /*!
        \param font Font to be used to draw the text
        \param text String to be drawn
        \param color to draw the text
        \param mode Draw mode
        \param bgColor Background color if drawing on Shaded mode
        \param renderer Renderer used to draw, leave null to use the one at main window
    */
    Text(Font* font = nullptr, std::string text = "", Color color = Color::White, DrawMode mode = DrawMode::BLENDED, Color bgColor = { 0, 0, 0 }, SDL_Renderer* renderer = nullptr);
    //! Destructor
    /*!
        Deletes the generated texture
    */
    ~Text();
    //! Sets the values and creates the texture
    /*!
        \param font Font to be used to draw the text
        \param text String to be drawn
        \param color to draw the text
        \param mode Draw mode
        \param bgColor Background color if drawing on Shaded mode
        \param renderer Renderer used to draw, leave null to use the one at main window
        \returns Wether it was possible to create the Texture
    */
    bool create(Font* font, std::string text, Color color = Color::White, DrawMode mode = DrawMode::BLENDED, Color bgColor = Color::Black, SDL_Renderer* renderer = nullptr);
    //! Changes the Font and recreates the Texture
    /*!
        \param font Font to draw the texture
        \returns Wether the Texture was created.
     * */
    bool changeFont(Font* font);
    //! Changes the text and recreates the Texture
    /*!
        \param text Text to draw in the texture
        \returns Wether the Texture was created.
     * */
    bool changeText(std::string text);
    //! Changes the Color and recreates the Texture
    /*!
        \param color Color to draw the texture
        \returns Wether the Texture was created.
     * */
    bool changeColor(Color color);
    //! Changes the Background Color and recreates the Texture
    /*!
        \param color Color to use as Background in the texture
        \returns Wether the Texture was created.
     * */
    bool changeBGColor(Color color);
    //! Changes the Renderer
    /*!
        \param renderer Renderer to draw the Texture, leave null to use the main_window one
        \returns Wether the Texture was created.
     * */
    bool changeRenderer(SDL_Renderer* renderer = nullptr);
    
    //! Implementation of the draw method
    /*!
        \param window Window to draw the Text
    */
    void draw(Window * window) override;
    
protected:
    //! Font to use
    Font* m_font = nullptr;
    //! String to draw
    std::string m_text = "";
    //! Renderer to use
    SDL_Renderer* m_renderer = nullptr;
    //! Color of the foreground
    Color m_color;
    //! Draw Mode
    DrawMode m_mode;
    //! Color of the Background, only used for SHADED mode
    Color m_bg;
    //! Generated Texture
    Texture* m_texture = nullptr;
    
private:
    //! Internal function to recreate the texture
    bool _recreate();
};

#endif
