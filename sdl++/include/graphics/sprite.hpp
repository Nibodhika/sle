#ifndef SPRITE_HPP
#define SPRITE_HPP

#include "graphics/drawable.hpp"
#include "graphics/texture.hpp"
#include "graphics/image.hpp"
#include "base/rect.hpp"

//A Sprite is an image that knows how to draw itself

/**
 *  @ingroup Graphics
 */

//! An Image that knows how to draw itself
/*!
    A Sprite is a Drawable that draws an Image passed as a parameter
 */
class Sprite : public Drawable {
public:
    //! Default constructor
    /*!
        \param i image to draw.
    */
    Sprite(Image* i = nullptr);
    
    //! Implementation of the draw method
    /*!
        \param window Window to draw the Sprite
    */
    virtual void draw(Window* window);
    //! Changes the image to be drawn
    /*!
        \param i image to draw.
    */
    void setImage(Image* i);
    //! Image to be drawn
    /*!
        \returns The image that this Sprite draws.
    */
    Image* getImage();
    
protected:
    //! Image to be drawn
    Image* m_image;
};

#endif
