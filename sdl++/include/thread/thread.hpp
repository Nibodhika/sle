#ifndef THREAD_HPP
#define THREAD_HPP

#include <SDL2/SDL_thread.h>
#include <string>

//This is here so that including thread includes everything in the thread module
#include "thread/threadable.hpp"

/** @defgroup Thread Thread
 *  @brief This group contains all classes that are related to Thread management
 */

/**
 *  @ingroup Thread
 */

//! A Thread is the simplest form of making a function run in a different thread

/*!
    Thread it's a wrapper around SDL_ThreadFunction and SDL_Thread.
    It manages the creation of the SDL_Thread when start is called.
 */

class Thread{
public:
    
    //! Default constructor
    /*!
        \param fn A function with a signature int function(void* data)
        \param name Name for the thread, passed to SDL
        \param data Data passed to the function fn
    */
     Thread(SDL_ThreadFunction fn, std::string name, void* data); //Direct sdl access
   
    
    //! Starts the thread
    /*!
      A wrapper around SDL_CreateThread
    */
    void start();
    
    //! Waits for the thread to finish
    /*!
      Waits for the thread to finish and returns the output, it should only be called after start has been called.
      Basically it's a wrapper around SDL_WaitThread.
      
      \returns the output of the function
    */
    int wait();
    
    //! Returns a pointer to the native SDL_Thread
    /*!
        \returns The native SDL_Thread
    */
    SDL_Thread* thread();
    
    //! The name passed to the constructor
    /*!
        \returns The name string
    */
    std::string getName();

private:
    //! Function to be called inside the thread
    SDL_ThreadFunction m_fn;
    //! Name passed to the SDL_CreateThread
    std::string m_name;
    //! Data passed to the SDL_CreateThread
    void* m_data;
    //! Native SDL_Thread
    SDL_Thread* m_thread;
};

#endif
