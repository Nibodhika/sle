#ifndef THREADABLE_HPP
#define THREADABLE_HPP
#include <string>



class Thread;

/**
 *  @ingroup Thread
 */

//! Threadable is a higher level implementation of a thread

/*!
    Threadable is a class that contains a thread, in order to implement your own threadable classes inherit from this and implement the run function.
 */
class Threadable {
public:
    
    //! Default constructor
    /*!
        \param name The name of the thread.
    */
    Threadable(std::string name = "thread");
    //! Default destructor
    /*!
        Empty by default
    */
    ~Threadable();
    //! Starts the thread
    /*!
        Calls the start on the thread
    */
    void start(); //Function to be called from the outside to start the thread
    
protected:
    //! Virtual function to be implemented in your class
    /*!
        You should implement this when creating your threadable class to do whatever you want to be done inside the thread.
    */
    virtual void run() = 0; //Function to be called inside a thread, must be overwritten in child object
    
private:
    //! Function used to call the thread passing this as a parameter
    static int doRun(void* threadable);
    //! Internal Thread used to run the functions
    Thread* m_thread;
};

#endif
