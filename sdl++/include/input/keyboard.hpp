#ifndef KEYBOARD_HPP
#define KEYBOARD_HPP

#include <SDL.h>
#include <map>

/**
 *  @ingroup Input
 */

//! Keyboard input handling

/*!
  SDL asumes only one keyboard, so all functions are static and there's no need to instanciate a Keyboard
 */
class Keyboard {
public:
    //! Wrapper around SDL_GetKeyboardState
    static const Uint8* getKeyboardState();
    //! Returns the state of a key
    /*!
        \param key SDL_Scancode of the key to check
        \returns State of a key, true if pressed, false if not
    */
    static bool isKeyDown(SDL_Scancode key);
    //! Returns the name of a key from it's SDL_Scancode
    /*!
        \returns Name of the key, "UNKNOWN" if an invalid value is passed
    */
    static std::string nameFromCode(SDL_Scancode key);
    //! Returns the SDL_Scancode of a key from it's name
    /*!
        \returns SDL_Scancode of the key, SDL_SCANCODE_UNKNOWN if an invalid value is passed
    */
    static SDL_Scancode codeFromName(std::string name);
  
protected:
    //! Reference obtained from SDL_GetKeyboardState(NULL)
    static const Uint8* m_state;
  
    const static std::map<std::string,SDL_Scancode> m_codes;
};

#endif
