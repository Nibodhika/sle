#ifndef CONTROLLER_HPP
#define CONTROLLER_HPP

#include <map>

#include <iostream>
#include <SDL.h>
/**
 *  @ingroup Input
 */


//! Handles Controller input

/*!
  Controller class is a high level implementation around the SDL_GameController.
  
  It attempts to open a GameController and it's haptic feedback, and provides several high level access to it.
  
  There's no wrapper around SDL_Joystick, it should only be needed for things that are recognized as joysticks but not as controllers, but a better solution is to add the mappings for that controller. If needed it'll be implemented and this class modified to use it.
 */
class Controller {
public:
    //! Default constructor
    /*!
        Attempts to open controller at index index. It also attempts to open it's haptic module and enable rumble support.
        
        \param index Index of the controller to be opened
        \param deadZone number between 0 and 32767 where axis are considered 0. SDL Recommends 3200, but my Xbox 360 controller disagrees, curiously ps3 wired requires 0 deadzone
    */
    Controller(int index, Uint16 deadZone = 5000);
    //! Destructor
    /*!
        Attempts to close the haptic and controller, joystic should be closed by the GameController.
    */
    ~Controller();
    //! Adds a mapping
    /*!
        Wrapper around SDL_GameControllerAddMapping
    */
    static void addMapping(std::string mapping);
    //! Adds a mapping from a file
    /*!
        Wrapper around SDL_GameControllerAddMappingsFromFile
    */
    static void addMappingFromFile(std::string filepath); // Perhaps also add bindings for SDL_GameControllerAddMappingsFromRW.
    //Also perhaps SDL_GameControllerMappingForGUID to add for live adding in game.
    
    //! Gets controller name
    /*!
        \returns The name of this controller
    */
    std::string name();
    //! Attempts to make the controller rumble
    /*!
      A wrapper around SDL_HapticRumblePlay
        \param strenght strength of the rumble to play as a 0-1 float value
        \param time length of the rumble to play in milliseconds
    */
    void rumble(float strenght, float time);
    //! Returns if a given button is pressed
    /*!
        \param button Button to be checked
        \returns Status of the button, true if pressed, false if not
    */
    bool isButtonDown(SDL_GameControllerButton button);
    //! Returns if the status of a given axis
    /*!
        \param axis Axis to be checked
        \returns Normalized status of the axis, from -1 to 1
    */
    double getAxis(SDL_GameControllerAxis axis); //This will return a value between 0 and 1.

    void setDeadZone(Uint16 deadZone);
    Uint16 getDeadZone();
    
    //DPad
    //! Status of the D-pad up
    bool up();
    //! Status of the D-pad down
    bool down();
    //! Status of the D-pad left
    bool left();
    //! Status of the D-pad right
    bool right();
    //ABXY or x, circle,square, triangle
    //! Status of the A button (Xbox style)
    bool A();
    //! Status of the B button (Xbox style)
    bool B();
    //! Status of the X button (Xbox style)
    bool X();
    //! Status of the Y button (Xbox style)
    bool Y();
    //Back Guide Start or Select, middle, start
    //! Status of the Back (Select) button (Xbox style)
    bool Back();
    //! Status of the Guide (PS3/Xbox/Steam) button
    bool Guide();
    //! Status of the Start button
    bool Start();
    // LStick RStick or L3 R3
    //! Status of the Left Stick click (Playstation's L3) button
    bool LStick();
    //! Status of the Right Stick click (Playstation's R3) button
    bool RStick();
    // LShoulder RShoulder L1, R1
    //! Status of the Left Shoulder (Playstation's L1) button
    bool LShoulder();
    //! Status of the Right Shoulder (Playstation's R1) button
    bool RShoulder();
    //! Status of the Left Axis in X axis
    double LX();
    //! Status of the Left Axis in Y axis
    double LY();
    //! Status of the Right Axis in X axis
    double RX();
    //! Status of the Right Axis in Y axis
    double RY();
    //! Status of the Left Trigger (Playstation's L2) axis
    double LT();
    //! Status of the Right Trigger (Playstation's R2) axis
    double RT();
    //! Id of the joystick (Might be different from the index if controllers were unplugged)
    SDL_JoystickID id();
    //! Index of the controller
    int index();
    //! Total amount of plugged joysticks
    /*!
        A wrapper around SDL_NumJoysticks
        \returns The amount of joystics plugged in
    */
    static int amount();
    //! SDL_GameControllerButton from a name
    /*!
        \returns SDL_GameControllerButton from the name, SDL_CONTROLLER_BUTTON_INVALID if an invalid value is passed
    */
    static SDL_GameControllerButton buttonFromName(std::string name);// There's SDL_GameControllerGetButtonFromString, probably should use
    //! SDL_GameControllerAxis from a name
    /*!
        \returns SDL_GameControllerAxis from the name, SDL_CONTROLLER_AXIS_INVALID if an invalid value is passed
    */
    static SDL_GameControllerAxis axisFromName(std::string name); // There's SDL_GameControllerGetAxisFromString, but it requires RIGHTTRIGGER instead of RT
    //! name from a SDL_GameControllerButton
    /*!
        \returns Name of the button, "UNKNOWN" if an invalid value is passed
    */
    static std::string nameFromButton(SDL_GameControllerButton btn);// SDL_GameControllerGetStringForButton probably should use it
    //! name from a SDL_GameControllerAxis
    /*!
        \returns Name of the axis, "UNKNOWN" if an invalid value is passed
    */
    static std::string nameFromAxis(SDL_GameControllerAxis axis); // SDL_GameControllerGetStringForAxis, but RIGHTTRIGGER
  
protected:
    //! Native SDL_GameController
    SDL_GameController* m_controller = NULL;
    //! Native SDL_Joystick
    SDL_Joystick* m_joystick = NULL;
    //! Native SDL_Haptic
    SDL_Haptic* m_haptic = NULL;
    //! Native SDL_JoystickID
    SDL_JoystickID m_id;
    //! Index of the Controller
    int m_index = -1;

private:
    //! Adds common mappings that are not native to SDL
    /*!
        It's called the first time a controller gets created and adds the following mappings.
        For now it only contains mappings for the PS3 controller when plugged via bluetooth, in the future it might contain more entries if other controllers are deemed common and not mapped by SDL
    */
    static void addNonDefaultMappings();
    //! Wether addNonDefaultMappings was already called
    static bool nonDefaultMappingsAdded;
    
    const static std::map<std::string,SDL_GameControllerButton> m_buttons;
    const static std::map<std::string,SDL_GameControllerAxis> m_axis;
    
    Uint16 m_deadZone;
};

#endif
