#include "input/keyboard.hpp"
#include "input/controller.hpp"

/** @defgroup Input Input
 *  @brief This group contains all classes that are related to input
 */
