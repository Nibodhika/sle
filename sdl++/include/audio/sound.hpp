#ifndef SOUND_HPP
#define SOUND_HPP

#include "audio/chunk.hpp"

/**
 *  @ingroup Audio
 */


//! A class that's able to play a Chunk

/*!
  The Sound class knows how to play an audio Chunk.
  It's a wrapper around several SDL funcions.
 */

class Sound {
public:

    //! Default constructor
    /*!
       If channel is -1 it will use the next highest avaliable channel number. 
       
     
        \param chunk Chunk to be played
        \param channel Audio channel to play the chunk at. It should only be used to overwrite an already exsiting channel.
    */
    Sound(Chunk* chunk, int channel = -1);
    //! Default destructor
    /*!
        Does nothing since an audio is only exisgintg while it's being played
    */
    ~Sound();

    //! Plays the chunk
    /*!
     * Internally it calls the Mix_PlayChannel and updates the m_channel variable
     
        \param loops Number of loops, -1 is infinite loops. Passing one here plays the sample twice (1 loop).
        \sa pause resume stop
    */
    void play(int loops = 0); // Pass -1 for infinite loop
    
    //! Pauses the channel
    /*!
     * Internally it calls the Mix_Pause on m_channel
     * \sa resume
    */
    void pause();
    
    //! Stops the channel
    /*!
     * Internally it calls the Mix_HaltChannel on m_channel
    */
    void stop();
    
    //! Resumes the channel
    /*!
     * Resumes the channel after being paused, internally it calls the Mix_Resume on m_channel
     * \sa pause
    */
    void resume();
    
    //! Returns the channel
    /*!
      \returns Channel number
    */
    int channel();
    
    //! Sets the channel number
    /*!
      <b>WARNING</b> This will not change the channel of a currently playing chunk. 
      This means that changing the channel while the sound is being played should result in the inability to stop that audio.
      Furthermore changing the channel while the audio is paused means that the audio should be forever lost in limbo.
      
      Since the only moment safe to change the channel is when it's stopped, this function will forcefully call stop.
      
       \param channel Channel number to overwrite the current channel
    */
    void setChannel(int channel); 
    
    //! Fades out
    /*!
      Internally calls Mix_FadeOutChannel.
      \param ms Milliseconds of time that the fade-out effect should take to go to silence, starting now.
      \sa fadeIn
    */
    void fadeOut(int ms);
    
    //! Fades in
    /*!
      Internally calls Mix_FadeInChannel.
      \param ms Milliseconds of time that the fade-in effect should take to go from silence to full volume.
      \param loops Number of loops, -1 is infinite loops. Passing one here plays the sample twice (1 loop).
      \sa fadeOut
    */
    void fadeIn(int ms, int loops = 0); //Instead of play fadein
    
    //! Is Playing
    /*!
      Returns if the audio is being played. 
      Internally calls Mix_Playing and checks if chunks are the same
      
      <b>WARNING</b> will return true if the channel is playing but paused
      
      \returns Wether the channel is being played (Even if paused)
    */
    bool isPlaying();
    
    //! Is Paused
    /*!
      Returns if the audio is paused. 
      Internally calls Mix_Paused and checks if chunks are the same
      
      \returns Wether the channel is paused
    */
    bool isPaused();
    
    //! Sets the play volume of the channel
    /*!
      Internally calls Mix_Volume
      
      \param volume The volume to use from 0 to MIX_MAX_VOLUME(128).\nIf greater than MIX_MAX_VOLUME,\nthen it will be set to MIX_MAX_VOLUME.
      
      \returns Current volume of the channel after setting it
      
      \sa pause
    */
    int setVolume(unsigned int volume);
    
    //! Gets the current volume of the channel
    /*!
      Internally calls Mix_Volume passing a negative value
      
      \returns Current volume of the channel
    */
    int getVolume();
    
    //! Chunk of audio
    /*!
      \returns pointer to the internal Chunk.
    */
    Chunk* getChunk();
    
    //! Sets the Chunk of audio
    /*!
     * Since the only moment safe to change the chunk is when it's stopped, this function will forcefully call stop.
      \param chunk pointer to the new Chunk to be played.
    */
    void setChunk(Chunk* chunk);
    

    //! Pauses all channels
    /*!
      Internally calls Mix_Pause(-1)
      \sa resumeAll
    */
    static void pauseAll();
    
    //! Resumes all channels
    /*!
      Internally calls Mix_Resume(-1)
      \sa pauseAll
    */
    static void resumeAll();
    
    //! Fades out all channels
    /*!
      Internally calls Mix_FadeOutChannel(-1, ms)
      \param ms Milliseconds of time that the fade-out effect should take to go to silence, starting now.
    */
    static void fadeOutAll(int ms);
    
    //! Stops all channels
    /*!
      Internally calls Mix_HaltChannel(-1)
    */
    static void stopAll();
    
    //! Initializes the SDL audio with some predefined parameters
    /*!
      calls Mix_Init with MIX_INIT_MP3 (because that seems to load everything else too)
      calls Mix_OpenAudio with most default parameters
    */
    static bool simpleInit();
  
protected:
    //! Chunk to be played
    Chunk* m_chunk;
    //! Channel to play the chunk at
    int m_channel;
};

#endif
