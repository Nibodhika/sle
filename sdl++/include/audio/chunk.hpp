#ifndef CHUNK_HPP
#define CHUNK_HPP

#include <string>

#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>

/**
 *  @ingroup Audio
 */

//! An audio Chunk is an audio file being loaded at memory

/*!
  Chunk is to Sound what Texture is to Sprite.
  A Chunk represents the audio itself, and there can be several Sounds with the same audio Chunk.
  The Chunk class it's a wrapper around Mix_Chunk.
 */

class Chunk {
public:

  //! Default constructor
  /*!
      \param path the path to the audio file to be loaded. if not informed no chunk will be loaded
  */
  Chunk(std::string path = "");
  
  //! Default destructor
  /*!
    if a chunk is opened it closes it with Mix_FreeChunk
  */
  ~Chunk();

  //! Opens a file
  /*!
      \param path the path to the audio file to be loaded.
      \returns wether it could open the file
  */
  bool open(std::string path);

  //! Returns a pointer to the native SDL Mix_Chunk
  /*!
      \returns The native SDL Mix_Chunk
   */
  Mix_Chunk* chunk();

protected:
  //! Native SDL Mix_Chunk
  Mix_Chunk* m_chunk = nullptr;

};




#endif
