#include <iostream>

#include <audio/sound.hpp>
#include <graphics/window.hpp>

#include "managers/assetManager.hpp"

int main(){
    //Creating a window loads SDL stuff
     Window main_window("title", 800,600); 
     
     //The correct way to handle assets is to use an AssetManager
    AssetManager& manager = AssetManager::instance();
    // Assets to be loaded can be added to the manager
    manager.addAsset("TEXTURE_1","../../../resources/SDL_logo.png",Asset::Type::TEXTURE);
    //The manager can then load those assets instantly, but it will block the thread until all assets are loaded
    manager.load();
    
    std::cout << "Finished loading" << std::endl;
    
    //After it was loaded new assets can be added, but they will only be loaded when load is called, assets that were already loaded will not be loaded again
    manager.addAsset("MUSIC_1","../../../resources/music.ogg",Asset::Type::AUDIO);
    manager.addAsset("MUSIC_2","../../../resources/music2.ogg",Asset::Type::AUDIO);
 
    //Assets can also be loaded one by one until all of them are loaded
    std::cout << "Starting one by one loading" << std::endl;
    float percent = manager.loadNext();
    std::cout << "percent: " << percent << std::endl;
    percent = manager.loadNext();
    std::cout << "percent: " << percent << std::endl;
    //There's no problem with calling it after it's finished loading assets
    percent = manager.loadNext();
    std::cout << "percent: " << percent << std::endl;
    percent = manager.loadNext();
    std::cout << "percent: " << percent << std::endl;
    
  
/*    
    //Assets can also be loaded in a thread
     manager.loadInThread();
    //The manager provides a isDone function and a percent 0-1 ranged value
    while(!manager.isDone()){
          std::cout << manager.percent() << std::endl;
          SDL_Delay(50);
    }*/

  
}
