#include <iostream>

#include <input/keyboard.hpp>
#include <graphics/window.hpp>
#include "ECS/systems/systemsManager.hpp"
#include "graphics/map/tiledMap.hpp"

#include "lua.hpp"

int main(){
    Window main_window("title", 800,600, SDL_WINDOW_SHOWN, Window::Interpolation::NEAREST); 
    
    SystemsManager::get().init( DRAWING );
    
    TiledMap map("my map");
    map.open("../../../resources/maps/map.json");
    
    
    float x = 0;
    float y = 0;
    float w = 300;
    float h = 300;
    float zoom = 1;
    //Just to show viewports working
    // ERROR THIS IS NOT HOW VIEWPORTS WORK
    // They're used to draw stuff in sepparate areas of the screen, for example to show a minimap or something like that.
    // I'm exploiting the fact that if I draw the map at -100,0 I'll be seeing 100,0 at 0,0. YOU SHOULD NOT USE VIEWPORTS FOR THIS
    Rect viewport(x,y,w,h);//600,400);
    
    float speed = 100;
    main_window.setViewport(viewport.rect());
    Sprite* s = map.createSprite(1);
     SDL_Event e;
    bool alive = true;
  while(alive) {
       SystemsManager::get().update(0.12f);
    while( SDL_PollEvent( &e ) != 0 ) {
      if( e.type == SDL_QUIT ) {
        std::cout << "closing" << std::endl;
        alive = false;
      }
      else if( e.type == SDL_KEYDOWN ){
        //Select surfaces based on key press
        switch( e.key.keysym.sym ) {
        case SDLK_ESCAPE:
          std::cout << "escaping" << std::endl;
          alive = false;
          break;
        }}
    }

    float dt = main_window.deltaSeconds();
//     std::cout << dt << " " << SDL_GetTicks() << std::endl;
    if(Keyboard::isKeyDown(SDL_SCANCODE_W) ){
        y += dt * speed;
        auto p = map.transform().position();
        p.y( p.y() + dt * speed);
        map.transform().setPosition(p);
//         viewport.y(y);
//         main_window.setViewport(viewport.rect());
    }
    else if(Keyboard::isKeyDown(SDL_SCANCODE_S) ){
        y -= dt * speed;
//         viewport.y(y);
//         main_window.setViewport(viewport.rect());
        auto p = map.transform().position();
        p.y( p.y() - dt * speed);
        map.transform().setPosition(p);
    }
    
    if(Keyboard::isKeyDown(SDL_SCANCODE_D) ){
        x -= dt * speed;
        viewport.x(x);
        main_window.setViewport(viewport.rect());
    }
    else if(Keyboard::isKeyDown(SDL_SCANCODE_A) ){
        x += dt * speed;
        viewport.x(x);
        main_window.setViewport(viewport.rect());
    }
    
    if(Keyboard::isKeyDown(SDL_SCANCODE_E) ){
        w += dt * speed;
        viewport.w(w);
        main_window.setViewport(viewport.rect());
    }
    else if(Keyboard::isKeyDown(SDL_SCANCODE_Q) ){
        w -= dt * speed;
        viewport.w(w);
        main_window.setViewport(viewport.rect());
    }
    if(Keyboard::isKeyDown(SDL_SCANCODE_C) ){
        h += dt * speed;
        viewport.h(h);
        main_window.setViewport(viewport.rect());
    }
    else if(Keyboard::isKeyDown(SDL_SCANCODE_Z) ){
        h -= dt * speed;
        viewport.h(h);
        main_window.setViewport(viewport.rect());
    }
    
    //Zoom screws viewport hack
//     if(Keyboard::isKeyDown(SDL_SCANCODE_R)){
//         zoom += dt * speed /10.f;
//          main_window.setZoom(zoom,zoom);
//     }
//     else if(Keyboard::isKeyDown(SDL_SCANCODE_F)){
//         zoom -= dt * speed / 10.f;
//          main_window.setZoom(zoom,zoom);
//     }
   

    //Clear the window
//     main_window.clear();
    
    //   main_window.draw(s);
//     map.draw(&main_window);
    //Display the screen
    main_window.display();
  }
    
    return 0;
}
