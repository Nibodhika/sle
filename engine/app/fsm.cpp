#include "FSM/stateMachine.hpp"

#include <base/base.hpp>

class A{
public:
    A(std::string name){
        m_name = name;
    }
    
    std::string getName(){
        return m_name;
    }
    
    bool isAlive(){
        return m_alive;
    }
    
    void setAlive(bool v){
        m_alive = v;
    }
    
protected:
    std::string m_name;
    bool m_alive = true;
};

class AAlive : public State<A>{
public: 
    AAlive() : State<A>("A_Alive"){
        
    }
    void enter(A& obj) {
        cumulative = 0;
        std::cout << obj.getName() << " is ALIVE! " << obj.isAlive() << std::endl;
    }
    
    void update(A& obj, float dt){
        cumulative += dt;
        if(cumulative >= 3){
            obj.setAlive(false);
        }
    }
    
    void exit(A& obj) {
        std::cout << obj.getName() << " is dying " << obj.isAlive() << std::endl;
    }
    
protected:
    float cumulative = 0;
};

class ADead : public State<A>{
public: 
    ADead() : State<A>("A_Dead"){
        
    }
    void enter(A& obj) {
        cumulative = 0;
        std::cout << obj.getName() << " is Dead! " << obj.isAlive() << std::endl;
    }
    
    void update(A& obj, float dt){
        cumulative += dt;
        if(cumulative >= 3){
            obj.setAlive(true);
        }
    }
    
    void exit(A& obj) {
        std::cout << obj.getName() << " is reviving " << obj.isAlive() << std::endl;
    }
    
protected:
    float cumulative = 0;
};

class AStateMachine : public StateMachine<A>{
public:
    std::shared_ptr<AAlive> alive = std::make_shared<AAlive>();
    std::shared_ptr<ADead> dead = std::make_shared<ADead>();
    AStateMachine(A& a) : StateMachine<A>(a){
        changeState(alive);
    };
    
    void checkStateChange() {
        if(m_currentState == alive && !m_obj.isAlive()){
            changeState(dead);
        }
        else if(m_currentState == dead && m_obj.isAlive()){
            changeState(alive);
        }
    }
};

int main(){
    A a("my first A");
  
    
    AStateMachine state_machine(a);
    
    Clock c;
    c.start();
    while(true){
        float dt = c.restart();
        state_machine.update(dt);
        Clock::wait(0.1);
    }
    
    
    return 0;
}
