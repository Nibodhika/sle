#include <iostream>

#include <audio/sound.hpp>
#include <graphics/window.hpp>
#include "lua.hpp"
#include "managers/assetManager.hpp"

int main(){
    //Creating a window loads SDL stuff
     Window main_window("title", 800,600); 
     
     LUA::init();
     
    /*  script assets table is
    assets = {                                                                                                       
        TEXTURE_1 = {                                                                                                 
            file = "../../../resources/SDL_logo.png",                                                                  
            type = "TEXTURE"                                                                                           
        },                                                                                                            
        MUSIC_1 = {                                                                                                   
            file = "../../../resources/music.ogg",                                                                     
            type = "AUDIO"                                                                                             
        }                                                                                                             
    } 
*/
     
     LUA::loadFile("../../../resources/scripts/script.lua");
     LuaRef table = LUA::getTable("assets");
     //The correct way to handle assets is to use an AssetManager
    AssetManager& manager = AssetManager::instance();
    
    manager.addAssetFromLua(table);
    
   
 
    //Assets can also be loaded one by one until all of them are loaded
    std::cout << "Starting one by one loading" << std::endl;
    float percent =manager.percent();
    std::cout << "before begining percent is: " << percent << std::endl;
    while(percent < 1){
            percent =manager.loadNext();
    }
    
    std::cout << "Finished loading" << std::endl;
  
    std::cout << "Starting to unload" << std::endl;
    
   manager.deleteFromLua(table);
    
    std::cout << "deleted the assets" << std::endl;
    
    std::cout << "It's safe to attempt to redelete assets already deleted" << std::endl;
   manager.deleteFromLua(table);
}

