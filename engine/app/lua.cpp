#include <memory>

#include <lua.hpp>

using namespace luabridge;

void printMessage(const std::string& s) {
    std::cout << s << std::endl;
}

class A {
    protected:
    std::string name;
  
    
    public:
        A(){
        }

    //There are 8 macros for declaring functions overwritable in lua, 
    // any combination of with or without return type, and with or without native implementation, for both passing the class as first param or no first param
    
    // None of this pass this as first param
    //LUA_FUNC_VOID(foo) //This declares a void foo() that can be overwritten in lua
    //LUA_FUNC(int, foo) //This declares a int foo() that can be overwritten in lua  
    //NATIVE_LUA_FUNC_VOID(foo) //declares a void native_foo() that is called if the lua script didn't overwrote foo
    //NATIVE_LUA_FUNC_VOID(int, foo) //declares a int native_foo() that is called if the lua script didn't overwrote foo
    
    // All of this pass this as first argument
    //LUA_CLASS_FUNC_VOID(foo) //This declares a void foo() that can be overwritten in lua
    //LUA_CLASS_FUNC(int, foo) //This declares a int foo() that can be overwritten in lua  
    //NATIVE_LUA_CLASS_FUNC_VOID(foo) //declares a void native_foo() that is called if the lua script didn't overwrote foo
    //NATIVE_LUA_CLASS_FUNC_VOID(int, foo) //declares a int native_foo() that is called if the lua script didn't overwrote foo
    
    
    // Any of them can have up to 8 parameters
    
    
    // example
    NATIVE_LUA_CLASS_FUNC(int, //This is the return type
        interact, //This is the function name 
             std::string, //This is the first parameter
             int) //Second param
    //Will be replaced by
//     protected:    
//         std::shared_ptr<luabridge::LuaRef> lua_interact;
//     public:
//          void interact(std::string paramName) {
//             if (lua_interact) {
//                 try{
//                     (*lua_interact)(this, paramName);
//                 }
//                 catch (luabridge::LuaException const& e) {
//                     std::cout << "LuaException: " << e.what() << std::endl;
//                 }
//             }
//         }
     
    
    std::string getName() const {
        return name;
    }
    void setName(const std::string& value) {
        name = value;
    }
    
    void loadScript(const std::string& scriptFilename, const std::string& tableName) {
        LUA::loadFile(scriptFilename);
        LuaRef table = LUA::getTable(tableName);
        loadTable(table);
    }
    void loadTable(LuaRef table) {
        LUA_LOAD_STRING(table,name)
        //Every function can be loaded from lua using the same macro
        LUA_LOAD_FUNC(table,interact," in my example class")
    }
                  //This is LUA_LOAD_STRING
//                 if (table["name"].isString()) {
//                     name = table["name"].cast<std::string>();
//                 } else {
//                     name = "Null";
//                 }

//                 this is LUA_LOAD_FUNC
//                 if (table["interact"].isFunction()) {
//                     std::cout << "loading interact" << std::endl;
//                     lua_interact = std::make_shared<LuaRef>(table["interact"]);
//                 } else {
//                     lua_interact.reset();
//                 }
    
};

//You need to remember to implement the native functions that are expected to exist
int A::native_interact(std::string p1, int p2){
    std::cout << "This is the native implementation" << std::endl;
    return 1;
}

int main() {
    LUA::init();

    //This needs to be done before loading the script in order to have acces to the exported functions
    getGlobalNamespace(LUA::STATE)
        .beginNamespace("game") //You can put things in a namespace, even if they're not in one
            .addFunction("log", printMessage) // The functions can have different names in lua
             .beginClass<A>("A")
                .addConstructor<void(*) (void)>()
                .addProperty("name", &A::getName, &A::setName) //Functions can be binded to geters and seters to make a property in lua
             .endClass()
        .endNamespace();
    
    std::cout << "Registered namespace" << std::endl;
        
    luaL_dofile(LUA::STATE, "../../../resources/scripts/script.lua");
    LuaRef table = LUA::getTable("lua_A");
    //After a script is loaded you can 
    std::vector<std::string> keys = LUA::getTableKeys(table);
    std::cout << "Keys are: " << std::endl;
    for(auto k : keys){
        std::cout << "- " << k << std::endl;
    }

    A a;
    a.setName("C-A");
    std::cout << "C A is: " << a.getName() << std::endl;
    
    A b;
    b.loadScript("../../../resources/scripts/script.lua", "lua_A");
    std::cout << "Lua A is: " << b.getName() << std::endl;
    int ret = b.interact("c string", 5);
    std::cout << "returned : " << ret << std::endl;
}
