#include <iostream>

#include "ECS/entity.hpp"
#include "ECS/component.hpp"
#include "ECS/system.hpp"

#include "ECS/components/spriteComp.hpp"

int main(){
    
    std::cout << "Creating entity" << std::endl;
    Entity e("e");
  
//     std::shared_ptr<Component> comp1 = std::make_shared<Component>(); //Component is abstract now
//     e.addComponent(comp1);
    
    std::shared_ptr<SpriteComp> spriteComp = std::make_shared<SpriteComp>();
    e.addComponent(spriteComp);
    
    std::cout << "Entity has : " << e.getComponents().size() << " components, but only " << e.getComponents<SpriteComp>().size() << " spriteComponents" << std::endl;
    
     std::shared_ptr<SpriteComp> spriteComp2 = e.getComponent<SpriteComp>();
     if(spriteComp == spriteComp2){
         std::cout << "the spriteComp inside the entity is the same as outside" << std::endl;
    }
    
    return 0;
}
