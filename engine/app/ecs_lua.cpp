#include <iostream>

#include "managers/assetManager.hpp"
#include "managers/localeManager.hpp"

#include "ECS/entity.hpp"
#include "ECS/component.hpp"
#include "ECS/system.hpp"

#include "ECS/components/spriteComp.hpp"
#include "ECS/components/collisionComp.hpp"
#include "ECS/components/scriptComp.hpp"
#include "ECS/components/textComp.hpp"

#include "ECS/systems/drawing.hpp"

#include "ECS/systems/systemsManager.hpp"

#include "graphics/map/tiledMap.hpp"

int main(){
     Window main_window("title", 800,600, SDL_WINDOW_SHOWN, Window::Interpolation::NEAREST); 
    LUA::init();
    
    SystemsManager::get().init();
    
//     SystemsManager::DEBUG = true;
    
//     LuaEntityHandle::registerInLua(); //It's done inside LUA::init() now
    
    /* 
    entity1 = {                                                                                                      
    name= "Entity1",                                                                                              
    Sprite = {                                                                                                    
        texture="TEXTURE_1",                                                                                       
        rect= {410, 320, 230, 140}                                                                                 
    },                                                                                                            
    Collision = {                                                                                                 
        rect={0,0,20,20},                                                                                          
        collide = function(this, other)                                                                            
            print(this.name, " collided with ", other.name)                                                         
        end                                                                                                        
    }                                                                                                             
    }                                                                                                                
                                                                                                                    
    entity2 = {                                                                                                      
    name= "Entity2",                                                                                              
    Sprite = {                                                                                                    
        texture="TEXTURE_2",                                                                                       
        rect= {410, 320, 230, 140}                                                                                 
    },                                                                                                            
    Collision = {                                                                                                 
        rect={0,0,20,20},                                                                                          
        collide = function(this, other)                                                                            
            print(this.name, " collided with ", other.name)                                                         
        end                                                                                                        
    }                                                                                                             
    }                                                                                                                
  
    
    */
    
    
// //     LuaRef drawings = LUA::getTable("../../../resources/scripts/script.lua", "Drawing");
// //     d.loadFromLua(drawings);
    
    //Loading configurations for the systems, things like Drawing layers, etc
    LUA::loadFile("../../../resources/scripts/script.lua");
    LuaRef systems = LUA::getTable("Systems");
    SystemsManager::get().loadFromLua(systems);
    
    //Loading asset table
    LuaRef assets = LUA::getTable("assets");
    AssetManager& am = AssetManager::instance();
    am.addAssetFromLua(assets);
    am.load(); //Just load everything now
    
    //loading locale table
    LuaRef locale_en = LUA::getTable("locale_en");
    LuaRef locale_pt = LUA::getTable("locale_pt");
    LocaleManager::instance().loadStringsFromLua(locale_en);
    
    LuaRef entity = LUA::getTable("entity1");
    std::cout << "Creating entity" << std::endl;
    Entity e1("entity1");
    std::cout << "Loading it from lua" << std::endl;
    e1.loadFromLua(entity);
        
    std::cout << "Loaded " << e1.getName() << std::endl;
  
    std::cout << "Entity has : " << e1.getComponents().size() << " components, but only " << e1.getComponents<SpriteComp>().size() << " spriteComponents" << std::endl;
    
    auto script = e1.getComponent<ScriptComp>();
    if(script){
        std::cout << "Forcing an update of 0.123f" << std::endl;
        script->update(0.123f);
    }
    
    auto action = e1.getComponent<ActionComp>();
    if(action){
        std::cout << "Forcing first action (" << action->actionName() << ") on" << std::endl;
        action->on();
    }
    
    std::cout << "Loading entity2" << std::endl;
    LuaRef entity2 = LUA::getTable("entity2");
    Entity e2("entity2");
    e2.loadFromLua(entity2);
    
    
    auto col = e1.getComponent<CollisionComp>();
    if(col){
        std::cout << "Found a collision component on first entity, colliding with 2" << std::endl;
        col->collide(e2.getLuaHandle());
    }
    else{
        std::cout << "No collision component" << std::endl;
    }
    
//     Sprite s(am->getTexture("TEXTURE_1"));
    
    
     std::shared_ptr<Input> input = SystemsManager::get().getSystem<Input>();
    
     input->setCurrentMode("GameMode");
     
//     d.update(0.1);
//     SDL_Event event;
    bool alive = true;
    while(alive){
        SystemsManager::get().update(0.12f);
        //Input manager handles the exit request and stores it in exitRequested
        alive = ! input->exitRequested();
        //It also provides a list of all events that weren't handled by it this frame
        for(auto e : input->unhandledEvents()){
            if(e.type == SDL_KEYDOWN){
                if(e.key.keysym.sym == SDLK_ESCAPE){
                    std::cout << "Escaping" << std::endl;
                    alive = false;
                }
                else if(e.key.keysym.sym == SDLK_1){
                    std::cout << "Regenerating text" << std::endl;
                     LocaleManager::instance().loadStringsFromLua(locale_pt);
                     LocaleManager::instance().localeChanged();
//                      e1.getComponent<TextComp>()->regenText();
                }
                else if(e.key.keysym.sym == SDLK_2){
                    input->setCurrentMode("GameMenu");
                }
                 else if(e.key.keysym.sym == SDLK_3){
                    input->setCurrentMode("GameMode");
                }
                else if(e.key.keysym.sym == SDLK_4){
                    am.getMap("MY_MAP")->registerDrawing();
                }
                else if(e.key.keysym.sym == SDLK_5){
                    am.getMap("MY_MAP")->deregisterDrawing();
                }
            }
        }
        
        
       
//         main_window.draw(&s);
        main_window.display();
    }
    
    
    std::cout << "Calling shutdown" << std::endl;
     SystemsManager::get().shutdown();
    
    return 0;
}

