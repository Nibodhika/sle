#include <iostream>

#include "graphics/window.hpp"
#include "graphics/texture.hpp"
#include "input/keyboard.hpp"

#include "graphics/spriteSheetAnimation.hpp"


int main(){

  //Creating the window. This will start most of th sdl stuff, and create a Window::main_window and Window::main_renderer
  Window main_window("title", 800,600);

  
  //Opening an image as a texture
  std::string imagePath = "../../../resources/sheet.png";
  Texture t(imagePath);
  SpriteSheetAnimation a(&t,4,4,12,4);
  a.setFPS(5);


  SDL_Event e;
  bool alive = true;  
  
  Uint32 prev_time = SDL_GetTicks();
  Uint32 current_time = SDL_GetTicks();
  float dt = 0;
  //Main loop
  while(alive){
      current_time = SDL_GetTicks();
      dt = (current_time - prev_time) / 1000.f;
      prev_time = current_time;
      
    Point move(0,0);
    //Event handler, using native SDL, no wrapping here
    while( SDL_PollEvent( &e ) != 0 ) {
      //User requests quit
      if( e.type == SDL_QUIT ) {
        printf("closing\n");
        alive = false;
      }
      //User presses a key
      else if( e.type == SDL_KEYDOWN ){
        //Select surfaces based on key press
        switch( e.key.keysym.sym ) {
        case SDLK_ESCAPE:
          std::cout << "escaping" << std::endl;
          alive = false;
          break;
          case SDLK_RETURN:
          std::cout << "pausing" << std::endl;
          if(a.isPaused())
              a.play();
          else
              a.pause();
          break;
          case SDLK_SPACE:
          std::cout << "loop changing from " << a.isLooping() << std::endl;
          a.setLoop( !a.isLooping());
//           if(a.isPaused())
//               a.play();
//           else
//               a.stop();
          break;
        }
      }

    }


//     Sprite* s = a.nextFrame();
    
    a.update(dt);
    
    //First clear the window
    main_window.clear();
    //Draw the texture occupying the entire screen
    main_window.draw(&a);
    //Display the screen
    main_window.display();
  }
  
  
  //No need to worry about closing SDL, it will automatically close whenever the main_window is destroyed

  return 0;
}

