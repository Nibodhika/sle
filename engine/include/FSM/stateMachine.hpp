#ifndef STATE_MACHINE_HPP
#define STATE_MACHINE_HPP

#include <vector>
#include <string>

#include "FSM/state.hpp"

template<typename T>
class StateMachine {
public:
    StateMachine<T>(T& obj) : m_obj(obj){
    };
    
    virtual void changeState(std::shared_ptr<State<T>> new_state){
        if(m_currentState)
            m_currentState->exit(m_obj);
        m_currentState = new_state;
         if(m_currentState)
            m_currentState->enter(m_obj);
    }
    
    std::shared_ptr<State<T>> getCurrentState(){
        return m_currentState;
    }
    
    bool isInState(std::shared_ptr<State<T>> state){
        return m_currentState == state;
    }
    
    //! A global state is a state that gets called every time update gets called, independent of the current state
    virtual void setGlobalState(std::shared_ptr<State<T>> state){
        m_globalState = state;
    }
    
    std::shared_ptr<State<T>> getGlobalState(){
        return m_globalState;
    }
    
    virtual void checkStateChange() = 0;
    
    virtual void update(float dt) {
        if(m_globalState)
            m_globalState->update(m_obj,dt);
        if(m_currentState)
            m_currentState->update(m_obj,dt);
        checkStateChange();
    }
    
protected:
    std::shared_ptr<State<T>> m_currentState = nullptr;
    std::shared_ptr<State<T>> m_globalState = nullptr;
    T& m_obj;
};

#endif
