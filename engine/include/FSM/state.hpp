#ifndef STATE_HPP
#define STATE_HPP

#include "lua.hpp"

template<typename T>
class State {
    public:
        State<T>(std::string name){
            m_name = name;
        };
        
        virtual void enter(T& obj) = 0;
        virtual void update(T& obj, float dt) = 0;
        virtual void exit(T& obj) = 0;
//         virtual void onMessage(T& obj, std::string message);
        
        std::string getName(){
            return m_name;
        }
        
protected:
        std::string m_name;
        
        
//         NATIVE_LUA_FUNC_VOID(enter)
//         NATIVE_LUA_FUNC_VOID(update)
//         NATIVE_LUA_FUNC_VOID(exit)
        
};

#endif
