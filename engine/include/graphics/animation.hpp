#ifndef ANIMATION_HPP
#define ANIMATION_HPP

#include <vector>
#include <graphics/sprite.hpp>

class Animation : public Drawable {
public:
    Animation();
    ~Animation();
    
    virtual void update(float dt) = 0;
    
    void pause(); //Pauses the animation in the current frame
    void play(); //unpauses the animation
    void stop(); //Pauses the animation and resets the current frame to 0
    bool isPaused();
    void setLoop(bool v);
    bool isLooping();
    
    virtual void reset() = 0; // Function to be called when the animation stops to reset it to initial parameters
    
protected:
    bool m_paused = false;
    bool m_looping = true;
    
};

#endif
