#ifndef SPRITE_SHEET_ANIMATION_HPP
#define SPRITE_SHEET_ANIMATION_HPP

#include "graphics/animation.hpp"

class SpriteSheetAnimation : public Animation {
public:
    SpriteSheetAnimation();
    SpriteSheetAnimation(Texture* t, 
                         int amount_x, int amount_y = 1, //Amount of divisions in the sprite sheet
                         int initial_anim = 0, //First animation to start counting
                         int amount_anim = -1); //Amount of animations in the spritesheet, so you can load for example the first 8 anims);
    
    
    virtual void update(float dt);  //updates a counter and after a given time has passed changes the current frame
    
    void setFrame(unsigned int f_number);
    void nextFrame();
    void setFPS(float fps);
    float getFPS();
    
    virtual void draw(Window* window);
     
    virtual void reset();
     
protected:
    unsigned int m_currentFrame = 0;
    unsigned int m_amount_frames;
    float m_spf; //Seconds per frame 1/fps
    float m_time; //time to count for frames
    
    void addFrame(Image subRect);
    std::vector<Image> m_frames;
    Image* m_currentImage;
    void onSetFrame(unsigned int f_number);
    
   
};

#endif
