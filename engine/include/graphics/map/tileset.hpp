#ifndef TILESET_HPP
#define TILESET_HPP

#include "json/json.hpp"
// for convenience
using json = nlohmann::json;

#include <graphics/sprite.hpp>

//TODO refactor this class to use things correctly

class Tileset{
public:
    Tileset(json json);
    ~Tileset();
    
    bool hasGid(int gid);
    
    Sprite* createSprite(int gid);
//     Sprite* getSprite(int x, int y);
    
protected:
    int m_firstgid, m_lastgid;
    std::string m_name;
    int m_margin;
    int m_spacing;
    //Size of the tile
    int m_tilewidth;
    int m_tileheight;
    int m_columns;
    Texture* m_texture = nullptr;
    std::vector<Image*> m_images;
};

#endif
