#ifndef TILED_MAP_MAP_HPP
#define TILED_MAP_MAP_HPP

#include <string>
#include <vector>

#include "graphics/map/tileset.hpp"
#include "graphics/map/layer.hpp"

#include "ECS/entity.hpp"

/*!
 Map is a specialized entity that loads from a json exported in tiled
 */

class TiledMap : public Entity {
public:
    TiledMap(std::string name, Entity* parent = nullptr);
    ~TiledMap();
    
    bool open(std::string filepath);
    
    Sprite* createSprite(int gid);
    
    int tileWidth();
    int tileHeight();
    
//     void draw(Window* window);
    
    void registerDrawing();
    void deregisterDrawing();
    
protected:
    int m_width, m_height; //Map width and height in tiles
    int m_tilewidth, m_tileheight; //width and height of each tile in pixels

    std::vector<Tileset*> m_tilesets;
    std::vector<std::shared_ptr<MapLayer>> m_layers;
    
};

#endif
