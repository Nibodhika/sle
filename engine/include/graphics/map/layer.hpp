#ifndef MAP_LAYER_HPP
#define MAP_LAYER_HPP

#include "json/json.hpp"
// for convenience
using json = nlohmann::json;

#include <graphics/window.hpp>
#include <graphics/sprite.hpp>
#include "graphics/spriteList.hpp"
#include "ECS/components/graphicsComp.hpp"

class TiledMap;

/*!
 A Layer is a graphical component that draws an entire layer of the map
 */
class MapLayer : public GraphicsComp {
public:
    enum Type {unknown, tilelayer, objectgroup};
    static std::string type2String(Type t);
    static Type string2Type(std::string t);
    
    MapLayer( json json, TiledMap* map);
    ~MapLayer();
    
    virtual bool load(LuaRef& table,AssetManager& m = AssetManager::instance()){
        std::cerr << "ERROR: Layers are not supposed to be loaded from lua" << std::endl;
        return false;
    };
    
protected:
    std::string m_name;
    float m_opacity; //TODO verificar se eh float mesmo
    Type m_type;
    TiledMap* m_map;
    SpriteList* m_spriteList;
//     std::vector<Sprite*> m_sprites;
};

#endif
