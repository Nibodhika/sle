#ifndef SPRITE_LIST_HPP
#define SPRITE_LIST_HPP

#include <vector>
#include <graphics/sprite.hpp>
#include <graphics/drawable.hpp>

class SpriteList : public Drawable {
public:
    SpriteList();
    ~SpriteList();
    
    virtual void draw(Window* window);
    
    void addSprite(Sprite* s);
    
protected:
    std::vector<Sprite*> m_spriteList;
};

#endif
