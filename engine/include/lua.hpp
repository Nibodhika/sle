#ifndef LUA_ENGINE
#define LUA_ENGINE

#include <iostream>
#include <memory>

extern "C" 
{
    #include <lua.h>
    #include <lauxlib.h>
    #include <lualib.h>
}

#include <LuaBridge.h>
#include <vector>

using namespace luabridge;

namespace LUA {
    extern lua_State* STATE;
    void init(bool registerECS = true);
    bool loadFile(const std::string& scriptFilename);
    LuaRef getTable(const std::string& tableName);
    std::vector<std::string> getTableKeys(const LuaRef& table, bool recursive = true, std::string base = "");
}

#define LUA_NULL_STRING "NULL"

//Counts the number of arguments in __VA_ARGS__ by calling the next function with the parameters shifted by the amount of arguments in __VA_ARGS__
// Counts to a maximum of 16 parameters which mean 8 parameters passed to lua. since LuaBridge can only candle 8 parameters there's no need to handle more
#define CNT_ARGS(...) CNT_ARGS_( , ## __VA_ARGS__,8,7,6,5,4,3,2,1,0)
#define CNT_ARGS_(_0,_1,_2,_3,_4,_5,_6,_7,_8,n, ...) n

//When there are 0 arguments I can't start the name list with a , int a, int b -> function(a,b) void -> function(,)
#define SHOULD_PUT_COMMA(...) SHOULD_PUT_COMMA_(CNT_ARGS(__VA_ARGS__))
#define SHOULD_PUT_COMMA_(n) SHOULD_PUT_COMMA__(n)
#define SHOULD_PUT_COMMA__(n) SHOULD_PUT_COMMA_##n
#define SHOULD_PUT_COMMA_0
#define SHOULD_PUT_COMMA_1 ,
#define SHOULD_PUT_COMMA_2 ,
#define SHOULD_PUT_COMMA_3 ,
#define SHOULD_PUT_COMMA_4 ,
#define SHOULD_PUT_COMMA_5 ,
#define SHOULD_PUT_COMMA_6 ,
#define SHOULD_PUT_COMMA_7 ,
#define SHOULD_PUT_COMMA_8 ,

// Drops the type and concatenates the names created by DECLARE_NAME
#define DROP_TYPE(...) DROP_TYPE_(CNT_ARGS(__VA_ARGS__),__VA_ARGS__)
//The first time it expands to DROP_TYPE_( CNT_ARGS literally, so if not using the __ it tries to access DROP_TYPE_CNT_ARGS which is not defined
#define DROP_TYPE_(n,...) DROP_TYPE__(n,__VA_ARGS__)
#define DROP_TYPE__(n,...) DROP_TYPE_##n(__VA_ARGS__)
#define DROP_TYPE_0(...)
#define DROP_TYPE_1(ptype,...) p1
#define DROP_TYPE_2(ptype,...) p2, DROP_TYPE_1(__VA_ARGS__)
#define DROP_TYPE_3(ptype,...) p3, DROP_TYPE_2(__VA_ARGS__)
#define DROP_TYPE_4(ptype,...) p4, DROP_TYPE_3(__VA_ARGS__)     
#define DROP_TYPE_5(ptype,...) p5, DROP_TYPE_4(__VA_ARGS__)  
#define DROP_TYPE_6(ptype,...) p6, DROP_TYPE_5(__VA_ARGS__)  
#define DROP_TYPE_7(ptype,...) p7, DROP_TYPE_6(__VA_ARGS__)  
#define DROP_TYPE_8(ptype,...) p8, DROP_TYPE_7(__VA_ARGS__)  
// Declares with the name, for example: std::string, v1, int, v2 becomes std::string v1,in v2
#define DECLARE_NAME(...) DECLARE_NAME_(CNT_ARGS(__VA_ARGS__),__VA_ARGS__)
#define DECLARE_NAME_(n,...) DECLARE_NAME__(n,__VA_ARGS__)
#define DECLARE_NAME__(n,...) DECLARE_NAME_##n(__VA_ARGS__)
#define DECLARE_NAME_0(...)
#define DECLARE_NAME_1(ptype,...) ptype p1
#define DECLARE_NAME_2(ptype,...) ptype p2, DECLARE_NAME_1(__VA_ARGS__)
#define DECLARE_NAME_3(ptype,...) ptype p3, DECLARE_NAME_2(__VA_ARGS__)
#define DECLARE_NAME_4(ptype,...) ptype p4, DECLARE_NAME_3(__VA_ARGS__)  
#define DECLARE_NAME_5(ptype,...) ptype p5, DECLARE_NAME_4(__VA_ARGS__)  
#define DECLARE_NAME_6(ptype,...) ptype p6, DECLARE_NAME_5(__VA_ARGS__)  
#define DECLARE_NAME_7(ptype,...) ptype p7, DECLARE_NAME_6(__VA_ARGS__)  
#define DECLARE_NAME_8(ptype,...) ptype p8, DECLARE_NAME_7(__VA_ARGS__)  


//Implements a function called function_name that can be "overloaded" in lua, example LUA_FUNC(void, interact, std::string, int)
//There are 4 macros for function, function without return, function with native implementation and function with native implementation and no return
//WARNING these 4 macros will change the scope to public

//Private method, starts the declaration of a function, the same for all 4
#define PRIVATE_LUA_FUNC_PRE(ret_type, function_name, ...)\
                protected: \
                 std::shared_ptr<luabridge::LuaRef> lua_##function_name; \
                 public: \
                  ret_type function_name(DECLARE_NAME(__VA_ARGS__)) { \
                      if (lua_##function_name) { \
                        try{
                            
// Private method, ends the function without a native implementation
#define PRIVATE_LUA_FUNC_POS \
                            }\
                        catch (luabridge::LuaException const& e) {\
                            std::cout << "LuaException: " << e.what() << std::endl;\
                        }\
                    }\
                  }
//Private method, starts the declaration of a function, the same for all 4
#define PRIVATE_NATIVE_LUA_FUNC_PRE(ret_type, function_name, ...)\
                protected: \
                 std::shared_ptr<luabridge::LuaRef> lua_##function_name; \
                 virtual ret_type native_##function_name(DECLARE_NAME(__VA_ARGS__)); \
                 public: \
                  ret_type function_name(DECLARE_NAME(__VA_ARGS__)) { \
                      if (lua_##function_name) { \
                        try{
// Private method, ends the function and declares a native implementation                  
#define PRIVATE_NATIVE_LUA_FUNC_POS(ret_type,function_name,...) \
                           }\
                        catch (luabridge::LuaException const& e) {\
                            std::cout << "LuaException: " << e.what() << std::endl;\
                        }\
                    }\
                    else {\
                        return native_##function_name(DROP_TYPE(__VA_ARGS__));\
                    }\
                  }
                  

//CLASS means pass this as first argument

// Declares an overwritable function with a return type without native implementation
#define LUA_CLASS_FUNC(ret_type, function_name, ...)  \
                    PRIVATE_LUA_FUNC_PRE(ret_type,function_name, __VA_ARGS__) \
                        return (*lua_##function_name)(this SHOULD_PUT_COMMA(__VA_ARGS__) DROP_TYPE(__VA_ARGS__));\
                    PRIVATE_LUA_FUNC_POS   
                  
// Declares an overwritable function with void type without native implementation
#define LUA_CLASS_FUNC_VOID(function_name, ...)  \
                PRIVATE_LUA_FUNC_PRE(void,function_name, __VA_ARGS__) \
                            (*lua_##function_name)(this SHOULD_PUT_COMMA(__VA_ARGS__) DROP_TYPE(__VA_ARGS__));\
                PRIVATE_LUA_FUNC_POS   
                  
// Declares an overwritable function with a return type and a native implementation
#define NATIVE_LUA_CLASS_FUNC(ret_type, function_name, ...)  \
                PRIVATE_NATIVE_LUA_FUNC_PRE(ret_type,function_name, __VA_ARGS__) \
                          return (*lua_##function_name)(this SHOULD_PUT_COMMA(__VA_ARGS__) DROP_TYPE(__VA_ARGS__));\
                PRIVATE_NATIVE_LUA_FUNC_POS(ret_type,function_name, __VA_ARGS__) \
                  
// Declares an overwritable function with void type and a native implementation
#define NATIVE_LUA_CLASS_FUNC_VOID(function_name, ...)  \
                 PRIVATE_NATIVE_LUA_FUNC_PRE(void,function_name, __VA_ARGS__) \
                          (*lua_##function_name)(this SHOULD_PUT_COMMA(__VA_ARGS__) DROP_TYPE(__VA_ARGS__));\
                PRIVATE_NATIVE_LUA_FUNC_POS(ret_type,function_name, __VA_ARGS__) \
        
        
        
        
// Declares an overwritable function with a return type without native implementation
#define LUA_FUNC(ret_type, function_name, ...)  \
                    PRIVATE_LUA_FUNC_PRE(ret_type,function_name, __VA_ARGS__) \
                        return (*lua_##function_name)(DROP_TYPE(__VA_ARGS__));\
                    PRIVATE_LUA_FUNC_POS   
                  
// Declares an overwritable function with void type without native implementation
#define LUA_FUNC_VOID(function_name, ...)  \
                PRIVATE_LUA_FUNC_PRE(void,function_name, __VA_ARGS__) \
                            (*lua_##function_name)(DROP_TYPE(__VA_ARGS__));\
                PRIVATE_LUA_FUNC_POS   
                  
// Declares an overwritable function with a return type and a native implementation
#define NATIVE_LUA_FUNC(ret_type, function_name, ...)  \
                PRIVATE_NATIVE_LUA_FUNC_PRE(ret_type,function_name, __VA_ARGS__) \
                          return (*lua_##function_name)(DROP_TYPE(__VA_ARGS__));\
                PRIVATE_NATIVE_LUA_FUNC_POS(ret_type,function_name, __VA_ARGS__) \
                  
// Declares an overwritable function with void type and a native implementation
#define NATIVE_LUA_FUNC_VOID(function_name, ...)  \
                 PRIVATE_NATIVE_LUA_FUNC_PRE(void,function_name, __VA_ARGS__) \
                          (*lua_##function_name)(DROP_TYPE(__VA_ARGS__));\
                PRIVATE_NATIVE_LUA_FUNC_POS(ret_type,function_name, __VA_ARGS__) \
        
        
        
        
        
// Loaders macros
//Loads a string to a variable named var_name
#define LUA_LOAD_STRING(table,var_name) \
         if (table[#var_name].isString()) {\
                    var_name = table[#var_name].cast<std::string>();\
                } else {\
                    var_name = LUA_NULL_STRING;\
                }\
                
//Loads a number to a variable named var_name
#define LUA_LOAD_NUMBER(table,var_name) \
         if (table[#var_name].isNumber()) {\
                    var_name = table[#var_name].cast<double>();\
                } else {\
                    var_name = -1;\
                }\
                
//Loads the function_name from the table
#define LUA_LOAD_FUNC(table,function_name, error_msg) \
            if (table[#function_name].isFunction()) { \
                    lua_##function_name = std::make_shared<LuaRef>(table[#function_name]); \
                } else { \
                    std::cout << "WARNING: " <<  #function_name " is not a function " << error_msg << std::endl; \
                    lua_##function_name.reset(); \
                }  

                
//iterates through the table and grants access to the keys and values only when key is a string.             
#define FOREACH_KEY_VAL(table) \
        for (Iterator iter (table); !iter.isNil (); ++iter) { \
        LuaRef key = iter.key (); \
        if(key.isString() ) { \
            LuaRef val = *iter; \
        
#define END_FOREACH \
        }}

        
#define LUA_LOAD_POINT(table,point_name,error_msg) \
            LuaRef table_##point_name = table[#point_name]; \
            LUA_DO_LOAD_POINT(table_##point_name,point_name,error_msg)

#define LUA_DO_LOAD_POINT(luaRef,point_name,error_msg)\
        if (luaRef.isTable()) { \
                std::vector<int> numbers; \
                for (Iterator iter_rect (luaRef); !iter_rect.isNil (); ++iter_rect) { \
                    LuaRef number = *iter_rect; \
                    if(number.isNumber()) {\
                        numbers.push_back(number); \
                    } \
                }\
                if(numbers.size() > 0)\
                    point_name.x(numbers[0]); \
                if(numbers.size() > 1)\
                    point_name.y(numbers[1]); \
            } else { \
              std::cout << "WARNING: " <<  #point_name " is not a valid point " << error_msg << std::endl; \
            }
            
            
//Loads the rect from the table
#define LUA_LOAD_RECT(table,rect_name,error_msg) \
            LuaRef table_##rect_name = table[#rect_name]; \
            LUA_DO_LOAD_RECT(table_##rect_name,rect_name,error_msg)
 
#define LUA_DO_LOAD_RECT(luaRef,rect_name,error_msg)\
        if (luaRef.isTable()) { \
                std::vector<int> numbers; \
                for (Iterator iter_rect (luaRef); !iter_rect.isNil (); ++iter_rect) { \
                    LuaRef number = *iter_rect; \
                    if(number.isNumber()) {\
                        numbers.push_back(number); \
                    } \
                }\
                rect_name.fromVector(numbers); \
            } else { \
              std::cout << "WARNING: " <<  #rect_name " is not a valid rect " << error_msg << std::endl; \
            }
#endif
