#ifndef GRAPHICS_COMP_HPP
#define GRAPHICS_COMP_HPP

#include <graphics/drawable.hpp>
#include <base/transform.hpp>
#include "ECS/component.hpp"


class GraphicsComp : public Component{
public:
    GraphicsComp(Entity* entity = nullptr);
    ~GraphicsComp();
    
    std::string getLayer();
    void updateTransform();
    Drawable* getDrawable();
    
protected:
    void loadDrawable(luabridge::LuaRef& table,Drawable* drawable);
    Drawable* m_drawable= nullptr;
    std::string m_layer;
    Transform m_transform;
};

#endif
