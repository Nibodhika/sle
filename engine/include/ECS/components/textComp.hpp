#ifndef TEXT_COMP_HPP
#define TEXT_COMP_HPP

#include <graphics/text.hpp>
#include "ECS/components/graphicsComp.hpp"

class TextComp : public GraphicsComp {
public:
    TextComp(Entity* entity = nullptr);
    ~TextComp();
     
    bool load(luabridge::LuaRef& table, AssetManager& m = AssetManager::instance()) override;
    
    static std::string TAG;
    static std::string TAG_PLURAL;
    
    void regenText();
    
    Text* text();
    
protected:
    Text* m_text = nullptr;
    std::string m_text_id;
};

#endif
