#ifndef COLLISION_COMPONENT_HPP
#define COLLISION_COMPONENT_HPP

#include "base/rect.hpp"

#include "ECS/component.hpp"
#include "ECS/entity.hpp"
#include "ECS/luaEntityHandle.hpp"

/*
 * Collision Components can be loaded from lua using the following syntax
 
   Collision = {                                                                                                 
      rect={0,0,20,20},                                                                                          
      collide = function()                                                                                       
         print("Collided in lua")                                                                                
      end                                                                                                        
   }  
   */

class CollisionComp : public Component {
public:
    CollisionComp(Entity* entity = nullptr);
    
    bool load(luabridge::LuaRef& table,AssetManager& m = AssetManager::instance()) override;
    
    static std::string TAG;
    static std::string TAG_PLURAL;
    
    //void collide(LuaEntityHandle& other)
    LUA_COMPONENT_FUNC_VOID(collide, LuaEntityHandle&)
    
protected:
    Rect m_rect;
    
};


#endif
