#ifndef SPRITE_COMPONENT_HPP
#define SPRITE_COMPONENT_HPP

#include <graphics/sprite.hpp>

#include "ECS/components/graphicsComp.hpp"


/*
 * Sprite Components can be loaded from lua using the following syntax
 
    Sprite = {                                                                                                    
      texture="Texture ID",                                                                                       
      rect= {x,y,w,h}                                                                                 
   }
 
 */

class SpriteComp : public GraphicsComp {
public:
    SpriteComp(Entity* entity = nullptr);
    ~SpriteComp();
    
    bool load(luabridge::LuaRef& table, AssetManager& m = AssetManager::instance()) override;
    
    static std::string TAG;
    static std::string TAG_PLURAL;
    
    
    Sprite* sprite();
    
protected:
    Sprite* m_sprite = nullptr;
};

#endif
