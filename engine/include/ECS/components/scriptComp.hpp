#ifndef SCRIPT_COMP_HPP
#define SCRIPT_COMP_HPP

#include "ECS/component.hpp"
#include "ECS/luaEntityHandle.hpp"
#include "ECS/entity.hpp"

class ScriptComp : public Component {
public:
    ScriptComp(Entity* entity = nullptr);
    
    bool load(luabridge::LuaRef & table,AssetManager& m = AssetManager::instance()) override;
    
    static std::string TAG;
    static std::string TAG_PLURAL;
    
    // TODO Descobrir como/quando chamar essa funcao
    //void start()
    LUA_COMPONENT_FUNC_VOID(start)

    //void update(float dt);
    LUA_COMPONENT_FUNC_VOID(update, float)
    
};

#endif
