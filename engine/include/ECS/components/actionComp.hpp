#ifndef ACTION_COMP_HPP
#define ACTION_COMP_HPP

#include "ECS/component.hpp"
#include "ECS/entity.hpp"

class ActionComp : public Component {
public:
    ActionComp(Entity* parent);
    ~ActionComp();
    
     bool load(luabridge::LuaRef & table,AssetManager& m = AssetManager::instance()) override;
     
     static std::string TAG;
     static std::string TAG_PLURAL;
     
     std::string actionName();
     
protected:
     std::string action;
     
     //void on() //called when action starts (i.e. when key was pressed)
     LUA_COMPONENT_FUNC_VOID(on)
     //void off() //called when action ends (i.e. when key was released)
     LUA_COMPONENT_FUNC_VOID(off)
     //void during(float dt, float axis) //called every frame when action is happening, first param is delta time, second param if a button param will be 1, if an axis 0-1 ranged
     LUA_COMPONENT_FUNC_VOID(during, float, float)
     
};

#endif
