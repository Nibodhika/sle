#ifndef COMPONENT_FACTORY_HPP
#define COMPONENT_FACTORY_HPP

#include <memory>

#include "lua.hpp"
#include "ECS/component.hpp"
#include "ECS/entity.hpp"
#include "ECS/system.hpp"
#include "ECS/systems/systemsManager.hpp"

namespace ComponentFactory{
    
    //Loads a component of type component
     template<class component>
     std::shared_ptr<Component> loadSingular(LuaRef& table, Entity* parent,AssetManager& m){
         std::shared_ptr<Component> out = std::make_shared<component>(parent);
         bool couldLoad = out->load(table,m);
//          return out;
         if(couldLoad)
             return out;
         return nullptr;
    }
    
     //Loads a component of type component and registers it in system of type system
    template<class component, class system>
     std::shared_ptr<Component> loadSingular(LuaRef& table, Entity* parent,AssetManager& m){
         std::shared_ptr<Component> out = loadSingular<component>(table,parent,m);
         if(out) {
            std::shared_ptr<System> sys = SystemsManager::get().getSystem<system>();
            if(sys)
                sys->registerComponent(out);
         }
         return out;
    }
    
   
    

     std::vector<std::shared_ptr<Component>> load(std::string name, LuaRef& table, Entity* parent,AssetManager& m = AssetManager::instance());
     
};

#endif
