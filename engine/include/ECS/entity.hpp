
#ifndef ENTITY_HPP
#define ENTITY_HPP

/*
 * The entity is a general purpose object. Usually, it only consists of a unique id. They "tag every coarse gameobject as a separate item". Implementations typically use a plain integer for this.
 * 
 * In this implementation they also have a transform that components can access
 */
#include <memory>
#include <vector>

#include <base/transform.hpp>

#include "ECS/component.hpp"
#include "ECS/luaEntityHandle.hpp"


class Entity {
    public:
        Entity(std::string name, Entity* parent = nullptr);
        virtual ~Entity();
        Transform& transform();
        Transform absoluteTransform();
        
        void addComponent(std::shared_ptr<Component> component);
        std::vector<std::shared_ptr<Component>>& getComponents();
        
        template<typename T> std::vector<std::shared_ptr<T>> getComponents(){
            std::vector<std::shared_ptr<T>> out;
            for(std::shared_ptr<Component> component : m_components){
                std::shared_ptr<T> c = std::dynamic_pointer_cast<T>(component);
                if(c != nullptr)
                    out.push_back(c);
            }
            return out;
        };
        
        template<typename T> std::shared_ptr<T> getComponent(){
            for(std::shared_ptr<Component> component : m_components){
                std::shared_ptr<T> c = std::dynamic_pointer_cast<T>(component);
                if(c != nullptr)
                 return c;
            }
            return nullptr;
        };
        
        void loadFromLua(LuaRef& table,AssetManager& m = AssetManager::instance());
        
        LuaEntityHandle& getLuaHandle();
        Entity* getParent();
        void setParent(Entity* e);
        
        void addChild(Entity* e);
        void removeChild(Entity* e);
        
        std::string getName() const;
        void setName(const std::string& name);
        
    protected:
        int uid; //Unique id for this entity
        std::string m_name;
        std::vector<std::shared_ptr<Component>> m_components;
        std::vector<Entity*> m_children;
        Transform m_transform;
        LuaEntityHandle m_luaHandle;
        Entity* m_parent = nullptr;
};

#endif
