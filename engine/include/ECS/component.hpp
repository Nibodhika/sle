#ifndef COMPONENT_HPP
#define COMPONENT_HPP

#include "lua.hpp"
#include "managers/assetManager.hpp"

/* 
 * Component: the raw data for one aspect of the object, and how it interacts with the world. "Labels the Entity as possessing this particular aspect". Implementations typically use Structs, Classes, or Associative Arrays.
*/

class Entity;

class Component {
public:
    Component();
    Component(Entity* entity);
    virtual ~Component();
    
    virtual bool load(LuaRef& table,AssetManager& m = AssetManager::instance()) = 0;  //Loads the component from a table
    void setEntity(Entity* entity);
    Entity* getEntity();
    
protected:
    Entity* m_entity;

};




//Passes entity->luaHandle as the first parameter
#define LUA_COMPONENT_FUNC(ret_type, function_name, ...)  \
                    PRIVATE_LUA_FUNC_PRE(ret_type,function_name, __VA_ARGS__) \
                        return (*lua_##function_name)(&m_entity->getLuaHandle() SHOULD_PUT_COMMA(__VA_ARGS__) DROP_TYPE(__VA_ARGS__));\
                    PRIVATE_LUA_FUNC_POS
                    
// Declares an overwritable function with void type without native implementation
#define LUA_COMPONENT_FUNC_VOID(function_name, ...)  \
                PRIVATE_LUA_FUNC_PRE(void,function_name, __VA_ARGS__) \
                            (*lua_##function_name)(&m_entity->getLuaHandle() SHOULD_PUT_COMMA(__VA_ARGS__) DROP_TYPE(__VA_ARGS__));\
                PRIVATE_LUA_FUNC_POS   
                
                
// Declares an overwritable function with a return type and a native implementation
#define NATIVE_LUA_COMPONENT_FUNC(ret_type, function_name, ...)  \
                PRIVATE_NATIVE_LUA_FUNC_PRE(ret_type,function_name, __VA_ARGS__) \
                          return (*lua_##function_name)(&m_entity->getLuaHandle() SHOULD_PUT_COMMA(__VA_ARGS__) DROP_TYPE(__VA_ARGS__));\
                PRIVATE_NATIVE_LUA_FUNC_POS(ret_type,function_name, __VA_ARGS__) \
                  
// Declares an overwritable function with void type and a native implementation
#define NATIVE_LUA_COMPONENT_FUNC_VOID(function_name, ...)  \
                 PRIVATE_NATIVE_LUA_FUNC_PRE(void,function_name, __VA_ARGS__) \
                          (*lua_##function_name)(&m_entity->getLuaHandle() SHOULD_PUT_COMMA(__VA_ARGS__) DROP_TYPE(__VA_ARGS__));\
                PRIVATE_NATIVE_LUA_FUNC_POS(ret_type,function_name, __VA_ARGS__) \


#endif
