#ifndef SYSTEM_HPP
#define SYSTEM_HPP

/*
 * System: "Each System runs continuously (as though each System had its own private thread) and performs global actions on every Entity that possesses a Component of the same aspect as that System."
 */

#include <memory>
#include <vector>

class Component;

class System {
    public:
        System();
        virtual bool registerComponent(std::shared_ptr<Component> component); //This is virtual to allow for systems that only accept one type of component to optimize by using an std::vector of the desired type, thus evoiding the overhead of casting each time they need to be processed
        
         virtual void update(float dt) = 0;
//          virtual void processAll() = 0;
         virtual void deleteOne(std::shared_ptr<Component> component);

         
protected:
    std::vector<std::shared_ptr<Component>> m_components;
};

#endif
