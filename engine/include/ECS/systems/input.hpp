#ifndef INPUT_SYSTEM_HPP
#define INPUT_SYSTEM_HPP
// SDL++
#include <input/keyboard.hpp>
#include <input/controller.hpp>

#include "ECS/system.hpp"
#include "lua.hpp"

#include "ECS/components/actionComp.hpp"

typedef struct T_Action{
    T_Action(std::string n){
        name = n;
    };
    std::string name;
    std::vector<std::shared_ptr<ActionComp>> listeners;
    
    void on() {
        for(auto l : listeners){
            l->on();
        }
    };
    void off() {
        for(auto l : listeners){
            l->off();
        }
    };
    void during(float dt, float v) {
        for(auto l : listeners){
            l->during(dt, v);
        }
    };
    
}Action;

typedef struct T_Keyboard_Mapping{
    SDL_Scancode key;
    std::vector<Action*> actions;
    std::vector<Action*> negative_actions; //Actions that triger during with -1 instead of 1
    bool operator==(const T_Keyboard_Mapping& other){
        return key == other.key;
    };
}Keyboard_Mapping;

typedef struct T_Button_Mapping{
    int controller = -1; // -1 means all controllers 
    SDL_GameControllerButton btn;
    std::vector<Action*> actions;
    std::vector<Action*> negative_actions;
    
    bool operator==(const T_Button_Mapping& other){
        return btn == other.btn && controller == other.controller;
    };
    
}Button_Mapping;

typedef struct T_Axis_Mapping{
    int controller = -1; // -1 means all controllers 
    SDL_GameControllerAxis axis;
    std::vector<Action*> actions; //No need for negative_actions here since they already implement -1 -> 1 range
    bool prevFrameWas0 = true;
     bool operator==(const T_Axis_Mapping& other){
        return axis == other.axis && controller == other.controller;
    };
}Axis_Mapping;

typedef struct T_InputMode {
    std::map<SDL_Scancode,Keyboard_Mapping> keyboard;
    std::map< int, std::map<SDL_GameControllerButton,Button_Mapping>> button;
    std::map< int, std::map<SDL_GameControllerAxis, Axis_Mapping>> axis;
}InputMode;

class Input : public System {
public:
    Input();
    ~Input();
    
    void update(float dt) override;
    
    void loadFromLua(LuaRef table);
    
    bool registerComponent(std::shared_ptr<Component> component) override;
    
    bool exitRequested();
    
    std::vector<SDL_Event> unhandledEvents();
    
    std::string getCurrentMode();
    bool setCurrentMode(std::string mode);
    
    void setControllerDeadZone(Uint16 deadZone);
    
protected:
    
    SDL_JoystickID getJoystickId(int controller_number);
    int getControllerIndex(SDL_JoystickID id);
    std::shared_ptr<Controller> getControllerByNumber(int index);
    std::shared_ptr<Controller> getControllerById(SDL_JoystickID id);
    
    void loadModeFromLua(LuaRef table);
    void addKeyboard(LuaRef table, Action* a, bool negative = false);
    void addKeyboardAxis(LuaRef table, Action* a);
    SDL_Scancode keyCodeFromLua(LuaRef keys);
    
    void addButton(LuaRef btn,std::vector<int> controllers, Action* a, bool negative = false);
    void addButtonAxis(LuaRef btn,std::vector<int> controllers, Action* a);
    
    void addAxis(LuaRef axis, std::vector<int> controllers, Action* a);
    std::vector<int> buildControllersVector(LuaRef controller);
    
    void onControllerAdded(int controller_number);
    void onControllerRemoved(int controller_id);
    
        std::map<std::string,Action*> m_actions;
    std::map<std::string,InputMode> m_modes;
    InputMode* m_currentMode = nullptr;
    
    std::vector<std::shared_ptr<Controller>> m_controllers;
    std::vector<SDL_Event> m_unhandledEvents; //Returns the events that were not handled in the previous frame
    bool m_exitRequested = false;
    
    Uint16 m_deadZone = 5000;
};

#endif
