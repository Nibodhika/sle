#ifndef DRAWING_HPP
#define DRAWING_HPP

#include "ECS/system.hpp"
#include <graphics/window.hpp>

#include "lua.hpp"

#include <map>

#include "ECS/components/graphicsComp.hpp"

typedef struct T_Layer{
    std::string name;
    T_Layer(std::string n) {
        name = n;
    }
    std::map<Entity*, std::vector<std::shared_ptr<GraphicsComp>>> elements;
}Layer;


//The table to load from lua is like this
/*
 Drawing = {
    Layers = {
        Background = 1,
        Default = 2
    },
    default_layer="Default"
 }
 */

// If Default layer is informed and exists it uses that position
// If Default layer is informed but does not exists it creates it at the top
// If Default layer is not informed, the topmost layer is used, if no layer exists a layer called Default is created

class Drawing : public System {
public:
    Drawing(Window* window = nullptr);
    ~Drawing();
    
//     std::vector<std::string> getLayers();
    int addLayer(std::string layer, int pos = -1); // returns the position added
//     bool removeLayer(std::string layer); //returns weather the layer was removed
//     int layerPos(std::string layer);
    
    Layer* getLayerByName(std::string name); // nullptr means not found
    int getLayerNumberByName(std::string name); // -1 means not found
    
    virtual void update(float dt);
    virtual bool registerComponent(std::shared_ptr<Component> component);
    virtual void deleteOne(std::shared_ptr<Component> component);
    
    void loadFromLua(LuaRef table);
    
    void setWindow(Window* window);
    
    std::string defaultLayerName();
    int defaultLayerPos();
    
    Layer* getDefaultLayer();
    
protected:
    std::string default_layer = "Default";
    int default_layer_pos = 10;
    
private:
//     std::map<std::string, std::vector<std::shared_ptr<Component>>> _layers;
    std::map<unsigned int,Layer> m_layers;
    
    Window* _window;
};

#endif
