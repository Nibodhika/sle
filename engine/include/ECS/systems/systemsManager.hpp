#ifndef SYSTEMS_MANAGER_HPP
#define SYSTEMS_MANAGER_HPP

#include <vector>

// Systems
#include "ECS/systems/drawing.hpp"
#include "ECS/systems/scripts.hpp"
#include "ECS/systems/input.hpp"

enum Systems {
    INPUT=1,
    PHYSICS=2,
    SCRIPTS=4,
    DRAWING=8,
    //SYSTEM_THAT_DEPENDS_ON_PHYSICS=NUMBER+PHYSICS
    ALL=INPUT+PHYSICS+SCRIPTS+DRAWING
};

class SystemsManager{
public:
    static SystemsManager& get(){
        static SystemsManager gsingleton;
        return gsingleton;
    }
    
    template<class T>
    std::shared_ptr<T> getSystem(){
        for(auto system : m_systems){
            std::shared_ptr<T> c = std::dynamic_pointer_cast<T>(system);
            if(c != nullptr)
                return c;
        }
        return nullptr;
    };
    
    void update(float dt);
    
    void init(Systems system = Systems::ALL);
    void shutdown();
    
    void loadFromLua(LuaRef table);
    //! Should print debug info on systems
    static bool DEBUG;
    
protected:
    SystemsManager();
    ~SystemsManager();
    
    std::vector< std::shared_ptr<System>> m_systems;
    
    
};

#endif
