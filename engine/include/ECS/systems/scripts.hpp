#ifndef SCRIPTS_SYSTEM_HPP
#define SCRIPTS_SYSTEM_HPP

#include "ECS/system.hpp"
#include "ECS/components/scriptComp.hpp"

class Scripts : public System {
public:
    Scripts();
    ~Scripts();
    void update(float dt) override;
};

#endif
