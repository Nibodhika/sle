#ifndef LUA_ENTITY_HANDLE_HPP
#define LUA_ENTITY_HANDLE_HPP

#include <base/pointF.hpp>
#include <base/transform.hpp>
#include "lua.hpp"

class Entity;
class LuaEntityHandle{
public:
    LuaEntityHandle(Entity* e);
    
    static void registerInLua();
    
    std::string name() const;
    
    PointF getPosition() const;
    void setPosition(PointF p);
    
protected:
    Entity* m_entity;
    Transform* transform;
    LuaRef m_data;
};

#endif
