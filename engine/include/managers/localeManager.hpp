#ifndef LOCALE_MANAGER_HPP
#define LOCALE_MANAGER_HPP

#include <map>

#include "lua.hpp"
#include "ECS/components/textComp.hpp"

//! Class that manages localized strings
/*!
  Locale manager is a class designed to manage localized strings in order to allow a game to render different texts depending on which table was loaded.
 */
class LocaleManager {
    
public:
    //! Destructor
    ~LocaleManager();
    //! Singleton
    static LocaleManager& instance();
    //! Returns the string for a given key
    /*!
        \param key Key of the string
        \returns String attributed to a given key, key value if string was not found
     */
    std::string getString(std::string key);
    //! Checks if exists and if not adds the string
    /*!
        \param key Key of the string to be used as language independent reference
        \param value Value of the string to be returned when asked for a key
        \returns Wether the string was added
        \sa replaceString
     */
    bool addString(std::string key, std::string value);
    //! Adds a string, replaces if exists
    /*!
       \param key Key of the string to be used as language independent reference
        \param value Value of the string to be returned when asked for a key
        \sa addString
     */
    void replaceString(std::string key, std::string value);
     //! Calls replaceString for every key value
    /*!
      Lua reference should be in the format
      ```lua
      Table = {
            key = "Value",
            key = "Value"
        }
      ```
       \param table Lua Table to be loaded
        \sa addString
     */
    void loadStringsFromLua(LuaRef table);
    
    //! Registers a TextComp to easily update them when changing locale
    /*!
      \param text Text Component to be registered
      \sa localeChanged
     */
    void registerText(TextComp* text);
    
    //! Deregisters the Text component
    /*!
      \param text Text Component to be deregistered
     */
    void deregisterText(TextComp* text);
    
    //! updates the text on all registered text components
    /*!
      Call this after loading several strings to change the text and update all of the registered texts
     */
    void localeChanged();
    
protected:
    //! Protected singleton constructor
    LocaleManager();
    //! Explicitly forbiding copy constructor   
    LocaleManager(const LocaleManager& that) = delete;
    //! A map of key values for Strings
    std::map<std::string,std::string> m_strings;
    std::vector<TextComp*> m_texts;
};

#endif
