#ifndef ASSET_MANAGER_HPP
#define ASSET_MANAGER_HPP

#include <map>
#include <string>

// SDL++
#include <graphics/texture.hpp>
#include <graphics/image.hpp>
#include <audio/chunk.hpp>
#include <graphics/font.hpp>


#include "lua.hpp"

class TiledMap;

class Asset {
public:
    enum Type {TEXTURE, IMAGE, AUDIO, FONT, SCRIPT, MAP};   
    std::string file; //filepath
    Type type; //Type of the void* below
    Asset* parent = nullptr;
    void* asset = nullptr; //Pointer to the actual loaded value, such as a texture or chunk
    Asset(){}
    Asset(std::string f, Type t, Asset* p = nullptr) {
        file = f;
        type = t;
        parent = p;
    }
    bool isLoaded = false;
    
};

//! Class that manages loading and unloading assets
/*!
   This class manages the loading and unloading of assets.
   There's a singleton to help ease simple projects but probably you want to have an AssetManager for each set of assets to be loaded.
   
   For example you can have an AssetManager that gets loaded instantly when the game opens to display the loading screen, and another one to load the game stuff while displaying the loading screen, that way it's easy to 
   
 */
class AssetManager { //: public Threadable {
public:
    AssetManager();
    ~AssetManager();
    
    void load();
    float loadNext(); //Loads the first asset not yet loaded, can be called inside the main loop to load things without stopping the main thread
//     void loadInThread(); //Not possible because of the underlying way SDL loads textures
    float percent();
    bool isDone();
    
    // Adds an asset to the load list
    bool addAsset(std::string id, Asset asset); 
    bool addAsset(std::string id, std::string file, Asset::Type type);
    
    //TODO fazer unload e delete, e corregir essas funcoes
    void deleteAsset(std::string id);
    
    
    Asset* getAsset(std::string id);
    Texture* getTexture(std::string id);
    Image* getImage(std::string id);
    Chunk* getChunk(std::string id);
    Font* getFont(std::string id);
    LuaRef* getScript(std::string id);
    TiledMap* getMap(std::string id);
    
    //! Loads all the assets listed in the lua table
    /*!
    Assets can be loaded from lua uising the following syntax
    ```lua
    TEXTURE_1 = {                                                                                                 
        file = "../../../resources/SDL_logo.png",                                                                  
        type = "TEXTURE"                                                                                           
    },                                                                                                            
    MUSIC_1 = {                                                                                                   
        file = "../../../resources/music.ogg",                                                                     
        type = "AUDIO"                                                                                             
    }
   ```
 */
    void addAssetFromLua(LuaRef table);
    void deleteFromLua(LuaRef table); //Deletes all assets listed in the table
    
    static AssetManager& instance();
    
protected:
//     void run();
    bool loadOne(std::pair<const std::string, Asset>& kv);
    
    void unloadAsset(Asset& asset);
    void deleteChildAssets(Asset* parent);
    
private:
    float m_percent = 0;
    std::map<std::string, Asset> m_assets; 
    //! Explicitly forbiding copy constructor   
    AssetManager(const AssetManager& that) = delete;
    
};

#endif
