#include "managers/assetManager.hpp"
#include <iostream>
#include <thread/thread.hpp>

#include "graphics/map/tiledMap.hpp"
#include "graphics/map/tileset.hpp"

AssetManager& AssetManager::instance() {
    static AssetManager m_instance;
    return m_instance;
}


AssetManager::AssetManager(){
    
}

AssetManager::~AssetManager() {
    //If the assetManager is destroyed it will destroy everything it loaded
    for(auto& kv : m_assets){
        Asset& as = kv.second;
        unloadAsset(as);
    }
}


float AssetManager::loadNext() {
    float each_adds = 1 / (float) m_assets.size();
    m_percent = 0;
    
      for(auto& kv : m_assets){
           m_percent += each_adds;
            if( loadOne(kv))
                return m_percent;
    }
    return 1;
}


void AssetManager::load() {

//TODO find a way to load childs
        for(auto& kv : m_assets){
             loadOne(kv);
        }

    m_percent = 1;
}

bool AssetManager::loadOne(std::pair<const std::string,Asset>& kv){
       Asset& as = kv.second;
        if(as.asset){ //Avoid reloading assets
            std::cout << "Skipping: " << kv.first << "(Already existed)"<< std::endl;
            return false;
        }
        bool loaded = true;
//         std::cout << "Loading: " << kv.first << " from " << as.file << std::endl;
        switch(as.type){
            case Asset::Type::TEXTURE:
                as.asset = new Texture(as.file);
                break;
            case Asset::Type::IMAGE: 
                if(as.parent->isLoaded){ //Avoid loading images before their textures are in memory
                    Rect r;
                    r.fromString(as.file);
                    as.asset = new Image((Texture*)as.parent->asset, r);
                }
                else{
                    loaded = false;
                }
                break;
            case Asset::Type::AUDIO:
                as.asset = new Chunk(as.file);
                break;
            case Asset::Type::FONT:
                as.asset = new Font(as.file);
                break;
            case Asset::Type::SCRIPT:
                //TODO ver como fazer isso
                break;
            case Asset::Type::MAP:
            { // Required for the initialization of m
                TiledMap* m = new TiledMap(as.file, nullptr);
                m->open(as.file);
                as.asset = m;
            }
                break;
            default:
                std::cout << "ERROR: Unknown Asset type: " << as.type << " on asset " << kv.first << std::endl;
                return false;
                break;
        }
        as.isLoaded = loaded;
        
        
        //Load children
        for(auto& kv : m_assets){
            if(kv.second.parent == &as)
                loadOne(kv);
        }
        
        return loaded;
}

// void AssetManager::run() {
//     load();
// }


// void AssetManager::loadInThread() {
//    start();
// }

float AssetManager::percent() {
    return m_percent;
}
bool AssetManager::isDone() {
    return m_percent == 1;
}


bool AssetManager::addAsset(std::string id, Asset asset) {
    bool insert = m_assets.count(id) == 0;
    if(insert)
        m_assets[id] = asset;
    return insert;
}
bool AssetManager::addAsset(std::string id, std::string file, Asset::Type type) {
    return addAsset(id, Asset(file,type));
}

void AssetManager::deleteChildAssets(Asset* parent) {
    //TODO find a better way
    std::vector<Asset*> delete_assets;
    for(auto& kv : m_assets){
        if(kv.second.parent == parent)
//             delete_positions.push_back(kv.first);
            delete_assets.push_back(&kv.second);
    }
    
    for(auto i : delete_assets){
        unloadAsset(*i);
    }
}


void AssetManager::deleteAsset(std::string id) {
    auto it=m_assets.find(id);
    if(it != m_assets.end() ){
        unloadAsset(it->second); 
        m_assets.erase(it);  
    }
    else{
        std::cerr << "Asset not found: " << id << std::endl;
    }
}

void AssetManager::unloadAsset(Asset& as) {
    if(as.asset) {
         deleteChildAssets(&as);
        switch(as.type){
            case Asset::Type::TEXTURE:
                delete( (Texture*) as.asset);
                //Also delete all images associated
                break;
            case Asset::Type::IMAGE:
                delete( (Image*) as.asset);
                break;
            case Asset::Type::AUDIO:
                delete( (Chunk*) as.asset);
                break;
            case Asset::Type::FONT:
                delete( (Font*) as.asset);
                break;
            case Asset::Type::SCRIPT:
                delete( (LuaRef*) as.asset);
                break;
            default:
                std::cout << "Unknown Asset type: " << as.type << std::endl;
                break;
        }
        as.asset = nullptr;
        as.isLoaded = false;
    }
}


Asset* AssetManager::getAsset(std::string id) {
    std::map<std::string,Asset>::iterator it = m_assets.find(id);
    if (it != m_assets.end())
        return &it->second;
    return nullptr;
}


Texture * AssetManager::getTexture(std::string id) {
    Asset* as = getAsset(id);
    if(as && as->type == Asset::Type::TEXTURE) {
        return (Texture*) as->asset;
    }
    return nullptr;
}

Image * AssetManager::getImage(std::string id) {
    Asset* as = getAsset(id);
    if(as && as->type == Asset::Type::IMAGE) {
        return (Image*) as->asset;
    }
    return nullptr;
}

Chunk * AssetManager::getChunk(std::string id) {
    Asset* as = getAsset(id);
    if(as && as->type == Asset::Type::AUDIO) {
        return (Chunk*) as->asset;
    }
    return nullptr;
}

Font * AssetManager::getFont(std::string id) {
    Asset* as = getAsset(id);
    if(as && as->type == Asset::Type::FONT) {
        return (Font*) as->asset;
    }
    return nullptr;
}

luabridge::LuaRef * AssetManager::getScript(std::string id) {
    Asset* as = getAsset(id);
    if(as && as->type == Asset::Type::SCRIPT) {
        return (LuaRef*) as->asset;
    }
    return nullptr;
}
TiledMap* AssetManager::getMap(std::string id){
    Asset* as = getAsset(id);
    if(as && as->type == Asset::Type::MAP) {
        return (TiledMap*) as->asset;
    }
    return nullptr;
}

void AssetManager::addAssetFromLua(luabridge::LuaRef table) {
    FOREACH_KEY_VAL(table)
        std::string file;
        LUA_LOAD_STRING(val,file)
        std::string type;
        LUA_LOAD_STRING(val,type)
        Asset::Type t;
        if(type == "TEXTURE")
            t = Asset::Type::TEXTURE;
        else if(type == "AUDIO")
            t = Asset::Type::AUDIO;
        else if(type == "FONT")
            t = Asset::Type::FONT;
        else if(type == "SCRIPT")
            t = Asset::Type::SCRIPT;
        else if(type == "MAP")
            t = Asset::Type::MAP;
        else {
            std::cout << "Unknown type: " << type << " on asset " << key << std::endl;
            continue;
        }
        addAsset(key, file,t);
        
        if(t == Asset::Type::TEXTURE){ //Also add the images
            LuaRef images = val["images"];
            if(images.isTable()){
                //Cannot use macro inside macro because of variable names
                for (Iterator iter2 (images); !iter2.isNil (); ++iter2) { 
                    LuaRef img_name = iter2.key (); 
                        if(img_name.isString() ) { 
                            std::string img_str = img_name;
                            std::string texture_str = key;
                            LuaRef img_rect = *iter2; 
                            Rect rect;
                            LUA_DO_LOAD_RECT(img_rect,rect,"in Image "+img_str+" in Texture "+texture_str);
//                             std::cout << "Read a rect as : " << rect.toString() << " in " << img_name << std::endl;
                            addAsset(img_str, Asset(rect.toString() , Asset::Type::IMAGE, &m_assets[texture_str]) );
                        }
                }
            }
        }
        
    END_FOREACH
}

void AssetManager::deleteFromLua(luabridge::LuaRef table) {
     FOREACH_KEY_VAL(table)
        deleteAsset(key);
    END_FOREACH
}
