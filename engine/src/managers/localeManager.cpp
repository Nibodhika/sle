#include "managers/localeManager.hpp"

LocaleManager::LocaleManager() {
    
}

LocaleManager::~LocaleManager() {
}

LocaleManager& LocaleManager::instance() {
    static LocaleManager m_instance;
    return m_instance;
}
std::string LocaleManager::getString(std::string key) {
     auto k = m_strings.find(key);
     if(k == m_strings.end())
         return key;
     return k->second;
}

bool LocaleManager::addString(std::string key, std::string value) {
    auto k = m_strings.find(key);
     if(k == m_strings.end()){
        replaceString(key,value);
     }
     return false;
}

void LocaleManager::replaceString(std::string key, std::string value) {
//     std::cout << "replacing" << key << " " << value << std::endl;
     m_strings[key] =value;
//      std::cout << "now is: " << getString(key) << std::endl;
}


void LocaleManager::loadStringsFromLua(luabridge::LuaRef table) {
    
    FOREACH_KEY_VAL(table)
        if(val.isString())
            replaceString(key,val);
        else
            std::cout << "Value is not a string in " << key << std::endl;
    END_FOREACH
    
}

void LocaleManager::registerText(TextComp* text) {
    m_texts.push_back(text);
}

void LocaleManager::deregisterText(TextComp* text) {
    for(int i =0; i < m_texts.size(); i++){
        if(m_texts[i] == text){
            m_texts.erase(m_texts.begin() + i);
            return;
        }
    }
}


void LocaleManager::localeChanged() {
    for(auto t : m_texts)
        t->regenText();
}

