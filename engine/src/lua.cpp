#include "lua.hpp"

#include "ECS/luaEntityHandle.hpp"

lua_State* LUA::STATE =nullptr;

void LUA::init(bool registerECS){
    if(! STATE) {
        STATE =  luaL_newstate();
        luaL_openlibs(STATE);
        
        if(registerECS)
            LuaEntityHandle::registerInLua();
    }
}

bool LUA::loadFile(const std::string& scriptFilename) {
    if (luaL_dofile(STATE, scriptFilename.c_str()) == 0) { // script has opened
           return true;
    }
    
    std::cout << "Error, can't open script " << scriptFilename << lua_tostring(STATE, -1)  << std::endl;
    return false;
}


LuaRef LUA::getTable(const std::string& tableName) {
    LuaRef table = getGlobal(STATE, tableName.c_str());
    if (table.isTable()) {
        return table;
    }
    
    return LuaRef(STATE); // References nil
}  


std::vector<std::string> LUA::getTableKeys(const LuaRef& table, bool recursive, std::string base) {
    std::vector<std::string> out;
    
    for (Iterator iter (table); !iter.isNil (); ++iter) {
      LuaRef key = iter.key ();
       if(key.isString()) { //Only do this for keys that are strings, ignore everything else
        LuaRef val = *iter;
        std::string key_string = key;
        //Recursivelly analize tables
        if(val.isTable() && recursive){
            std::vector<std::string> sub_keys = LUA::getTableKeys(val, recursive, key_string + ".");
            for(auto k : sub_keys)
                out.push_back(base + k);
        }
        
        out.push_back(base + key_string);
       }
    }
  
    return out;
}
