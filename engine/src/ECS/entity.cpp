#include "ECS/entity.hpp"
#include "ECS/components/componentFactory.hpp"
#include "ECS/component.hpp"

Entity::Entity(std::string name, Entity* parent) : m_luaHandle(this) {
    m_name = name;
    setParent(parent);
}

Entity::~Entity() {
}


Transform & Entity::transform() {
    return m_transform;
}

Transform Entity::absoluteTransform() {
    Transform out;
    if(m_parent){
        out *= m_parent->absoluteTransform();
    }
    return out*m_transform;
}


void Entity::addComponent(std::shared_ptr<Component> component) {
    m_components.push_back(component);
    component->setEntity(this);
}


std::vector<std::shared_ptr<Component>>& Entity::getComponents() {
    return m_components;
}

LuaEntityHandle& Entity::getLuaHandle() {
    return m_luaHandle;
}
Entity * Entity::getParent() {
    return m_parent;
}
void Entity::setParent(Entity* e) {
    if(m_parent)
        m_parent->removeChild(this);
    m_parent = e;
    if(m_parent)
        m_parent->addChild(this);
}

void Entity::addChild(Entity* e) {
    m_children.push_back(e);
}
void Entity::removeChild(Entity* e) {
    for(int i = 0; i < m_children.size(); i++){
        if(m_children[i]== e){
            m_children.erase(m_children.begin() + i);
            return;
        }
    }
}


std::string Entity::getName() const {
    return m_name;
}

void Entity::setName(const std::string& name) {
    m_name = name;
}


void Entity::loadFromLua(LuaRef& table, AssetManager& m) {
//     std::string name;
//     LUA_LOAD_STRING(table,name);
//     if(name != LUA_NULL_STRING)
//         m_name = name;
    
    FOREACH_KEY_VAL(table)
    
        if(key == "pos"){
            Point pos;
            LUA_LOAD_POINT(table,pos,"in Entity "+getName())
            m_transform.setPosition(pos);
        }
        else {
            std::vector<std::shared_ptr<Component>> comps = ComponentFactory::load(key, val, this,m);
            for(auto comp : comps)
                addComponent(comp);
        }

    END_FOREACH
}
