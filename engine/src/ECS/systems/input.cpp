#include "ECS/systems/input.hpp"

#include "ECS/systems/systemsManager.hpp"

Input::Input(){
    
}

Input::~Input() {
}

SDL_Scancode Input::keyCodeFromLua(luabridge::LuaRef keys) {
    SDL_Scancode out = SDL_SCANCODE_UNKNOWN;
        if(keys.isString()){
            out = Keyboard::codeFromName(keys);
        }
        else if(keys.isNumber()){
            out = (SDL_Scancode) keys;
        }
        else{
            std::cout << "WARNING: Binding for keyboard is not string, number or table"<< std::endl;
        }
        return out;
}
void Input::addKeyboard(luabridge::LuaRef keys, Action* a, bool negative) {
    if(keys.isTable()){
        for (Iterator iter (keys); !iter.isNil (); ++iter) { 
            addKeyboard(*iter,a);
        }
    }
    else {
        SDL_Scancode code = keyCodeFromLua(keys);
        //If not valid return
        if(code == SDL_SCANCODE_UNKNOWN){
            std::cout << "Could not map " << keys << " for action " << a->name << std::endl;
            return;
        }
        //if the mapping already exists add the action to it
        if(negative)
            m_currentMode->keyboard[code].negative_actions.push_back(a);
        else
            m_currentMode->keyboard[code].actions.push_back(a);
        if(SystemsManager::DEBUG)
            std::cout << "Created key binding " << keys << " for action " << a->name << std::endl;
    }
}
void Input::addKeyboardAxis(luabridge::LuaRef table, Action* a) {
    if(table.isTable()) {
        int count = 0;
         for (Iterator iter (table); !iter.isNil (); ++iter) { 
             addKeyboard(*iter,a,
                 count % 2 == 0 //It's negative if this is pair
            );
             count++;
        }
        if(count % 2 != 0){
             std::cerr << "WARNING: Axis emulation over keyboard at " << a->name << " Used an odd axis amount, you have one negative axis that has no positive value" << std::endl;     
        }
    }
    else
        std::cerr << "WARNING: Axis emulation over keyboard at " << a->name << " Requires format key_axis = { \"NEGATIVE_KEY\", \"POSITIVE_KEY\" , ... }" << std::endl;
}

void Input::addButton(luabridge::LuaRef btn, std::vector<int> controllers, Action* a, bool negative) {
    if(btn.isTable() ){
        int count = 0;
        //If no controller was informed use all of them
        if(controllers.size() == 0)
            controllers.push_back(-1);
        
        for (Iterator iter (btn); !iter.isNil (); ++iter) { 
            //Since btn is a table, we expect controller to also be a table, i.e. having multiple numbers
            //If it has, each number corresponds to each button, i.e. button A on controller 0, button B on controller 1, etc.
            //If controllers was only one value (or less than the number of buttons, repeat the last one
            // So btn = { "A", "B"}, controller = 0 becomes -> Button A Controller 0, Button B controller 0
            
            //making sure we don't exeed the vector, also repeat last value if it happens
            int pos = std::min(count,(int) controllers.size() -1); 
            std::vector<int> controllers2;
            controllers2.push_back(controllers[pos]);
            addButton(*iter,controllers2, a);
            count++;
        }
    }
    else { 
        //Only one button, using first controller number
        int controller_number = -1; //If no controller informed use all
        if(controllers.size() > 0)
            controller_number = controllers[0];
        
        SDL_GameControllerButton code = SDL_CONTROLLER_BUTTON_INVALID;
        if(btn.isString()){
            code = Controller::buttonFromName(btn);
        }
        else if(btn.isNumber()){
            code = (SDL_GameControllerButton) btn;
        }
        else{
            std::cout << "Binding " << a->name << " for controller button is not string, number or table"<< std::endl;
        }
        //If not valid return
        if(code == SDL_CONTROLLER_BUTTON_INVALID){
            std::cout << "Could not map " << btn << " for action " << a->name << std::endl;
            return;
        }
        if(negative)
            m_currentMode->button[controller_number][code].negative_actions.push_back(a);
        else
            m_currentMode->button[controller_number][code].actions.push_back(a);
        if(SystemsManager::DEBUG) {
            std::string controller_string = "on";
            if(controller_number == -1)
                controller_string += " all controllers";
            else
                controller_string += " controller " + std::to_string(controller_number);
            std::cout << "Created btn binding " << btn << " " << controller_string << " for action " << a->name << std::endl;
        }
        
    }
}

void Input::addButtonAxis(luabridge::LuaRef btn, std::vector<int> controllers, Action* a) {
    if(btn.isTable() ){
        int count = 0;
        //If no controller was informed use all of them
        if(controllers.size() == 0)
            controllers.push_back(-1);
        //We expect two buttons here, so unlike the addButton we have to pair stuff
        for (Iterator iter (btn); !iter.isNil (); ++iter) {
            //Pairing values, so only advancing controller after 2 buttons (that's why count/2)
            int pos = std::min(count / 2,(int) controllers.size() -1);
            std::vector<int> controllers2;
            controllers2.push_back(controllers[pos]);
            addButton(*iter,controllers2, a,
                count % 2 == 0 // This is a negative if count is pair
            );
            count++;
        }
         if(count % 2 != 0){
             std::cerr << "WARNING: Axis emulation over Button at " << a->name << " Used an odd axis amount, you have one negative axis that has no positive value" << std::endl;     
        }
        
    }
    else
        std::cerr << "WARNING: Axis emulation over Button at " << a->name << " Requires format btn_axis = { \"NEGATIVE_KEY\", \"POSITIVE_KEY\"}" << std::endl;     
}


void Input::addAxis(luabridge::LuaRef lua_axis, std::vector<int> controllers, Action* a) {
    if(lua_axis.isTable() ){
        int count = 0;
        //If no controller was informed use all of them
        if(controllers.size() == 0)
            controllers.push_back(-1);
        
        for (Iterator iter (lua_axis); !iter.isNil (); ++iter) { 
            int pos = std::min(count,(int) controllers.size() -1); //making sure we don't exeed the vector, also repeat last value if it happens
            std::vector<int> controllers2;
            controllers2.push_back(controllers[pos]);
            addAxis(*iter,controllers2, a);
            count++;
        }
    }
    else { 
        //Only one button, using first controller number
        int controller_number = -1; //If no controller informed use all
        if(controllers.size() > 0)
            controller_number = controllers[0];
        
        SDL_GameControllerAxis code = SDL_CONTROLLER_AXIS_INVALID;
        if(lua_axis.isString()){
            code = Controller::axisFromName(lua_axis);
        }
        else if(lua_axis.isNumber()){
            code = (SDL_GameControllerAxis) lua_axis;
        }
        else{
            std::cout << "Binding " << a->name << " for controller button is not string, number or table"<< std::endl;
        }
        //If not valid return
        if(code == SDL_CONTROLLER_AXIS_INVALID){
            std::cout << "Could not map " << lua_axis << " for action " << a->name << std::endl;
            return;
        }
        
        m_currentMode->axis[controller_number][code].actions.push_back(a);
        if(SystemsManager::DEBUG) {
             std::string controller_string = "on";
            if(controller_number == -1)
                controller_string += " all controllers";
            else
                controller_string += " controller " + std::to_string(controller_number);
            std::cout << "Created axis binding " << lua_axis << " " << controller_string << " for action " << a->name << std::endl;
        }
    }
}

std::vector<int> Input::buildControllersVector(luabridge::LuaRef controller) {
    std::vector<int> out;
    if(controller.isNumber()){
        out.push_back(controller);
    }
    if(controller.isTable()){
        for (Iterator iter (controller); !iter.isNil (); ++iter) { 
            LuaRef n = *iter;
            if(n.isNumber()){
                out.push_back(n);
            }
        }
    }
    return out;
}

bool Input::registerComponent(std::shared_ptr<Component> component) {
    std::shared_ptr<ActionComp> action = std::dynamic_pointer_cast<ActionComp>(component);
    
    auto it = m_actions.find(action->actionName());
    if(it != m_actions.end()){
        it->second->listeners.push_back(action);
    }
    else
        std::cerr << "Unknown action " << action->actionName() << std::endl;
    
    return System::registerComponent(component);
}

void Input::onControllerAdded(int controller_number) {
//     std::cout << "added controller_number " << controller_number  << std::endl;
//     
//     auto c = m_controllers.find(controller_number);
//      if(c !=  m_controllers.end()){
//         std::cout << "Which already existed with id " << c->second->id() << std::endl;
//          delete c->second;
//          m_controllers.erase(c);
//          std::cout << "Deleted it" << std::endl;
//      }
    
    //Perhaps check if controller exists
    std::shared_ptr<Controller> controller = std::make_shared<Controller>(controller_number,m_deadZone);
//     m_controllers[controller_number] = controller;
    std::cout << "created controller " << controller->id() << " For controlling " << controller->index() << std::endl;
    m_controllers.push_back(controller);
}

void Input::onControllerRemoved(int controller_id) {
    
    for(int i = 0; i < m_controllers.size(); i++){
        auto controller = m_controllers[i];
        if(controller->id() == controller_id){
            std::cout << "Controller " << controller_id << " is controlling " << controller->index() << std::endl;
            m_controllers.erase(m_controllers.begin() + i);
            std::cout << "Deleted it" << std::endl;
            return;
        }
    }
}


void Input::loadFromLua(luabridge::LuaRef table) {
    FOREACH_KEY_VAL(table)
        std::string mode_name = key;
        if(mode_name == "deadZone"){
            if(val.isNumber()){
                setControllerDeadZone(val);
            }
            else{
                std::cerr << "WARNING: deadZone " << val << " is not a number" << std::endl;
            }
        }
        else {
            m_modes[mode_name] = InputMode();
            setCurrentMode(mode_name);
            loadModeFromLua(val);
        }
    END_FOREACH
};
    
void Input::loadModeFromLua(luabridge::LuaRef table){
    LuaRef actions = table["Actions"];
    if(actions.isTable()){
        
        FOREACH_KEY_VAL(actions)
            std::string name = key;
            if(val.isTable() ) {
                Action* a = new Action(name);
                //Keyboard
                LuaRef keyboard = val["keyboard"];
                if(keyboard.isTable()){
                    LuaRef key = keyboard["key"];
                    if(!key.isNil())
                        addKeyboard(key,a);
                    LuaRef key_axis = keyboard["key_axis"];
                    if(! key_axis.isNil() )
                        addKeyboardAxis(key_axis,a);
                }
                //Controllers
                LuaRef controller = val["controller"];
                if(controller.isTable()){
                     LuaRef lua_controllers = controller["controller"];
                     std::vector<int> controllers = buildControllersVector(lua_controllers);
                     
                     LuaRef btn = controller["btn"];
                     if( ! btn.isNil() )
                         addButton(btn,controllers,a);
                    
                    LuaRef btn_axis = controller["btn_axis"];
                    if(! btn_axis.isNil() )
                        addButtonAxis(btn_axis,controllers,a);
                     
                     LuaRef axis = controller["axis"];
                     if( ! axis.isNil() ) 
                         addAxis(axis,controllers,a);
                    
                }
                m_actions[name] = a;
            }
        END_FOREACH
        
    }
}

int Input::getControllerIndex(SDL_JoystickID id) {
    return getControllerById(id)->index();
}

SDL_JoystickID Input::getJoystickId(int controller_number) {
    return getControllerByNumber(controller_number)->id();
}

std::shared_ptr<Controller> Input::getControllerById(SDL_JoystickID id) {
    for(auto controller : m_controllers)
        if(controller->id() == id)
            return controller;
    return nullptr;
}

std::shared_ptr<Controller> Input::getControllerByNumber(int index) {
     for(auto controller : m_controllers)
        if(controller->index() == index)
            return controller;
    return nullptr;
}

bool Input::exitRequested() {
    return m_exitRequested;
}

std::vector<SDL_Event> Input::unhandledEvents() {
    return m_unhandledEvents;
}


void Input::update(float dt) {
    m_unhandledEvents.clear();
    SDL_Event e;
    auto btn_all_controllers = m_currentMode->button[-1];
    auto axis_all_controllers = m_currentMode->axis[-1];
    while( SDL_PollEvent( &e ) != 0 ) {
        bool handled = false;
        //User requests quit
        if( e.type == SDL_QUIT ) {
            m_exitRequested = true;
        }
        
        //Keyboard
         else if( e.type == SDL_KEYDOWN ){
                auto it = m_currentMode->keyboard.find(e.key.keysym.scancode);
                if(it != m_currentMode->keyboard.end() ) {
                    handled = true;
                    for(Action* a : it->second.actions){
//                         //Call the on function on all of the action listeners
//                         std::cout << "Calling on action " << a->name << std::endl;
                        a->on();
                    }
                }
        }
        else if( e.type == SDL_KEYUP){
             auto it = m_currentMode->keyboard.find(e.key.keysym.scancode);
                if(it != m_currentMode->keyboard.end() ) {
                    handled = true;
                    for(Action* a : it->second.actions){
//                         //Call the off function on all of the action listeners
//                         std::cout << "Calling off action " << a->name << std::endl;
                        a->off();
                    }
                }
        }
        
        //Controllers Buttons
        else if(e.type == SDL_CONTROLLERBUTTONDOWN){
            
            
            
            auto btn = btn_all_controllers.find( (SDL_GameControllerButton) e.cbutton.button);
            if(btn != btn_all_controllers.end()){
                handled = true;
                for(Action* a : btn->second.actions){
                    //Call the on function of all the action listeners
//                     std::cout << "Calling on action " << a->name << std::endl;
                    a->on();
                }
            }
            
            auto ctn = m_currentMode->button.find( getControllerIndex(e.cbutton.which) );
            if(ctn != m_currentMode->button.end()){
                auto btn = ctn->second.find( (SDL_GameControllerButton) e.cbutton.button);
                if(btn != ctn->second.end()){
                    handled = true;
                    for(Action* a : btn->second.actions){
                        //Call the on function of all the action listeners
//                         std::cout << "Calling on action " << a->name << std::endl;
                        a->on();
                    }
                }
            }
        }
        else if(e.type == SDL_CONTROLLERBUTTONUP){
            auto btn = btn_all_controllers.find( (SDL_GameControllerButton) e.cbutton.button);
            if(btn != btn_all_controllers.end()){
                handled = true;
                for(Action* a : btn->second.actions){
                    //Call the off function of all the action listeners
//                     std::cout << "Calling off action " << a->name << std::endl;
                    a->off();
                }
            }
            
            auto controller = m_currentMode->button.find(getControllerIndex(e.cbutton.which));
            if(controller != m_currentMode->button.end()){
                auto btn = controller->second.find( (SDL_GameControllerButton) e.cbutton.button);
                if(btn != controller->second.end()){
                    handled = true;
                    for(Action* a : btn->second.actions){
                        //Call the off function of all the action listeners
//                         std::cout << "Calling off action " << a->name << std::endl;
                        a->off();
                    }
                }
            }
        }
        
        
        // I don't think this is needed here
//         //Controllers Axis
//         else if(e.type == SDL_CONTROLLERAXISMOTION){
//             auto axis = axis_all_controllers.find( (SDL_GameControllerAxis) e.caxis.axis);
//             if(axis != axis_all_controllers.end()){
//                 for(Action* a : axis->second.actions){
//                     //Call the function of all the action listeners
//                 }
//             }
//             
//             auto controller = m_axis.find(e.caxis.which);
//             if(controller != m_axis.end()){
//                 auto axis = controller->second.find( (SDL_GameControllerAxis) e.caxis.axis);
//                 if(axis != controller->second.end()){
//                     for(Action* a : axis->second.actions){
//                         //Call the function of all the action listeners
//                     }
//                 }
//             }
//         }
        
        
        else if(e.type == SDL_JOYDEVICEADDED){
            //When plugged is controller number
          onControllerAdded(e.jdevice.which);
          handled = true;
        }
        else if(e.type == SDL_JOYDEVICEREMOVED){
            //when removed it's controller_id
            onControllerRemoved(e.jdevice.which);
            handled = true;
        }
        
        else if(e.type == SDL_CONTROLLERDEVICEADDED){
            std::cout << "Added controller " << e.cdevice.which << std::endl;
        }
        else if(e.type == SDL_CONTROLLERDEVICEREMOVED){
            std::cout << "Removed controller " << e.cdevice.which << std::endl;
        }
        else if(e.type == SDL_CONTROLLERDEVICEREMAPPED){
            std::cout << "Remapped controller " << e.cdevice.which << std::endl;
        }
        
        
        if(!handled) { //Unhandled event
            m_unhandledEvents.push_back(e);
        }
        
    }
    
    
    for(auto& kv : m_currentMode->keyboard){
        if(Keyboard::isKeyDown(kv.first) ) {
            for(Action* a : kv.second.actions)
                a->during(dt,1);
            for(Action* a : kv.second.negative_actions)
                a->during(dt,-1);
        }
    }
    
    for(auto& kv : m_currentMode->button){
        
        int controller_number = kv.first;
        auto& map_btn = kv.second;
        if(controller_number == -1){ //All/Any controllers
        
            for(auto controller: m_controllers){
                for(auto& kv2: map_btn){
                    if ( controller->isButtonDown(kv2.first) ){
                        for(Action* a : kv2.second.actions)
                            a->during(dt,1);
                        for(Action* a : kv2.second.negative_actions)
                            a->during(dt,-1);
                    }
                }
            }
            
            
        }
        else { //Only one controller
            auto controller = getControllerByNumber(controller_number);
            if(controller){ //If controller is plugged in
                for(auto& kv2: map_btn){
                    if ( controller->isButtonDown(kv2.first) ){
                        for(Action* a : kv2.second.actions){
                            //Call the action while on all listeners
//                             std::cout << "Calling action " << a->name << std::endl;
                            a->during(dt,1);
                        }
                    }
                }
            }
        }
        
    }
    
    for(auto& kv : m_currentMode->axis){
        int controller_number = kv.first;
        auto& map_axis = kv.second;
        if(controller_number == -1){ //All/Any controllers
        
            for(auto& controller : m_controllers){
                for(auto& kv2: map_axis){
                     double axis = controller->getAxis(kv2.first);
                     
                     bool prevFrameWas0 = false;
                     if(axis == 0)
                        prevFrameWas0 = true;
                     
                      for(Action* a : kv2.second.actions){
                          //Call the action while on all listeners
                          if(axis != 0){
                              if(kv2.second.prevFrameWas0)
                                  a->on();
                            a->during(dt,axis);
                          }
                          else if(! kv2.second.prevFrameWas0)
                              a->off();
                      }
                      
                       kv2.second.prevFrameWas0 = prevFrameWas0;
                }
            }
            
            
        }
        else { //Only one controller
            auto controller = getControllerByNumber(controller_number);
            if(controller){ //If controller is plugged in
                for(auto& kv2: map_axis){
                    double axis = controller->getAxis(kv2.first);
                    for(Action* a : kv2.second.actions){
                        //Call the action while on all listeners
                        a->during(dt,axis);
                    }
                }
            }
        }
    }
    
}

std::string Input::getCurrentMode() {
    for(auto& m : m_modes){
        if(&(m.second) == m_currentMode)
            return m.first;
    }
    return "";
}
bool Input::setCurrentMode(std::string mode){
    auto it = m_modes.find(mode);
    if(it != m_modes.end()){
       m_currentMode = &(it->second);
       return true;
    }
    return false;
}
void Input::setControllerDeadZone(Uint16 deadZone) {
    m_deadZone = deadZone;
    for(auto c : m_controllers){
        c->setDeadZone(m_deadZone);
    }
}

