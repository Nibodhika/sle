#include "ECS/systems/scripts.hpp"
#include "ECS/entity.hpp"

Scripts::Scripts(){
    
}

Scripts::~Scripts() {
}


void Scripts::update(float dt) {
    for(auto comp : m_components){
        auto script = std::dynamic_pointer_cast<ScriptComp>(comp);
        if(script != nullptr)
            script->update(dt);
    }
}
