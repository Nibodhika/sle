#include "ECS/systems/drawing.hpp"
#include <algorithm>

#include "ECS/components/spriteComp.hpp"
#include "ECS/systems/systemsManager.hpp"

Drawing::Drawing(Window* window){
    if(window == nullptr)
        window = Window::main_window;
//     _window = window;
    setWindow(window);
    
    addLayer(default_layer, default_layer_pos);
}

Drawing::~Drawing() {
}

void Drawing::setWindow(Window* window) {
    _window = window;
}

std::string Drawing::defaultLayerName() {
    return default_layer;
}

int Drawing::defaultLayerPos() {
    return default_layer_pos;
}

int Drawing::addLayer(std::string layer, int pos) {
//     _layers.push_back(layer);
    if(pos == -1) {
        int max_pos = 0;
        for(auto& kv : m_layers)
            if(kv.first > max_pos)
                max_pos = kv.first;
        pos = max_pos + 1;
    }
    Layer l(layer);
//     l.name = layer;
    m_layers.emplace(pos, l);
//     _layers[layer] = std::vector<std::shared_ptr<Component>>();
    return pos;
}

// bool Drawing::removeLayer(std::string layer) {
//     int pos = layerPos(layer);
//     if(pos == -1)
//         return false;
// //     _layers.erase( _layers.begin() +  pos);
//     return true;
// }
// std::vector<std::string> Drawing::getLayers() {
//     return m_layers;
// }
// int Drawing::layerPos(std::string layer) {
//     auto it = std::find(_layers.begin(), _layers.end(), layer);
//     if ( it == _layers.end() ){
//         return -1;
//     }
//     return std::distance(_layers.begin(), it);
// }

Layer* Drawing::getLayerByName(std::string name) {
    for(auto& kv : m_layers)
        if(kv.second.name == name)
                return &kv.second;
    return nullptr;
}

int Drawing::getLayerNumberByName(std::string name) {
    for(auto& kv : m_layers)
        if(kv.second.name == name)
                return kv.first;
    return -1;
}

Layer * Drawing::getDefaultLayer() {
     for(auto& kv : m_layers)
        if(kv.first == default_layer_pos)
                return &kv.second;
    return nullptr;
}

bool Drawing::registerComponent(std::shared_ptr<Component> component) {
    //Get the layer from the component
    std::shared_ptr<GraphicsComp> graphics = std::dynamic_pointer_cast<GraphicsComp>(component);
    if(graphics == nullptr)
        return false;
    //Add the component to the map
    Layer* layer = getLayerByName(graphics->getLayer());
    if(layer){
//         std::cout << "Found layer " << graphics->getLayer() << " " << layer->name << std::endl;
        layer->elements[graphics->getEntity()].push_back(graphics);
        return true;
    }
    else {
        std::cerr << "WARNING: Could not find layer " << graphics->getLayer() << " for component " << graphics->getEntity()->getName() << " using default layer " << std::endl;
        layer = getDefaultLayer();
        if(layer){
            layer->elements[graphics->getEntity()].push_back(graphics);
            return true;
        }
        else 
            std::cerr << "Default layer not found" << std::endl;
    }
    return false;
}

void Drawing::deleteOne(std::shared_ptr<Component> component) {
    //Do actual deleting
    std::shared_ptr<GraphicsComp> graphics = std::dynamic_pointer_cast<GraphicsComp>(component);
    if(graphics == nullptr)
        return;
    //Add the component to the map
    Layer* layer = getLayerByName(graphics->getLayer());
    if(! layer) {
        std::cerr << "WARNING: Could not find layer " << graphics->getLayer() << " for component " << graphics->getEntity()->getName() << " searching at default layer " << std::endl;
        layer = getDefaultLayer();
        if(! layer){
            std::cerr << "Default layer not found" << std::endl;
            return;
        }
    }
    
    auto it = layer->elements.find(graphics->getEntity());
    if(it != layer->elements.end()){ //Found entity in layers
        std::vector<std::shared_ptr<GraphicsComp>>& entity_components = it->second;
        for(int i = 0; i < entity_components.size(); i++){
            if(entity_components[i] == graphics){
//                 std::cout << "Found the component" << std::endl;
                entity_components.erase(entity_components.begin() + i);
                break;
            }
        }
        if(entity_components.size() == 0){ //Removed last graphic components remove entity as well
//             std::cout << "Removing entity" << std::endl;
            layer->elements.erase(it);
        }
    }
    else { 
        std::cerr << "WARNING: Removing GraphicsComp on entity " << graphics->getEntity()->getName() << ", But I don't know that entity" << std::endl;
    }
    
}


void Drawing::loadFromLua(luabridge::LuaRef table) {
    LuaRef layers = table["Layers"];
    if(!layers.isTable()){
        std::cerr << "Table does not contain a Layers, or Layers is not a table" << std::endl;
    }
    else {
        m_layers.clear();
        FOREACH_KEY_VAL(layers)
            addLayer(key,val);
        END_FOREACH
    }
    
    default_layer_pos = -1;
    LUA_LOAD_STRING(table,default_layer)
   
    if(default_layer == LUA_NULL_STRING){
        for(auto& kv : m_layers){
            default_layer = kv.second.name;
            default_layer_pos = kv.first;
        }
        if(default_layer_pos == -1){
            default_layer = "Default";
             default_layer_pos = addLayer(default_layer);
        }
//         default_layer = "Default";
    }
    else {
        default_layer_pos = getLayerNumberByName(default_layer);
        if(default_layer_pos == -1){
            default_layer_pos = addLayer(default_layer);
        }
    }
    
    if(SystemsManager::DEBUG)
        std::cout << "Default layer is " << default_layer << " at pos " << default_layer_pos << std::endl;
}


#include <GL/glew.h>
void Drawing::update(float dt) {
    //Clear the screen
    _window->clear();
    //Draw things in order
    for(auto& kv : m_layers) {
        for(auto& kv2 : kv.second.elements){
            //Translating to make the entity at 0,0 so that drawable position and rotation are relative to entity
            glPushMatrix(); {
                Transform& t = kv2.first->transform();
                PointF pos = t.position();
                glTranslatef(pos.x(),pos.y(),0.f);
                glRotatef(t.rotation(),0.f,0.f,1.f);
                PointF scale = t.scale();
                glScalef(scale.x(),scale.y(),1.f);
                for(std::shared_ptr<GraphicsComp> c : kv2.second) {
                        _window->draw(c->getDrawable());
                }
            } glPopMatrix();
            // Poped matrix, so entity is now back at their original place
        }
    }
}
