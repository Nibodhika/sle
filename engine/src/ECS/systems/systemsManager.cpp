#include "ECS/systems/systemsManager.hpp"

bool SystemsManager::DEBUG = false;

SystemsManager::SystemsManager(){
    
}

SystemsManager::~SystemsManager() {
    shutdown();
}


void SystemsManager::init(Systems systems) {
    
    if(systems & INPUT){
        m_systems.push_back(std::make_shared<Input>());
    }
    if(systems & PHYSICS){
        
    }
    if(systems & SCRIPTS){
        m_systems.push_back(std::make_shared<Scripts>());
    }
    if(systems & DRAWING){
        m_systems.push_back( std::make_shared<Drawing>());
    }
    
}

void SystemsManager::update(float dt) {
    //Update systems in order
    for(auto s : m_systems)
        s->update(dt);
}


void SystemsManager::shutdown() {
    //Delete in the reverse order of creation
    while (!m_systems.empty()) {
        m_systems.back().reset(); //Should delete the pointer
        m_systems.pop_back();
    }
}


void SystemsManager::loadFromLua(luabridge::LuaRef table) {
    auto drawing = getSystem<Drawing>();
    if(drawing){
        auto t = table["Drawing"];
        if(t.isTable())
            drawing->loadFromLua(t);
    }
    
    
    auto input = getSystem<Input>();
    if(input){
        auto t = table["Input"];
        if(t.isTable())
            input->loadFromLua(t);
    }
}
