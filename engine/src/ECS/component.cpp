#include "ECS/component.hpp"

#include "ECS/entity.hpp"

Component::Component(){
}

Component::Component(Entity* entity) {
    setEntity(entity);
}


Component::~Component(){
}


void Component::setEntity(Entity* entity) {
    m_entity = entity;
}


Entity * Component::getEntity() {
    return m_entity;
}
