#include "ECS/system.hpp"

#include <algorithm>

System::System(){
    
}

bool System::registerComponent(std::shared_ptr<Component> component) {
    m_components.push_back(component);
    return true;
}

void System::deleteOne(std::shared_ptr<Component> component) {
    auto it =  std::find (m_components.begin(), m_components.end(), component);
    if(it != m_components.end())
        m_components.erase(it);
}
