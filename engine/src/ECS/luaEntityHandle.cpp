#include "ECS/luaEntityHandle.hpp"

#include "ECS/entity.hpp"
                

void LuaEntityHandle::registerInLua() {
    getGlobalNamespace(LUA::STATE)
//     .beginNamespace("engine") //You can put things in a namespace, even if they're not in one
        .beginClass<PointF>("Point")
            .addConstructor <void (*) (float,float)> ()
            .addProperty("x",&PointF::x, &PointF::x)
            .addProperty("y",&PointF::y, &PointF::y)
        .endClass()
        .beginClass<Transform>("Transform")
            .addProperty("pos",&Transform::position, &Transform::setPosition)
            .addProperty("rotation", &Transform::rotation, &Transform::setRotation)
            .addProperty("scale", &Transform::scale, &Transform::setScale)
            .addFunction("move", (void (Transform::*)(PointF)) &Transform::move)
            .addFunction("move2", (void (Transform::*)(float,float)) &Transform::move)
            .addFunction("rotate",&Transform::rotate)
            .addFunction("scaleBy",(void (Transform::*)(PointF)) &Transform::scale)
            .addFunction("scaleBy2",(void (Transform::*)(float,float)) &Transform::scale)
            .addFunction("scaleUniform",(void (Transform::*)(float)) &Transform::scale)
        .endClass()
        .beginClass<LuaEntityHandle>("Entity")
//             .addConstructor<void(*) (void)>()
//             .addProperty("name", &A::getName, &A::setName) //Functions can be binded to geters and seters to make a property in lua
                .addProperty("name", &LuaEntityHandle::name)
                .addData("transform", &LuaEntityHandle::transform)
                .addData("data", &LuaEntityHandle::m_data)
        .endClass();
//     .endNamespace();
}


LuaEntityHandle::LuaEntityHandle(Entity* e)  : m_data(LUA::STATE){
     transform = &e->transform();
    m_entity = e;
    m_data = newTable(LUA::STATE);
}

std::string LuaEntityHandle::name() const {
    return m_entity->getName();
}
