#include "ECS/components/componentFactory.hpp"

#include "ECS/components/spriteComp.hpp"
#include "ECS/components/collisionComp.hpp"
#include "ECS/components/scriptComp.hpp"
#include "ECS/components/actionComp.hpp"
#include "ECS/components/textComp.hpp"

#include "ECS/systems/systemsManager.hpp"

#include "ECS/entity.hpp"

namespace ComponentFactory{
    
    std::vector<std::shared_ptr<Component>> load(std::string name, LuaRef& table, Entity* parent,AssetManager& m){
        std::vector<std::shared_ptr<Component>> out;
        std::shared_ptr<Component> comp = nullptr;
        bool attemptedLoad = false;
        if(name == SpriteComp::TAG) {
           comp = loadSingular<SpriteComp,Drawing>(table,parent,m);
           attemptedLoad = true;
        } 
         else if(name == CollisionComp::TAG) {
            comp = loadSingular<CollisionComp>(table,parent,m);
            attemptedLoad = true;
        }
        else if(name == ScriptComp::TAG) {
            comp = loadSingular<ScriptComp,Scripts>(table,parent,m);
            attemptedLoad = true;
        }
        else if(name == ActionComp::TAG) {
            comp = loadSingular<ActionComp,Input>(table,parent,m);
            attemptedLoad = true;
        }
        else if(name == TextComp::TAG) {
            comp = loadSingular<TextComp,Drawing>(table,parent,m);
            attemptedLoad = true;
        }
        
        if(attemptedLoad){ //Already found, this was a single component
            if(comp) //was loaded correctly, add to list and return it
                out.push_back(comp);
            else
                std::cerr << "ERROR: Error loading component " << name << " in entity " << parent->getName() << std::endl;
            return out;
        }

        
        //Component was not loaded as a single, either it's a plural or was not loaded correctly
        if(name == SpriteComp::TAG_PLURAL){
            for (Iterator iter (table); !iter.isNil (); ++iter) {
                LuaRef item = *iter;
                std::shared_ptr<Component> comp = loadSingular<SpriteComp,Drawing>(item,parent,m);
                if(comp)
                    out.push_back(comp);
                else
                    std::cerr << "ERROR: Error loading component " << iter.key() << " in " << name << " in entity " << parent->getName() << std::endl;
            }
        }
        else if(name == CollisionComp::TAG_PLURAL){
            std::cout << "\n\n\nNot implemented loading of Plural collision comp in componentFactory\n\n" << std::endl;
//             for (Iterator iter (table); !iter.isNil (); ++iter) {
//                 LuaRef item = *iter;
//                 std::shared_ptr<Component> comp = loadSingular<CollisionComp,Collision>(item,parent);
//                 out.push_back(comp);
//             }
        }
        else if(name == ScriptComp::TAG_PLURAL){
            for (Iterator iter (table); !iter.isNil (); ++iter) {
                LuaRef item = *iter;
                std::shared_ptr<Component> comp = loadSingular<ScriptComp,Scripts>(item,parent,m);
                if(comp)
                    out.push_back(comp);
                 else
                    std::cerr << "ERROR: Error loading component " << iter.key() << " in " << name << " in entity " << parent->getName() << std::endl;
            }
        }
        else if(name == ActionComp::TAG_PLURAL){
            for (Iterator iter (table); !iter.isNil (); ++iter) {
                LuaRef item = *iter;
                std::shared_ptr<Component> comp = loadSingular<ActionComp,Input>(item,parent,m);
                if(comp)
                    out.push_back(comp);
                 else
                    std::cerr << "ERROR: Error loading component " << iter.key() << " in " << name << " in entity " << parent->getName() << std::endl;
            }
        }
        else if(name == TextComp::TAG_PLURAL){
            for (Iterator iter (table); !iter.isNil (); ++iter) {
                LuaRef item = *iter;
                std::shared_ptr<Component> comp = loadSingular<TextComp,Drawing>(item,parent,m);
                if(comp)
                    out.push_back(comp);
                else
                    std::cerr << "ERROR: Error loading component " << iter.key() << " in " << name << " in entity " << parent->getName() << std::endl;
            }
        }

        return out;
    }
    
};
