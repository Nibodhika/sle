#include "ECS/components/spriteComp.hpp"

#include "managers/assetManager.hpp"

#include "ECS/systems/systemsManager.hpp"

std::string SpriteComp::TAG = "Sprite";
std::string SpriteComp::TAG_PLURAL = "Sprites";

SpriteComp:: SpriteComp(Entity* entity) : GraphicsComp(entity) {
}
SpriteComp::~SpriteComp() {
    if(m_sprite)
        delete(m_sprite);
}


Sprite * SpriteComp::sprite() {
    return m_sprite;
}


bool SpriteComp::load(luabridge::LuaRef& table,AssetManager& m) {
    std::string image;
    LUA_LOAD_STRING(table,image)
    Image* img = m.getImage(image);
    
    if(!img){
        std::cerr << "ERROR: Image " << image << " not found" << std::endl;
        return false;
    }
    
    m_sprite = new Sprite(img);
    
    loadDrawable(table,m_sprite);
    return true;
}
