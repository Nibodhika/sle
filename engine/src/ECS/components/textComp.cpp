#include "ECS/components/textComp.hpp"

#include "ECS/systems/systemsManager.hpp"
#include "managers/localeManager.hpp"

std::string TextComp::TAG = "Text";
std::string TextComp::TAG_PLURAL = "Texts";

TextComp:: TextComp(Entity* entity) : GraphicsComp(entity) {
}
TextComp::~TextComp() {
    
    LocaleManager::instance().deregisterText(this);
    if(m_text)
        delete(m_text);
}


Text * TextComp::text() {
    return m_text;
}

void TextComp::regenText() {
//     std::cout << "Called text regen" << std::endl;
    std::string t = LocaleManager::instance().getString(m_text_id);
//     std::cout << "new text is: " << t << std::endl;
    m_text->changeText(t);
}


bool TextComp::load(luabridge::LuaRef& table,AssetManager& m) {
  std::string font;
    LUA_LOAD_STRING(table,font)

    Font* f = m.getFont(font);
    if(!f){
        std::cerr << "ERROR: Font " << font << " not found" << std::endl;
        return false;
    }
    
    std::string text;
    LUA_LOAD_STRING(table,text)
    m_text_id = text;
    auto& locale = LocaleManager::instance();
    std::string t = locale.getString(text);
    locale.registerText(this);
    
    m_text = new Text(f,t);
    
    loadDrawable(table,m_text);
    return true;
}
