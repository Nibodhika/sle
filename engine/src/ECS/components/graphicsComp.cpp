#include "ECS/components/graphicsComp.hpp"
#include "ECS/systems/systemsManager.hpp"
GraphicsComp::GraphicsComp(Entity* parent) : Component(parent){
    auto drawing = SystemsManager::get().getSystem<Drawing>();
    if(drawing)
        m_layer = drawing->defaultLayerName();
    else
        std::cerr << "ERROR: Graphics Component created before Drawing system was initialized" << std::endl;
}

GraphicsComp::~GraphicsComp() {
    //Should not delete drawable, as it's deleted by it's own class
}

void GraphicsComp::loadDrawable(luabridge::LuaRef& table, Drawable* drawable) {
    std::string layer;
    LUA_LOAD_STRING(table,layer)

    m_layer = layer;
    m_drawable = drawable;
    if(table["pos"].isTable()){
        Rect pos;
        LUA_LOAD_RECT(table,pos,"in GraphicsComp in Entity "+m_entity->getName())
        m_drawable->setPos(pos.pos());
        if(! pos.isNull() ){
            m_drawable->setSize(pos.size());
        }
    }
    if(table["rotation"].isNumber()){
        m_drawable->setRotation(table["rotation"]);
    }
}

void GraphicsComp::updateTransform() {
    Transform abs = m_entity->absoluteTransform();
    abs*=m_transform;
    m_drawable->setPos(abs.position().rounded());
//     m_drawable->setCenter(m_entity->absoluteTransform().position().rounded());
    m_drawable->setRotation(abs.rotation());
}


std::string GraphicsComp::getLayer() {
    return m_layer;
}

Drawable * GraphicsComp::getDrawable() {
    return m_drawable;
}
