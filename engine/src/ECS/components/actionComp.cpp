#include "ECS/components/actionComp.hpp"

std::string ActionComp::TAG = "Action";
std::string ActionComp::TAG_PLURAL = "Actions";

ActionComp::ActionComp(Entity* e) : Component(e) {
    
}

ActionComp::~ActionComp() {
}

std::string ActionComp::actionName() {
    return action;
}


bool ActionComp::load(luabridge::LuaRef& table,AssetManager& m) {
    LUA_LOAD_STRING(table,action)
    LUA_LOAD_FUNC(table,on,"in ActionComp in Entity "+m_entity->getName())
    LUA_LOAD_FUNC(table,off,"in ActionComp in Entity "+m_entity->getName())
    LUA_LOAD_FUNC(table,during,"in ActionComp in Entity "+m_entity->getName())
    return true;
}


