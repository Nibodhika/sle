#include "ECS/components/collisionComp.hpp"

std::string CollisionComp::TAG = "Collision";
std::string CollisionComp::TAG_PLURAL = "Collisions";

CollisionComp::CollisionComp(Entity* entity) : Component(entity) {
    
}

bool CollisionComp::load(luabridge::LuaRef& table,AssetManager& m) {
    Rect rect;
    LUA_LOAD_RECT(table,rect,"in CollisionComp in Entity "+m_entity->getName())
//     std::cout << "Collision rect is: " << rect.x() << " " << rect.y() << " " << rect.w() << " " << rect.h() << std::endl;
    LUA_LOAD_FUNC(table,collide,"in CollisionComp in Entity "+m_entity->getName())
    
    return true;
}

