#include "ECS/components/scriptComp.hpp"

std::string ScriptComp::TAG = "Script";
std::string ScriptComp::TAG_PLURAL = "Scripts";

ScriptComp::ScriptComp(Entity* entity) : Component(entity){
}

bool ScriptComp::load(luabridge::LuaRef& table,AssetManager& m) {
    LUA_LOAD_FUNC(table,update,"in ScriptComp in Entity "+m_entity->getName())
    LUA_LOAD_FUNC(table,start,"in ScriptComp in Entity "+m_entity->getName())
    return true;
}
