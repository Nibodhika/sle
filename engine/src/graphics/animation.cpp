#include "graphics/animation.hpp"

#include <iostream>

Animation::Animation(){
    
}

Animation::~Animation() {
}

void Animation::pause() {
    m_paused = true;
}

void Animation::play() {
    m_paused = false;
}

void Animation::stop() {
    pause();
    reset();
}

bool Animation::isPaused() {
    return m_paused;
}


bool Animation::isLooping() {
    return m_looping;
}
void Animation::setLoop(bool v) {
    m_looping = v;
}


