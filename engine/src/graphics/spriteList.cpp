#include "graphics/spriteList.hpp"

SpriteList::SpriteList(){
    
}

SpriteList::~SpriteList() {
    for(auto s: m_spriteList)
        delete(s);
}

void SpriteList::addSprite(Sprite* s) {
    m_spriteList.push_back(s);
}


void SpriteList::draw(Window* window) {
    for(auto s : m_spriteList){
        s->draw(window);
    }
}

