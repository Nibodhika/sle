#include "graphics/spriteSheetAnimation.hpp"

#include <iostream>

SpriteSheetAnimation::SpriteSheetAnimation(){
    
}

SpriteSheetAnimation::SpriteSheetAnimation(Texture* t, int amount_x, int amount_y,int initial_anim, int amount_anim) {
    float width = t->width() / amount_x;
    float height = t->height() / amount_y;
    if(amount_anim == -1)
        amount_anim = amount_x * amount_y - initial_anim;
    else if(amount_anim > amount_x * amount_y - initial_anim){
        std::cerr << "amount anim was set to " << amount_anim << " but spriteSheet has " << amount_x << "x" << amount_y << " animations, and started from " << initial_anim << ". Setting to the maximum with the same initial" << std::endl;
        amount_anim = amount_x * amount_y - initial_anim;
    }
    
    std::cout << "a texture of " << t->width() << "x" << t->height() << " can be splited into " << amount_x << "x" << amount_y << " rectangles of " << width << "x" << height << std::endl;
    
//     setTexture(t);
    setSize(width,height);
    m_amount_frames = amount_anim;
    
    std::cout << width << "x" << height << std::endl;
    
    int j = 0;
    int i = 0;
    if(initial_anim != 0) {
        j = initial_anim / amount_x;
        i = initial_anim % amount_x;
        std::cout << "Initial anim " << initial_anim << " means " << i << "," << j << std::endl;
    }
    
    for(; j < amount_x; j++)
        for(; i < amount_y; i++) {
            float x = i*width;
            float y = j*height;
                Rect subRect(x,y,width,height);
                Image i(t,subRect);
                addFrame(i);
                if( --amount_anim <= 0){
                    setFrame(0);
                    return;
                }
        }
    
    setFrame(0);
}

void SpriteSheetAnimation::onSetFrame(unsigned int f_number) {
    m_currentImage = &m_frames[f_number];
}

void SpriteSheetAnimation::nextFrame() {
    if( m_currentFrame + 1 >= m_amount_frames){
        if(m_looping){
            setFrame(0);
        }
        else
            stop();
    }
    else{
        setFrame(m_currentFrame + 1);
    }
    
}

void SpriteSheetAnimation::update(float dt) {
    if(m_paused)
        return;
    m_time += dt;
    if(m_time > m_spf){
        m_time -= m_spf;
        nextFrame();
    }
}

void SpriteSheetAnimation::reset() {
    setFrame(0);
}

void SpriteSheetAnimation::setFPS(float fps) {
    m_spf = 1 / fps;
}
float SpriteSheetAnimation::getFPS() {
    return 1 / m_spf;
}

void SpriteSheetAnimation::addFrame(Image sprite) {
    m_frames.push_back(sprite);
}

void SpriteSheetAnimation::setFrame(unsigned int f_number) {
    m_currentFrame = f_number;
    onSetFrame(f_number);
}

void SpriteSheetAnimation::draw(Window* window) {
    doDraw(window, m_currentImage->texture(), m_currentImage->srcRect());
//       window->draw(m_currentImage->texture(), m_currentImage->srcRect(), &m_pos, m_angle, m_center, m_flipped);
}
