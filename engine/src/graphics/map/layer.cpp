#include "graphics/map/layer.hpp"

#include "graphics/map/tiledMap.hpp"
#include "ECS/systems/systemsManager.hpp"

// Bits on the far end of the 32-bit global tile ID are used for tile flags
// const unsigned FLIPPED_HORIZONTALLY_FLAG = 0x80000000;
// const unsigned FLIPPED_VERTICALLY_FLAG   = 0x40000000;
// const unsigned FLIPPED_DIAGONALLY_FLAG   = 0x20000000;
//TODO read the flipped flags

MapLayer::MapLayer(json json, TiledMap* map) : GraphicsComp(map) {
//     std::cout << json << std::endl;
    
    m_map = map;
    
    m_name = json["name"];
    m_opacity = json["opacity"];

    m_type = string2Type( json["type"]);
    
    m_spriteList = new SpriteList();
    m_drawable = m_spriteList;
    
    int width = json["width"];
    int height = json["height"];
    
//     std::cout << "Layer: " << m_name << " is of type " << type2String(m_type) << " and has oppacity of: " << m_opacity << std::endl;
    //If there's a drawing layer with the same name as myself put me in that layer, otherwise I'm at default
    auto drawing_layer = json["properties"]["layer"]; 
    if(drawing_layer.is_string()) {
        //User especified a layer to be used
        m_layer = drawing_layer;
    }
    else {
        //Attempting to use my name as a layer
        auto layer = SystemsManager::get().getSystem<Drawing>()->getLayerByName(m_name) ;
        if(layer)
            m_layer = m_name;
    }
  
   
    
    if( m_type == tilelayer) {
        auto& data = json["data"];
        int i = 0;
        for(auto& d : data){
            Sprite* sprite = m_map->createSprite( (int) d);
            if(sprite){
                int x = (i % width) * m_map->tileWidth();
                int y = (i / width) * m_map->tileHeight();
//                 std::cout << "Setting position to " << x << "," << y << std::endl;
                sprite->setPos(x,y);
                m_spriteList->addSprite( sprite );
            }
            i++;
        }
    }
    else if( m_type == objectgroup){
         auto& objects = json["objects"];
         for(auto& o : objects){
             int gid = o["gid"];

             //TODO should read rotation and use it
             
             Sprite* sprite = m_map->createSprite(gid);
             if(sprite){
                 
                    int height = o["height"];
                    int width = o["width"];
                    int x = o["x"];
                    int y = o["y"];
                    double angle = o["rotation"];
                    std::cout << "setting pos to: " << x << "," << y << std::endl;
                    sprite->setPos(x,y - height); //TODO why? WHY!?
                    sprite->setSize(width,height);
                    sprite->rotate(angle);
                    m_spriteList->addSprite( sprite );
             }
             //Things I'm not reading
             // id, name, type, visible, rotation
        }
    }
   
    
    // Other informations that I probably don't need
    // x,y,, visible
    

}

MapLayer::~MapLayer() {
    delete(m_spriteList);
}

MapLayer::Type MapLayer::string2Type(std::string t) {
    MapLayer::Type out;
     if(t == "tilelayer"){
        out = tilelayer;
    }
    else if( t == "objectgroup"){
        out = objectgroup;
    }
    else {
        out = unknown;
    }
    return out;
}

std::string MapLayer::type2String(Type t) {
   switch(t){
       case unknown:
           return "Unknown";
           break;
       case tilelayer:
           return "TileLayer";
           break;
       case objectgroup:
           return "ObjectGroup";
           break;
    }
}
