#include "graphics/map/tiledMap.hpp"

#include<iostream>
#include <fstream> 
#include "ECS/systems/systemsManager.hpp"
#include "json/json.hpp"
// for convenience
using json = nlohmann::json;

TiledMap::TiledMap(std::string name, Entity* parent) : Entity(name,parent){
    
}
TiledMap::~TiledMap() {
    for(auto t : m_tilesets){
        delete(t);
    }
//     for(auto l : m_layers){
//         delete(l);
//     }
}


bool TiledMap::open(std::string filepath) {
    std::ifstream ifs(filepath);
    json j(ifs);
    m_width = j["width"];
    m_height = j["height"];
    m_tilewidth = j["tilewidth"];
    m_tileheight = j["tileheight"];
    
//     std::cout << m_width << "x" << m_height << " tiles of " << m_tilewidth << "x" << m_tileheight << std::endl;
    
    auto& tilesets = j["tilesets"];
//     std::cout << "Tilesets:" << std::endl;
    for (auto& tileset : tilesets) {
        m_tilesets.push_back( new Tileset(tileset));
    }
    
    auto& layers = j["layers"];
//     std::cout << "Layers:" << std::endl;
    for (auto& layer : layers) {
        auto l = std::make_shared<MapLayer>(layer,this);
        m_layers.push_back(l);
        addComponent(l);
    }

    return true;
}

int TiledMap::tileWidth() {
    return m_tilewidth;
}


int TiledMap::tileHeight() {
    return m_tileheight;
}

void TiledMap::registerDrawing() {
    auto drawingSystem = SystemsManager::get().getSystem<Drawing>();
    for(auto l : m_layers)
        drawingSystem->registerComponent(l);
}

void TiledMap::deregisterDrawing() {
    auto drawingSystem = SystemsManager::get().getSystem<Drawing>();
    for(auto l : m_layers)
        drawingSystem->deleteOne(l);
}


// void TiledMap::draw(Window* window){
//     
//     for(std::shared_ptr<GraphicsComp> g : getComponents<GraphicsComp>() ){
// //         layer->draw(window);
//         g->getDrawable()->draw(window);
//     }
// }

Sprite* TiledMap::createSprite(int gid) {
//     std::cout << "Getting sprite from gid " << gid << std::endl;
    if(gid == 0)
        return nullptr;
     for (auto tileset : m_tilesets) {
        if(tileset->hasGid(gid)) {
//             std::cout << "Found a tileset that has that gid" << std::endl;
            return tileset->createSprite(gid);
        }
    }
    return nullptr;
}

