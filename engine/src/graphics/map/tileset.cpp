#include "graphics/map/tileset.hpp"

Tileset::Tileset(json json){
//     std::cout << json << std::endl;
    
    std::string file = json["image"];
    file = "../../../resources/maps/"+file; //Quick hack
    m_texture = new Texture(file);

    
    m_firstgid = json["firstgid"];
    m_name = json["name"];
    
    m_margin = json["margin"];
    m_spacing = json["spacing"];
    
    m_tilewidth = json["tilewidth"];
    m_tileheight = json["tileheight"];    
    
    m_columns = json["columns"];   
    
    int tilecount = ((int) json["tilecount"]);
    m_lastgid = m_firstgid +  tilecount;
    
    //Other informations that I probably don't need
    // imageheight, imagewidth
    
    for(int gid = 0; gid < tilecount; gid++){
        int y = gid / m_columns;
        int x = gid % m_columns;
        
        int px = x * (m_tilewidth + m_spacing) + m_margin;
        int py = y * (m_tileheight + m_spacing) + m_margin;
        
        Rect rect(px,py,m_tilewidth,m_tileheight);
        Image* i = new Image(m_texture, rect);
        m_images.push_back( i);
    }

//     std::cout << "Tileset " << m_name << " starts on " << m_firstgid << " and has tiles of " << m_tilewidth << "x" <<  m_tileheight << std::endl;
}

Tileset::~Tileset() {
    if(m_texture)
        delete(m_texture);
    for(auto i : m_images)
        delete(i);
}


bool Tileset::hasGid(int gid) {
    return gid >= m_firstgid && gid < m_lastgid;
}


Sprite* Tileset::createSprite(int gid) {
    if( ! hasGid(gid)) {
        std::cout << "ERROR: Tileset " << m_name << " does not have gid " << gid << " this will fail" << std::endl;
        return nullptr;
    }
    
    int internal_gid = gid - m_firstgid;
    return new Sprite( m_images[internal_gid] );
}


